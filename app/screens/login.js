import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  ScrollView,
  TouchableOpacity,
  KeyboardAvoidingView,
  Image,
  TextInput,
  Dimensions,
  Keyboard,
  View,
  TouchableHighlight
} from "react-native";
import { Actions } from "react-native-router-flux";
import Orientation from "react-native-orientation";
import { connect } from "react-redux";
import { addCard, addValues } from "../actions";
import { Container, Header, Content, Card, CardItem, Body } from "native-base";
import Icon from "react-native-vector-icons/dist/FontAwesome";
import Icon1 from "react-native-vector-icons/dist/EvilIcons";
import SplashScreen from "react-native-splash-screen";
import { ActionCreators } from "../actions";
import { bindActionCreators } from "redux";

var width = Dimensions.get("window").width; //full width
var height = Dimensions.get("window").height; //full height

var show = false;
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = { username: "Username / Email Id" };
    this.state = { isKeyBoardVisible: false };
    this.state = { Password: "Password" };
    this.focusNextField = this.focusNextField.bind(this);
    this.updateValue = this.updateValue.bind(this);
    this._onPressButton = this._onPressButton.bind(this);

    this.inputs = {};
  }

  focusNextField(id) {
    this.inputs[id].focus();
  }

  componentDidMount() {
    Orientation.lockToLandscape();
    SplashScreen.hide();
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide
    );
  }

  _keyboardDidShow = () => {
    this.setState({ isKeyBoardVisible: true });
    show = true;
  };

  _keyboardDidHide = () => {
    this.setState({ isKeyBoardVisible: false });
    show = false;
  };

  updateValue() {
    this.props.performLogin();
    // Actions.home()
  }

  _onPressButton() {
    Keyboard.dismiss();
    Actions.home();
    this.updateValue();

    //  console.log(this.props);
    //this.props.performLogin()

    //  Actions.resetpass()
  }
  _onForgotPass() {
    // console.log('login clicked');
    //  Actions.home()
    Keyboard.dismiss();
    Actions.resetpass();
  }

  render() {
    const myIcon = (
      <Icon1 name="envelope" size={width * 0.03} color="#D8D8D8" />
    );
    const passIcon = <Icon1 name="lock" size={width * 0.03} color="#D8D8D8" />;
    var keyStyle = this.state.isKeyBoardVisible
      ? styles.cardStyle1
      : styles.cardStyle;
    const logoView = !this.state.isKeyBoardVisible ? (
      <View style={styles.applogoView}>
        <Image style={styles.applogo} source={require("../assets/logo.png")} />
      </View>
    ) : (
      <View />
    );

    return (
      <View style={styles.container}>
        <View style={styles.centerView}>
          {logoView}

          <View style={styles.loginView}>
            <View style={keyStyle}>
              <View style={styles.inputView}>
                {myIcon}
                <TextInput
                  style={styles.inputText}
                  onChangeText={text => this.setState({ username: text })}
                  value={this.state.username}
                  placeholder="/Email"
                  underlineColorAndroid="rgba(0,0,0,0)"
                  returnKeyType={"next"}
                  onSubmitEditing={() => {
                    this.focusNextField("Password");
                    this.setState({ isKeyBoardVisible: false });
                  }}
                  ref={input => {
                    this.inputs["Username/Email"] = input;
                  }}
                />
              </View>
              <View style={styles.inputView}>
                {passIcon}
                <TextInput
                  style={styles.inputText}
                  onChangeText={text => this.setState({ password: text })}
                  value={this.state.password}
                  placeholder="Password"
                  underlineColorAndroid="rgba(0,0,0,0)"
                  secureTextEntry={true}
                  returnKeyType={"done"}
                  ref={input => {
                    this.inputs["Password"] = input;
                  }}
                />
              </View>

              <View style={styles.buttonViewBackground}>
                <TouchableOpacity
                  style={styles.buttonBackground}
                  onPress={this._onPressButton}
                  underlayColor="#25777F"
                >
                  <Text style={styles.buttonText}>LOGIN</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.forgotView}>
              <TouchableOpacity
                onPress={this._onForgotPass}
                style={styles.forgotBtnBack}
              >
                <Text style={styles.forgotPassText}>Forgot Password ?</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#EFEBEB"
  },
  keyContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  centerView: {
    flex: 0.95,
    width: "55%",
    alignItems: "center",
    backgroundColor: "transparent"
  },
  applogo: {
    justifyContent: "center",
    alignItems: "center",
    flex: 0.3
  },
  loginView: {
    backgroundColor: "transparent",
    flex: 0.6,
    width: "70%",
    height: "80%"
  },

  buttonBackground: {
    borderRadius: 40,
    backgroundColor: "#24767E"
  },

  button: {
    width: "45%",
    height: "45%",
    borderRadius: 50,
    flexDirection: "row",
    backgroundColor: "#25777F",
    borderColor: "#48BBEC",
    margin: 20,
    justifyContent: "center"
  },
  submitText: {
    color: "#fff",
    textAlign: "center"
  },
  viewContainer: {
    backgroundColor: "#2E9298",
    borderRadius: 7,
    padding: 10,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 0.8,
    shadowOpacity: 1.0
  },

  inputView: {
    width: "90%",
    height: "30%",
    flexDirection: "row",
    marginLeft: "8%",
    marginRight: "10%",
    justifyContent: "flex-start",
    alignItems: "center",
    borderBottomWidth: 0.5,
    borderColor: "#EBEBEB"
  },
  inputText: {
    height: "100%",
    width: "100%",
    textAlign: "center",
    fontSize: width * 0.017
  },
  buttonViewBackground: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: "33%",
    marginTop: "2%",
    marginBottom: "2%"
  },
  buttonText: {
    margin: "7%",
    alignSelf: "center",
    color: "#FFF",
    fontSize: width * 0.016
  },
  forgotView: {
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "flex-end",
    marginBottom: "2%"
  },
  forgotBtnBack: {
    flexDirection: "row",
    marginBottom: "2%",
    justifyContent: "flex-end",
    alignItems: "flex-end"
  },
  forgotPassText: {
    color: "#24767E",
    marginTop: "4%",
    fontSize: width * 0.012
  },

  applogo: {
    width: "90%",
    height: "90%",
    margin: 10,
    resizeMode: "contain"
  },
  applogoView: {
    width: "50%",
    height: "25%",
    marginTop: 10,
    marginBottom: 15,
    justifyContent: "center",
    alignItems: "center"
  },
  cardStyle: {
    width: "100%",
    height: "85%",
    paddingTop: "1%",
    paddingLeft: "5%",
    paddingRight: "5%",
    position: "relative",
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "white",

    borderWidth: 0.5,
    borderRadius: 2,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#E8E5E5",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1
  },
  cardStyle1: {
    width: "100%",
    height: "75%",
    paddingTop: "1%",
    paddingLeft: "5%",
    paddingRight: "5%",
    position: "relative",
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "white",

    borderWidth: 0.5,
    borderRadius: 2,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#E8E5E5",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1
  }
});
//export default Login
function mapDispatchToprops(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(state => {
  return {
    loginData: state.loginData
  };
}, mapDispatchToprops)(Login);

//export default connect(mapStateToProps, {addValues})(Login);
