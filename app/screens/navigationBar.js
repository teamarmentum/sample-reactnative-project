import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  Dimensions,
  ScrollView
} from "react-native";

import LinearGradient from "react-native-linear-gradient";

var { height, width } = Dimensions.get("window");
export default class NavigationBar extends Component {
  constructor(props, context) {
    super(props, context);
    console.log(props);
    this.state = {
      active: "Dashboard",
      key: "Dashboard"
    };

    this._selectTab = this._selectTab.bind(this);
  }

  _selectTab(key) {
    this.setState({ active: key, key: key });
    this.props.method(key);
  }

  render() {
    return (
      <View
        style={{
          width: "8%",
          height: "100%",
          backgroundColor: "white",
          flexDirection: "column"
        }}
      >
        <View
          style={{
            width: "100%",
            height: "75%",
            backgroundColor: "transparent",
            flexDirection: "column"
          }}
        >
          <View
            style={{
              width: "100%",
              height: "16.66%",
              backgroundColor: "transparent",
              flexDirection: "column",
              justifyContent: "center"
            }}
          >
            <Image
              style={{
                width: "70%",
                height: "70%",
                resizeMode: "contain",
                alignSelf: "center"
              }}
              source={require("../assets/logo.png")}
            />
          </View>

          <TouchableOpacity
            style={{
              marginBottom: 0,
              width: "100%",
              height: "16.66%",
              backgroundColor: "transparent",
              flexDirection: "column"
            }}
            onPress={() => this._selectTab("Dashboard")}
          >
            {this.state.active === "Dashboard" && (
              <LinearGradient
                colors={["#004F63", "#008963"]}
                style={{
                  width: "100%",
                  height: "100%",
                  justifyContent: "center",
                  alignSelf: "center"
                }}
                source={require("../assets/menuSelect.png")}
              >
                <Image
                  style={{
                    width: "55%",
                    height: "55%",
                    resizeMode: "contain",
                    alignSelf: "center"
                  }}
                  source={require("../assets/dash.png")}
                />
                <Text
                  style={{
                    fontSize: width * 0.0075,
                    marginLeft: 5,
                    color: "#FFF",
                    alignSelf: "center",
                    marginTop: 5
                  }}
                >
                  DASHBOARD
                </Text>
              </LinearGradient>
            )}

            {this.state.active !== "Dashboard" && (
              <View
                style={{
                  width: "100%",
                  height: "100%",
                  justifyContent: "center",
                  alignSelf: "center"
                }}
              >
                <Image
                  style={{
                    width: "55%",
                    height: "55%",
                    resizeMode: "contain",
                    alignSelf: "center"
                  }}
                  source={require("../assets/dash.png")}
                />
                <Text
                  style={{
                    fontSize: width * 0.0075,
                    marginLeft: 5,
                    color: "#A9A8A8",
                    alignSelf: "center",
                    marginTop: 5
                  }}
                >
                  DASHBOARD
                </Text>
              </View>
            )}
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              marginBottom: 3,
              width: "100%",
              height: "16.66%",
              backgroundColor: "transparent",
              flexDirection: "column"
            }}
            onPress={() => this._selectTab("OperationManagment")}
          >
            {this.state.active === "OperationManagment" && (
              <LinearGradient
                colors={["#004F63", "#008963"]}
                style={{
                  width: "100%",
                  height: "100%",
                  justifyContent: "center",
                  alignSelf: "center"
                }}
                source={require("../assets/menuSelect.png")}
              >
                <Image
                  style={{
                    width: "50%",
                    height: "50%",
                    resizeMode: "contain",
                    alignSelf: "center"
                  }}
                  source={require("../assets/opmanage.png")}
                />
                <Text
                  style={{
                    fontSize: width * 0.0075,
                    color: "#FFF",
                    alignSelf: "center",
                    marginTop: 5
                  }}
                >
                  OPERATION
                </Text>
                <Text
                  style={{
                    fontSize: width * 0.0075,
                    color: "#FFF",
                    alignSelf: "center"
                  }}
                >
                  MANAGMENT
                </Text>
              </LinearGradient>
            )}

            {this.state.active !== "OperationManagment" && (
              <View
                style={{
                  width: "100%",
                  height: "100%",
                  justifyContent: "center",
                  alignSelf: "center"
                }}
              >
                <Image
                  style={{
                    width: "50%",
                    height: "50%",
                    resizeMode: "contain",
                    alignSelf: "center"
                  }}
                  source={require("../assets/opmanage.png")}
                />
                <Text
                  style={{
                    fontSize: width * 0.0075,
                    color: "#8EB6BA",
                    alignSelf: "center",
                    marginTop: 5
                  }}
                >
                  OPERATION
                </Text>
                <Text
                  style={{
                    fontSize: width * 0.0075,
                    color: "#8EB6BA",
                    alignSelf: "center"
                  }}
                >
                  MANAGMENT
                </Text>
              </View>
            )}
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              marginBottom: 3,
              width: "100%",
              height: "16.66%",
              backgroundColor: "transparent",
              flexDirection: "column"
            }}
            onPress={() => this._selectTab("BarrelManagement")}
          >
            {this.state.active === "BarrelManagement" && (
              <LinearGradient
                colors={["#004F63", "#008963"]}
                style={{
                  width: "100%",
                  height: "100%",
                  justifyContent: "center",
                  alignSelf: "center"
                }}
                source={require("../assets/menuSelect.png")}
              >
                <Image
                  style={{
                    width: "50%",
                    height: "50%",
                    resizeMode: "contain",
                    alignSelf: "center"
                  }}
                  source={require("../assets/barrelmanage.png")}
                />
                <Text
                  style={{
                    fontSize: width * 0.0075,
                    color: "#FFF",
                    alignSelf: "center",
                    marginTop: 5
                  }}
                >
                  BARREL
                </Text>
                <Text
                  style={{
                    fontSize: width * 0.0075,
                    color: "#FFF",
                    alignSelf: "center"
                  }}
                >
                  MANAGMENT
                </Text>
              </LinearGradient>
            )}

            {this.state.active !== "BarrelManagement" && (
              <View
                style={{
                  width: "100%",
                  height: "100%",
                  justifyContent: "center",
                  alignSelf: "center"
                }}
              >
                <Image
                  style={{
                    width: "50%",
                    height: "50%",
                    resizeMode: "contain",
                    alignSelf: "center"
                  }}
                  source={require("../assets/barrelmanage.png")}
                />
                <Text
                  style={{
                    fontSize: width * 0.0075,
                    color: "#8EB6BA",
                    alignSelf: "center",
                    marginTop: 5
                  }}
                >
                  BARREL
                </Text>
                <Text
                  style={{
                    fontSize: width * 0.0075,
                    color: "#8EB6BA",
                    alignSelf: "center"
                  }}
                >
                  MANAGMENT
                </Text>
              </View>
            )}
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              marginBottom: 0,
              width: "100%",
              height: "16.66%",
              backgroundColor: "transparent",
              flexDirection: "column"
            }}
            onPress={() => this._selectTab("Sales")}
          >
            {this.state.active === "Sales" && (
              <LinearGradient
                colors={["#004F63", "#008963"]}
                style={{
                  width: "100%",
                  height: "100%",
                  justifyContent: "center",
                  alignSelf: "center"
                }}
                source={require("../assets/menuSelect.png")}
              >
                <Image
                  style={{
                    width: "55%",
                    height: "55%",
                    resizeMode: "contain",
                    alignSelf: "center"
                  }}
                  source={require("../assets/sales.png")}
                />
                <Text
                  style={{
                    fontSize: width * 0.0075,
                    color: "#FFF",
                    alignSelf: "center",
                    marginTop: 5
                  }}
                >
                  SALES
                </Text>
              </LinearGradient>
            )}

            {this.state.active !== "Sales" && (
              <View
                style={{
                  width: "100%",
                  height: "100%",
                  justifyContent: "center",
                  alignSelf: "center"
                }}
              >
                <Image
                  style={{
                    width: "55%",
                    height: "55%",
                    resizeMode: "contain",
                    alignSelf: "center"
                  }}
                  source={require("../assets/sales.png")}
                />
                <Text
                  style={{
                    fontSize: width * 0.0075,
                    color: "#8EB6BA",
                    alignSelf: "center",
                    marginTop: 5
                  }}
                >
                  SALES
                </Text>
              </View>
            )}
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              marginBottom: 0,
              width: "100%",
              height: "16.66%",
              backgroundColor: "transparent",
              flexDirection: "column"
            }}
            onPress={() => this._selectTab("Inventory")}
          >
            {this.state.active === "Inventory" && (
              <LinearGradient
                colors={["#004F63", "#008963"]}
                style={{
                  width: "100%",
                  height: "100%",
                  justifyContent: "center",
                  alignSelf: "center"
                }}
                source={require("../assets/menuSelect.png")}
              >
                <Image
                  style={{
                    width: "55%",
                    height: "55%",
                    resizeMode: "contain",
                    alignSelf: "center"
                  }}
                  source={require("../assets/inventry.png")}
                />
                <Text
                  style={{
                    fontSize: width * 0.0075,
                    color: "#FFF",
                    alignSelf: "center",
                    marginTop: 5
                  }}
                >
                  INVENTORY
                </Text>
              </LinearGradient>
            )}

            {this.state.active !== "Inventory" && (
              <View
                style={{
                  width: "100%",
                  height: "100%",
                  justifyContent: "center",
                  alignSelf: "center"
                }}
              >
                <Image
                  style={{
                    width: "55%",
                    height: "55%",
                    resizeMode: "contain",
                    alignSelf: "center"
                  }}
                  source={require("../assets/inventry.png")}
                />
                <Text
                  style={{
                    fontSize: width * 0.0075,
                    color: "#8EB6BA",
                    alignSelf: "center",
                    marginTop: 5
                  }}
                >
                  INVENTORY
                </Text>
              </View>
            )}
          </TouchableOpacity>
        </View>

        <View
          style={{
            width: "100%",
            height: "25%",
            backgroundColor: "white",
            flexDirection: "column"
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  foot: {
    width: "100%",
    height: "5%",
    backgroundColor: "white",
    flexDirection: "row"
  },
  col1: {
    flex: 1,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center"
  },
  col2: {
    flex: 1,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center"
  },
  col3: {
    flex: 1,
    backgroundColor: "#f05128",
    justifyContent: "center",
    alignItems: "center"
  },
  col4: {
    flex: 1,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center"
  },
  col5: {
    flex: 1,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center"
  }
});
