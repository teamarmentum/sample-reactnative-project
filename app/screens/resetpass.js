import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Keyboard,
  Image,
  TextInput,
  Dimensions,
  View,
  TouchableHighlight
} from "react-native";
import { Actions } from "react-native-router-flux";
import Orientation from "react-native-orientation";
import { connect } from "react-redux";
import { addCard, addValues } from "../actions";
import { Container, Header, Content, Card, CardItem, Body } from "native-base";
import Icon from "react-native-vector-icons/dist/FontAwesome";

var width = Dimensions.get("window").width; //full width
var height = Dimensions.get("window").height; //full height

class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = { username: "Username / Email Id" };
    this.state = { Password: "Password" };
    this.state = { isKeyBoardVisible: false };
  }

  componentDidMount() {
    Orientation.lockToLandscape();
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide
    );
  }

  _keyboardDidShow = () => {
    this.setState({ isKeyBoardVisible: true });
  };

  _keyboardDidHide = () => {
    this.setState({ isKeyBoardVisible: false });
  };

  updateValue() {
    this.props.addValues();
    // Actions.home()
  }

  _onPressButton() {
    Keyboard.dismiss();
    Actions.forgotpass();
  }

  render() {
    const myIcon = (
      <Icon name="envelope-o" size={width * 0.03} color="#D8D8D8" />
    );
    var keyStyle = this.state.isKeyBoardVisible
      ? styles.cardStyle1
      : styles.cardStyle;

    const logoView = !this.state.isKeyBoardVisible ? (
      <View style={styles.applogoView}>
        <Image style={styles.applogo} source={require("../assets/logo.png")} />
      </View>
    ) : (
      <View />
    );

    return (
      <View style={styles.container}>
        <View style={styles.centerView}>
          {logoView}

          <View style={styles.loginView}>
            <View style={keyStyle}>
              <View style={styles.inputView}>
                {myIcon}
                <TextInput
                  style={styles.inputText}
                  onChangeText={text => this.setState({ username: text })}
                  value={this.state.username}
                  placeholder="Enter Registered Email Id "
                  underlineColorAndroid="rgba(0,0,0,0)"
                />
              </View>

              <View style={styles.buttonViewBackground}>
                <TouchableOpacity
                  style={styles.buttonBackground}
                  onPress={this._onPressButton}
                  underlayColor="#25777F"
                >
                  <Text style={styles.buttonText}>Send Reset Link</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#EFEBEB"
  },
  keyContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  centerView: {
    flex: 0.95,
    width: "55%",
    alignItems: "center",
    backgroundColor: "transparent"
  },
  applogo: {
    justifyContent: "center",
    alignItems: "center",
    flex: 0.3
  },
  loginView: {
    backgroundColor: "transparent",
    flex: 0.6,
    width: "70%",
    height: "75%"
  },

  buttonBackground: {
    borderRadius: 40,
    backgroundColor: "#24767E"
  },

  button: {
    width: "45%",
    height: "45%",
    borderRadius: 50,
    flexDirection: "row",
    backgroundColor: "#25777F",
    borderColor: "#48BBEC",
    margin: 20,
    justifyContent: "center"
  },
  submitText: {
    color: "#fff",
    textAlign: "center"
  },
  viewContainer: {
    backgroundColor: "#2E9298",
    borderRadius: 7,
    padding: 10,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 0.8,
    shadowOpacity: 1.0
  },

  inputView: {
    width: "90%",
    height: "30%",
    flexDirection: "row",
    marginLeft: "8%",
    marginRight: "10%",
    justifyContent: "flex-start",
    alignItems: "center",
    borderBottomWidth: 0.5,
    borderColor: "#EBEBEB"
  },
  inputText: {
    height: "100%",
    width: "100%",
    textAlign: "center",
    fontSize: width * 0.017
  },
  buttonViewBackground: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: "33%",
    marginTop: "10%",
    marginBottom: "2%"
  },
  buttonText: {
    margin: "7%",
    alignSelf: "center",
    color: "#FFF",
    fontSize: width * 0.016
  },
  forgotView: {
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "flex-end"
  },
  forgotBtnBack: {
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "flex-end"
  },
  forgotPassText: {
    color: "#24767E",
    marginTop: 10,
    fontSize: width * 0.012
  },

  applogo: {
    width: "90%",
    height: "90%",
    margin: 10,
    resizeMode: "contain"
  },
  applogoView: {
    width: "50%",
    height: "25%",
    marginTop: 10,
    marginBottom: 15,
    justifyContent: "center",
    alignItems: "center"
  },
  cardStyle: {
    width: "100%",
    height: "85%",
    paddingTop: "2%",
    paddingLeft: "5%",
    paddingRight: "5%",
    position: "relative",
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "white",

    borderWidth: 0.5,
    borderRadius: 2,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#E8E5E5",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1
  },
  cardStyle1: {
    width: "100%",
    height: "65%",
    paddingTop: "1%",
    paddingLeft: "5%",
    paddingRight: "5%",
    position: "relative",
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "white",

    borderWidth: 0.5,
    borderRadius: 2,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#E8E5E5",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1
  }
});
//export default Login
const mapStateToProps = state => {
  return {
    loginData: state.loginData
  };
};

export default connect(mapStateToProps, { addValues })(ResetPassword);
