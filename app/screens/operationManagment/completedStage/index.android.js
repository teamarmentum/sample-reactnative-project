import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  StatusBar,
  Image,
  FlatList,
  View,
  Dimensions
} from "react-native";
var { height, width } = Dimensions.get("window");
import Icon from "react-native-vector-icons/dist/FontAwesome";
import Icon1 from "react-native-vector-icons/dist/EvilIcons";
import Icon2 from "react-native-vector-icons/dist/Ionicons";
import Icon3 from "react-native-vector-icons/dist/MaterialIcons";
import { getSizeInDp } from "@lib/Dimensions";

export default class completedStage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [
        { name: "" },
        { name: "" },
        { name: "" },
        { name: "" },
        { name: "" },
        { name: "" },
        { name: "" },
        { name: "" },
        { name: "" },
        { name: "" },
        { name: "" },
        { name: "" }
      ]
    };
  }

  // componentDidMount() {
  //   this.props.onRef(this)
  //  }
  //  componentWillUnmount() {
  //   this.props.onRef(undefined)
  //  }
  updateDialogValue(value) {
    console.log("do stuff " + value);
  }

  renderItem({ item, index }) {
    return (
      <View style={styles.renderItemStyle}>
        <View style={styles.itemStyle} key={index}>
          <View style={styles.itemSection1}>
            <Image
              style={styles.renderImageStyle}
              source={require("@assets/vessel.png")}
            />
            <View style={styles.renderSection1}>
              <Text style={styles.renderText1}>Mash Tun -mt1-</Text>
              <Text style={styles.renderText2}>170616.01.MT1</Text>
            </View>
            <View style={styles.renderSection2}>
              <Icon1 name="close" size={width * 0.02} color="#717171" />
            </View>
          </View>

          <View style={styles.itemSection2}>
            <Text style={styles.textSection1}>Vessel:</Text>
            <Text style={styles.textSection2}>Mash Ton</Text>
            <Text style={styles.textSection1}>Volume:</Text>
            <Text style={styles.textSection2}>368 Gallons</Text>
          </View>

          <View style={styles.itemSection3}>
            <Text style={styles.textSection3}>Age:</Text>
            <Text style={styles.textSection4}>28 Days, 19 hrs,38 ml</Text>
          </View>

          <View style={styles.itemSection4} />
          <View style={styles.itemSection5}>
            <View style={styles.viewSecion}>
              <Icon name="file-text-o" size={width * 0.012} color="#53939A" />
              <Text style={styles.textSection5}>View Detail</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.cardStyle}>
          <View style={styles.actionViewMain}>
            <Text style={styles.titleMain}>INBOND SPIRITS TRANSFER</Text>
          </View>
        </View>

        {/* FlatList ItemList View Starts */}

        <FlatList
          contentContainerStyle={styles.list}
          removeClippedSubviews={false}
          data={this.state.items}
          renderItem={this.renderItem}
        />
        {/* FlatList ItemList View ends */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "85%",
    flexDirection: "row",
    flexDirection: "column",

    backgroundColor: "#F1ECEC"
  },

  actionView: {
    width: "25%",
    height: "100%",
    flexDirection: "row",
    padding: 5,
    justifyContent: "space-between"
  },
  actionViewMain: {
    width: "50%",
    height: "100%",
    flexDirection: "row",
    backgroundColor: "white",
    justifyContent: "flex-start"
  },
  titleMain: {
    fontSize: width * 0.01,
    color: "#4E9197",
    alignSelf: "center",
    fontWeight: "bold",
    paddingLeft: "2%"
  },
  titleSub: {
    fontSize: width * 0.011,
    color: "#848484",
    alignSelf: "center",
    fontWeight: "bold"
  },
  titleSub1: {
    fontSize: width * 0.011,
    color: "#ACACAC",
    alignSelf: "center",
    marginRight: 10
  },
  spaceview: {
    width: "0.5%",
    height: "100%",
    flexDirection: "row",
    backgroundColor: "#F1ECEC"
  },
  itemSection1: {
    width: "100%",
    height: "40%",
    flexDirection: "row",
    backgroundColor: "transparent"
  },
  itemSection2: {
    width: "100%",
    height: "15%",
    flexDirection: "row",
    backgroundColor: "transparent"
  },
  itemSection3: {
    width: "100%",
    height: "15%",
    flexDirection: "row",
    backgroundColor: "transparent"
  },
  itemSection4: {
    marginTop: "5%",
    width: "100%",
    height: "1%",
    flexDirection: "row",
    backgroundColor: "#ECECEC"
  },
  itemSection5: {
    width: "100%",
    height: "20%",
    flexDirection: "row",
    backgroundColor: "transparent"
  },
  renderItemStyle: {
    flex: 1,
    minWidth: width * 0.22,
    maxWidth: width * 0.22,
    height: width * 0.18,
    maxHeight: width * 0.18,
    padding: width * 0.01
  },
  renderImageStyle: {
    marginTop: width * 0.01,
    width: "25%",
    height: "60%",
    resizeMode: "contain",
    alignSelf: "flex-start"
  },
  renderSection1: {
    width: "50%",
    height: "100%",
    padding: width * 0.005,
    flexDirection: "column",
    justifyContent: "center"
  },
  renderText1: {
    fontSize: width * 0.007,
    color: "#848484",
    fontWeight: "bold"
  },
  renderText2: {
    fontSize: width * 0.007,
    color: "#ACACAC",
    marginRight: width * 0.007,
    fontWeight: "bold"
  },
  renderSection2: {
    width: "25%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: width * 0.005
  },
  textSection1: {
    width: "20%",
    height: "100%",
    fontSize: width * 0.007,
    color: "#000",
    fontWeight: "bold"
  },

  textSection2: {
    width: "30%",
    height: "100%",
    fontSize: width * 0.007,
    color: "#ACACAC",
    marginRight: width * 0.007,
    fontWeight: "bold"
  },
  textSection3: {
    width: "20%",
    height: "100%",
    fontSize: width * 0.007,
    color: "#000",
    fontWeight: "bold"
  },
  textSection4: {
    width: "80%",
    height: "100%",
    fontSize: width * 0.007,
    color: "#ACACAC",
    marginRight: width * 0.01,
    fontWeight: "bold"
  },
  textSection5: {
    fontSize: width * 0.007,
    color: "#53939A",
    fontWeight: "bold",
    marginLeft: width * 0.004,
    alignSelf: "center"
  },
  viewSecion: {
    width: "37%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: width * 0.005
  },
  viewSection1: {
    width: "25%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: width * 0.005
  },

  list: {
    justifyContent: "center",
    flexDirection: "row",
    flexWrap: "wrap",
    margin: width * 0.012,
    alignItems: "center"
  },

  mainContainer: {
    width: "92%",
    height: "100%",
    backgroundColor: "green",
    flexDirection: "column"
  },
  header: {
    backgroundColor: "#455A64"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  itemStyle: {
    width: "100%",
    height: "100%",
    flexDirection: "column",
    backgroundColor: "white",

    paddingLeft: width * 0.008,
    paddingRight: width * 0.008,
    borderWidth: 0.5,
    borderRadius: width * 0.008,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#E8E5E5",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  cardStyle: {
    justifyContent: "space-between",
    backgroundColor: "#FFF",
    width: "99%",
    height: "7%",
    flexDirection: "row",
    backgroundColor: "white",
    alignSelf: "center",
    paddingLeft: width * 0.004,
    paddingRight: width * 0.004,
    borderWidth: 0.5,
    borderRadius: width * 0.003,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#E8E5E5",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1
  }
});
