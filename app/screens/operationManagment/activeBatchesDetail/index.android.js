import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  StatusBar,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  FlatList,
  View,
  Image
} from "react-native";

var { height, width } = Dimensions.get("window");
import Icon from "react-native-vector-icons/dist/Entypo";
import Icon1 from "react-native-vector-icons/dist/EvilIcons";
import Icon2 from "react-native-vector-icons/dist/Ionicons";
import Icon3 from "react-native-vector-icons/dist/MaterialIcons";
import Icon4 from "react-native-vector-icons/dist/FontAwesome";
import Icon5 from "react-native-vector-icons/dist/Foundation";
import { Dropdown } from "react-native-material-dropdown";

export default class ActiveBatchesDetail extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      items: [
        { name: "" },
        { name: "" },
        { name: "" },
        { name: "" },
        { name: "" },
        { name: "" },
        { name: "" },
        { name: "" },
        { name: "" },
        { name: "" },
        { name: "" },
        { name: "" }
      ]
    };
    //  console.log(props);
  }

  // componentDidMount() {
  //   this.props.onRef(this)
  //  }
  //  componentWillUnmount() {
  //   this.props.onRef(undefined)
  //  }
  updateDialogValue(value) {
    console.log("do stuff " + value);
  }

  renderItem({ item, index }) {
    return (
      <TouchableOpacity
        onPress={() => this.props.method("ActiveBatchesDetail")}
        style={styles.renderItemStyle}
      >
        <View style={styles.itemStyle} key={index}>
          <View style={styles.itemSection1}>
            <Image
              style={styles.renderImageStyle}
              source={require("@assets/vessel.png")}
            />
            <View style={styles.renderSection1}>
              <Text style={styles.renderText1}>Mash Tun -mt1-</Text>
              <Text style={styles.renderText2}>170616.01.MT1</Text>
            </View>
            <View style={styles.renderSection2}>
              <Icon1 name="close" size={width * 0.02} color="#717171" />
            </View>
          </View>

          <View style={styles.itemSection2}>
            <Text style={styles.textSection1}>Vessel:</Text>
            <Text style={styles.textSection2}>Mash Ton</Text>
            <Text style={styles.textSection1}>Volume:</Text>
            <Text style={styles.textSection2}>368 Gallons</Text>
          </View>

          <View style={styles.itemSection3}>
            <Text style={styles.textSection3}>Age:</Text>
            <Text style={styles.textSection4}>28 Days, 19 hrs,38 ml</Text>
          </View>

          <View style={styles.itemSection4} />
          <View style={styles.itemSection5}>
            <View style={styles.viewSecion}>
              <Icon4 name="file-text-o" size={width * 0.012} color="#53939A" />
              <Text style={styles.textSection5}>View Detail</Text>
            </View>
            <View
              style={{
                width: "1%",
                height: "100%",
                flexDirection: "row",
                backgroundColor: "#ECECEC"
              }}
            />
            <View style={styles.viewSection1}>
              <Icon name="edit" size={width * 0.012} color="#53939A" />
              <Text style={styles.textSection5}>Edit</Text>
            </View>

            <View
              style={{
                width: "1%",
                height: "100%",
                flexDirection: "row",
                backgroundColor: "#ECECEC"
              }}
            />

            <View style={styles.viewSecion}>
              <Icon4 name="send-o" size={width * 0.012} color="#53939A" />
              <Text style={styles.textSection5}>Transfer</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    let data = [
      {
        value: "Specific"
      },
      {
        value: "Option 1"
      },
      {
        value: "Option 2"
      }
    ];

    return (
      <View style={styles.container}>
        <ScrollView
          scrollEnabled={!this.props.screenProps.isShow}
          style={{ marginLeft: "2%", marginRight: "2%", width: "100%" }}
        >
          {/* <Icon4 name="caret-up" size={30} color="red" style={{zIndex: 1,marginTop:-30,  alignSelf:'center',marginLeft:'85%'}} />
        */}
          {this.props.screenProps.isShow && (
            <View style={styles.viewPopup}>
              {/* FlatList ItemList View Starts */}

              <FlatList
                contentContainerStyle={styles.list}
                removeClippedSubviews={false}
                data={this.state.items}
                overScrollMode="auto"
                renderItem={this.renderItem.bind(this)}
              />
              {/* FlatList ItemList View ends */}
            </View>
          )}

          <View style={styles.view1}>
            <View style={styles.view2}>
              <Text style={styles.welcome}>BATCH DETAILS</Text>
              <View style={styles.headerActionView}>
                <View style={styles.btnBackground}>
                  <TouchableOpacity
                    onPress={() => this.props.showDialog("EditActiveBatch")}
                    style={styles.actionButton}
                  >
                    <View style={styles.buttonView}>
                      <Icon
                        name="edit"
                        size={width * 0.01}
                        color="#61AAAE"
                        style={{ marginTop: 1 }}
                      />
                      <Text style={styles.textStyle}>Edit</Text>
                    </View>
                  </TouchableOpacity>
                </View>

                <View style={styles.btnBackground}>
                  <TouchableOpacity
                    onPress={() => this.props.showDialog("DeleteBatch")}
                    style={styles.actionButton}
                  >
                    <View style={styles.buttonView}>
                      <Icon3
                        name="delete"
                        size={width * 0.011}
                        color="#61AAAE"
                      />
                      <Text style={styles.textStyle}>Delete</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <View style={styles.view10}>
              <View
                style={{
                  width: "18%",
                  height: "100%",
                  backgroundColor: "transparent",
                  flexDirection: "column"
                }}
              >
                <View
                  style={{
                    width: "100%",
                    height: "85%",
                    backgroundColor: "white",
                    flexDirection: "column",
                    alignItems: "flex-start"
                  }}
                >
                  <Image
                    style={{
                      width: width * 0.11,
                      height: width * 0.15,
                      marginLeft: 0,
                      alignSelf: "center"
                    }}
                    source={require("@assets/vessel_mash_tun_new.png")}
                  />
                  <Text
                    style={{
                      marginLeft: width * 0.003,
                      fontSize: width * 0.011,
                      color: "#4D4D4D",
                      alignSelf: "center",
                      fontWeight: "bold"
                    }}
                  >
                    Mash Ton-Mt1
                  </Text>
                  <Text
                    style={{
                      marginLeft: width * 0.003,
                      fontSize: width * 0.006,
                      color: "#D0D0D0",
                      alignSelf: "center",
                      fontWeight: "bold"
                    }}
                  >
                    ORIGINAL GRAVITY
                  </Text>
                  <Text style={styles.textStyle}>1,0920</Text>
                </View>
                <View
                  style={{
                    width: "100%",
                    height: "30%",
                    backgroundColor: "transparent",
                    flexDirection: "row"
                  }}
                />
              </View>

              <View style={styles.section2}>
                <View style={styles.section21}>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle2}>BATCH NAME</Text>
                  </View>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle21}>170815.10</Text>
                  </View>
                </View>

                <View style={styles.spaceView} />

                <View style={styles.section21}>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle2}>IN HOUSE BATCH NAME</Text>
                  </View>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle21}>DEMO</Text>
                  </View>
                </View>

                <View style={styles.spaceView} />

                <View style={styles.section21}>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle2}>BATCH START DATE</Text>
                  </View>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle21}>5/11/2017 07:22 AM</Text>
                  </View>
                </View>

                <View style={styles.spaceView} />
                <View style={styles.section21}>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle2}>
                      BATCH COMPLETED DATE TIME
                    </Text>
                  </View>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle21}>15/11/2017 07:22 AM</Text>
                  </View>
                </View>

                <View style={styles.spaceView} />
                <View style={styles.section21}>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle2}>BATCH AGE</Text>
                  </View>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle21}>
                      07 D | 02hrs | 43 mins
                    </Text>
                  </View>
                </View>

                <View style={styles.spaceView} />
                <View style={styles.section21}>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle2}>STATUS</Text>
                  </View>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle21}>Completed</Text>
                  </View>
                </View>

                <View style={styles.spaceView} />
              </View>

              {/*second view*/}

              <View style={styles.section2}>
                <View style={styles.section21}>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle2}>STATUS</Text>
                  </View>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle21}>Active</Text>
                  </View>
                </View>

                <View style={styles.spaceView} />

                <View style={styles.section21}>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle2}>CLASS/TYPE</Text>
                  </View>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle21}>Bourbon Whishkey</Text>
                  </View>
                </View>

                <View style={styles.spaceView} />

                <View style={styles.section21}>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle2}>STAGE START DATE</Text>
                  </View>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle21}>5/11/2017 07:22 AM</Text>
                  </View>
                </View>

                <View style={styles.spaceView} />
                <View style={styles.section21}>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle2}>
                      STAGE COMPLETED DATE TIME
                    </Text>
                  </View>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle21}>15/11/2017 07:22 AM</Text>
                  </View>
                </View>

                <View style={styles.spaceView} />
                <View style={styles.section21}>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle2}>STAGE AGE</Text>
                  </View>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle21}>
                      07 D | 02hrs | 43 mins
                    </Text>
                  </View>
                </View>

                <View style={styles.spaceView} />
                <View style={styles.section21}>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle2}>ACCOUNT</Text>
                  </View>
                  <View style={styles.section23}>
                    <Text style={styles.textStyle21}>Production</Text>
                  </View>
                </View>

                <View style={styles.spaceView} />
              </View>
            </View>

            <View style={styles.divider} />
            <View style={styles.bottomAction}>
              <View style={[styles.btnBackground1, { width: width * 0.08 }]}>
                <TouchableOpacity
                  onPress={() => this.props.showDialog("Transfer")}
                  style={styles.actionButton}
                >
                  <View style={styles.buttonView}>
                    <Icon4
                      name="send-o"
                      size={width * 0.01}
                      color="#61AAAE"
                      style={{ marginTop: width * 0.0 }}
                    />
                    <Text style={styles.textStyle1}>Transfer</Text>
                  </View>
                </TouchableOpacity>
              </View>

              <View style={[styles.btnBackground1, { width: width * 0.08 }]}>
                <TouchableOpacity
                  onPress={() => this.props.showDialog("Record")}
                  style={styles.actionButton}
                >
                  <View style={styles.buttonView}>
                    <Icon5
                      name="record"
                      size={width * 0.012}
                      color="#61AAAE"
                      style={{ marginTop: 0 }}
                    />
                    <Text style={styles.textStyle1}> Record </Text>
                  </View>
                </TouchableOpacity>
              </View>

              <View style={[styles.btnBackground1, { width: width * 0.1 }]}>
                <TouchableOpacity
                  onPress={() => this.props.showDialog("AddIngredients")}
                  style={styles.actionButton}
                >
                  <View style={styles.buttonView}>
                    <Icon2
                      name="md-add-circle"
                      size={width * 0.011}
                      color="#61AAAE"
                      style={{ marginTop: 1 }}
                    />
                    <Text style={styles.textStyle1}> Add Ingredient </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={[styles.btnBackground1, { width: width * 0.07 }]}>
                <TouchableOpacity
                  onPress={() => this.props.showDialog("AddNote")}
                  style={styles.actionButton}
                >
                  <View style={styles.buttonView}>
                    <Icon5
                      name="clipboard-notes"
                      size={width * 0.01}
                      color="#61AAAE"
                      style={{ marginTop: 1 }}
                    />
                    <Text style={styles.textStyle1}> Add Note</Text>
                  </View>
                </TouchableOpacity>
              </View>

              <View style={[styles.btnBackground1, { width: width * 0.1 }]}>
                <TouchableOpacity
                  onPress={() => this.props.showDialog("DumpTowaste")}
                  style={styles.actionButton}
                >
                  <View style={styles.buttonView}>
                    <Icon3
                      name="content-copy"
                      size={width * 0.01}
                      color="#61AAAE"
                      style={{ marginTop: 1 }}
                    />
                    <Text style={styles.textStyle1}> Dump to waste</Text>
                  </View>
                </TouchableOpacity>
              </View>

              <View style={[styles.btnBackground1, { width: width * 0.1 }]}>
                <TouchableOpacity
                  onPress={() => this.props.showDialog("ProofSpirits")}
                  style={styles.actionButton}
                >
                  <View style={styles.buttonView}>
                    <Icon3
                      name="content-copy"
                      size={width * 0.01}
                      color="#61AAAE"
                      style={{ marginTop: 1 }}
                    />
                    <Text style={styles.textStyle1}> Proof Spirit</Text>
                  </View>
                </TouchableOpacity>
              </View>

              <View style={[styles.btnBackground1, { width: width * 0.075 }]}>
                <TouchableOpacity
                  onPress={() => this.props.showDialog("GaugeSpirits")}
                  style={styles.actionButton}
                >
                  <View style={styles.buttonView}>
                    <Icon3
                      name="content-copy"
                      size={width * 0.01}
                      color="#61AAAE"
                      style={{ marginTop: 1 }}
                    />
                    <Text style={styles.textStyle1}> Gause Spirit</Text>
                  </View>
                </TouchableOpacity>
              </View>

              <View style={[styles.btnBackground1, { width: width * 0.07 }]}>
                <TouchableOpacity
                  onPress={() => this.props.showDialog("FillBarrel")}
                  style={styles.actionButton}
                >
                  <View style={styles.buttonView}>
                    <Icon3
                      name="content-copy"
                      size={width * 0.01}
                      color="#61AAAE"
                      style={{ marginTop: 1 }}
                    />
                    <Text style={styles.textStyle1}>Fill Barrels</Text>
                  </View>
                </TouchableOpacity>
              </View>

              <View style={[styles.btnBackground1, { width: width * 0.075 }]}>
                <TouchableOpacity
                  onPress={() => this.props.showDialog("Withdraw")}
                  style={styles.actionButton}
                >
                  <View style={styles.buttonView}>
                    <Icon5
                      name="record"
                      size={width * 0.012}
                      color="#61AAAE"
                      style={{ marginTop: 0 }}
                    />
                    <Text style={styles.textStyle1}> Withdraw </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View
            style={{
              width: "98%",
              height: width * 0.35,
              backgroundColor: "transparent",
              justifyContent: "flex-start",
              marginTop: width * 0.02,
              alignSelf: "center"
            }}
          >
            <View style={styles.view2}>
              <Text style={styles.welcome}>BATCH HISTORY</Text>
            </View>
            <View style={styles.view3}>
              {/*Header view starts*/}
              <View
                style={{
                  width: "35%",
                  height: "100%",
                  backgroundColor: "white",
                  flexDirection: "row",
                  alignItems: "center",
                  paddingLeft: 5,
                  paddingRight: 2,
                  justifyContent: "space-between"
                }}
              >
                {/*Date and Time button*/}
                <Dropdown
                  value="Date & Time"
                  data={data}
                  style={{ fontSize: 13 }}
                  inputContainerStyle={{ borderBottomColor: "transparent" }}
                  pickerStyle={{ marginTop: 90 }}
                  containerStyle={{
                    width: "50%",
                    alignSelf: "center",
                    marginTop: -20,
                    marginLeft: 5
                  }}
                />

                {/* Search  button */}

                <Dropdown
                  value="All"
                  data={data}
                  style={{ fontSize: 13 }}
                  inputContainerStyle={{ borderBottomColor: "transparent" }}
                  pickerStyle={{ marginTop: 90 }}
                  containerStyle={{
                    width: "30%",
                    alignSelf: "center",
                    marginTop: -20,
                    marginLeft: 5
                  }}
                />
              </View>

              <View
                style={{
                  width: "65%",
                  height: "100%",
                  backgroundColor: "white",
                  flexDirection: "row"
                }}
              >
                <View
                  style={{
                    width: "50%",
                    height: "100%",
                    backgroundColor: "transparent"
                  }}
                >
                  {/*Detail button*/}
                  <View
                    style={{
                      width: "35%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      alignItems: "center",
                      paddingLeft: "10%"
                    }}
                  >
                    <Text style={{ fontSize: 15, fontWeight: "bold" }}>
                      Details
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    width: "50%",
                    height: "100%",
                    backgroundColor: "transparent",
                    paddingTop: "0.5%"
                  }}
                >
                  {/*Staff all*/}

                  <Dropdown
                    value="Staff All"
                    data={data}
                    style={{ fontSize: 13 }}
                    inputContainerStyle={{ borderBottomColor: "transparent" }}
                    pickerStyle={{ marginTop: 90 }}
                    containerStyle={{
                      width: "30%",
                      alignSelf: "flex-start",
                      marginTop: -34,
                      marginLeft: 5
                    }}
                  />
                </View>
              </View>
              {/*Header view ends*/}
            </View>

            <View style={styles.spaceView} />
            {/*item view starts*/}
            <View style={styles.view3}>
              <View
                style={{
                  width: "35%",
                  height: "100%",
                  backgroundColor: "white",
                  flexDirection: "row",
                  alignItems: "center",
                  paddingLeft: 5,
                  paddingRight: 2,
                  justifyContent: "space-between"
                }}
              >
                {/*TimeEntry*/}
                <View
                  style={{
                    width: "65%",
                    height: "100%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    alignItems: "center",
                    marginLeft: 10
                  }}
                >
                  <Text
                    style={{
                      fontSize: width * 0.009,
                      fontWeight: "normal",
                      marginLeft: "1%"
                    }}
                  >
                    6/16/2017 09:39 AM
                  </Text>
                </View>
                <View
                  style={{
                    width: "35%",
                    height: "100%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      fontSize: width * 0.009,
                      fontWeight: "normal",
                      marginRight: "50%",
                      marginLeft: "7%"
                    }}
                  >
                    All
                  </Text>
                </View>
              </View>

              <View
                style={{
                  width: "55%",
                  height: "100%",
                  backgroundColor: "white",
                  flexDirection: "row"
                }}
              >
                {/*TimeEntry*/}
                <View
                  style={{
                    width: "45%",
                    height: "100%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    alignItems: "center",
                    marginLeft: "6%"
                  }}
                >
                  <Text
                    style={{
                      fontSize: width * 0.009,
                      fontWeight: "normal",
                      marginRight: "50%",
                      marginLeft: "1%"
                    }}
                  >
                    ph 12 0000
                  </Text>
                </View>
                <View
                  style={{
                    width: "30%",
                    height: "100%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    alignItems: "center",
                    marginLeft: "8%"
                  }}
                >
                  <Text
                    style={{
                      fontSize: width * 0.009,
                      fontWeight: "normal",
                      marginLeft: "2%"
                    }}
                  >
                    All
                  </Text>
                </View>
              </View>

              <View
                style={{
                  width: "10%",
                  height: "100%",
                  backgroundColor: "transparent",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  paddingLeft: "2%",
                  paddingRight: "2%"
                }}
              >
                <TouchableOpacity
                  onPress={() => this.props.showDialog("EditActiveBatch")}
                  style={styles.actionButton1}
                >
                  <Icon
                    name="edit"
                    size={width * 0.015}
                    color="#787878"
                    style={{ marginTop: width * 0.002 }}
                  />
                </TouchableOpacity>

                <View
                  style={{
                    backgroundColor: "grey",
                    height: "80%",
                    width: 1,
                    alignSelf: "center"
                  }}
                />

                <TouchableOpacity
                  onPress={() => this.props.showDialog("DeleteBatchHistory")}
                  style={styles.actionButton1}
                >
                  <Icon3
                    name="delete"
                    size={width * 0.017}
                    color="#787878"
                    style={{ marginTop: width * 0.002 }}
                  />
                </TouchableOpacity>
              </View>
            </View>

            {/*item view ends*/}

            <View style={styles.spaceView} />
            {/*item view starts*/}
            <View style={styles.view3}>
              <View
                style={{
                  width: "35%",
                  height: "100%",
                  backgroundColor: "white",
                  flexDirection: "row",
                  alignItems: "center",
                  paddingLeft: 5,
                  paddingRight: 2,
                  justifyContent: "space-between"
                }}
              >
                {/*TimeEntry*/}
                <View
                  style={{
                    width: "65%",
                    height: "100%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    alignItems: "center",
                    marginLeft: 10
                  }}
                >
                  <Text
                    style={{
                      fontSize: width * 0.009,
                      fontWeight: "normal",
                      marginRight: "0%",
                      marginLeft: "1%"
                    }}
                  >
                    6/16/2017 09:39 AM
                  </Text>
                </View>
                <View
                  style={{
                    width: "35%",
                    height: "100%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      fontSize: width * 0.009,
                      fontWeight: "normal",
                      marginLeft: "6%"
                    }}
                  >
                    All
                  </Text>
                </View>
              </View>

              <View
                style={{
                  width: "55%",
                  height: "100%",
                  backgroundColor: "transparent",
                  flexDirection: "row",
                  justifyContent: "flex-start"
                }}
              >
                {/*TimeEntry*/}
                <View
                  style={{
                    width: "45%",
                    height: "100%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    alignItems: "center",
                    marginLeft: "6%"
                  }}
                >
                  <Text
                    style={{
                      fontSize: width * 0.009,
                      fontWeight: "normal",
                      marginLeft: "1%"
                    }}
                  >
                    Specific Gravity 15550
                  </Text>
                </View>
                <View
                  style={{
                    width: "30%",
                    height: "100%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    alignItems: "center",
                    marginLeft: "8%"
                  }}
                >
                  <Text
                    style={{
                      fontSize: width * 0.009,
                      fontWeight: "normal",
                      marginLeft: "2%"
                    }}
                  >
                    Huge Cubs
                  </Text>
                </View>
              </View>

              <View
                style={{
                  width: "10%",
                  height: "100%",
                  backgroundColor: "transparent",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  paddingLeft: "2%",
                  paddingRight: "2%"
                }}
              >
                <TouchableOpacity
                  onPress={() => this.props.showDialog("EditActiveBatch")}
                  style={styles.actionButton1}
                >
                  <Icon
                    name="edit"
                    size={width * 0.015}
                    color="#787878"
                    style={{ marginTop: width * 0.002 }}
                  />
                </TouchableOpacity>

                <View
                  style={{
                    backgroundColor: "grey",
                    height: "80%",
                    width: 1,
                    alignSelf: "center"
                  }}
                />

                <TouchableOpacity
                  onPress={() => this.props.showDialog("EditActiveBatch")}
                  style={styles.actionButton1}
                >
                  <Icon3
                    name="delete"
                    size={width * 0.017}
                    color="#787878"
                    style={{ marginTop: width * 0.002 }}
                  />
                </TouchableOpacity>
              </View>
            </View>

            {/*item view ends*/}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "85%",
    flexDirection: "row",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F1ECEC"
  },
  welcome: {
    alignSelf: "flex-start",
    fontSize: width * 0.01,
    textAlign: "center",
    color: "#2FACAB",
    fontWeight: "bold",
    margin: width * 0.01
  },
  bottomAction: {
    width: "100%",
    height: "10%",
    backgroundColor: "#FFFFFF",
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center"
  },
  mainContainer: {
    width: "92%",
    height: "100%",
    backgroundColor: "green",
    flexDirection: "column"
  },
  header: {
    backgroundColor: "#455A64"
  },

  section2: {
    width: "41%",
    height: "100%",
    backgroundColor: "white",
    flexDirection: "column"
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  spaceView: {
    width: "100%",
    height: 1,
    backgroundColor: "#A4A4A4",
    flexDirection: "column"
  },
  textStyle21: {
    marginLeft: width * 0.003,
    fontSize: width * 0.01,
    color: "#4D4D4D",
    alignSelf: "flex-start",
    fontWeight: "bold"
  },
  textStyle2: {
    marginLeft: width * 0.003,
    fontSize: width * 0.009,
    color: "#A4A4A4",
    alignSelf: "flex-start",
    fontWeight: "bold"
  },
  btnBackground: {
    marginRight: 5,
    marginLeft: 5,
    marginTop: 7,
    paddingTop: 2,
    paddingBottom: 3,
    backgroundColor: "#E6E6E6",
    borderRadius: width * 0.03,
    borderWidth: 1,
    borderColor: "#fff",
    width: width * 0.06,
    height: width * 0.02,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center"
  },

  btnBackground: {
    marginRight: 5,
    marginLeft: 5,
    marginTop: 7,
    paddingTop: 2,
    paddingBottom: 3,
    backgroundColor: "#E6E6E6",
    borderRadius: width * 0.03,
    borderWidth: 1,
    borderColor: "#fff",
    width: width * 0.06,
    height: width * 0.02,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  btnBackground2: {
    marginRight: 5,
    marginLeft: 5,
    marginTop: 0,
    paddingTop: 2,
    paddingBottom: 3,
    backgroundColor: "#fff",
    borderRadius: width * 0.03,
    borderWidth: 1,
    borderColor: "#E6E6E6",
    width: width * 0.1,
    height: width * 0.025,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },

  divider: {
    width: "100%",
    height: "0.5%",
    backgroundColor: "#E6E6E6",
    flexDirection: "row"
  },
  view10: {
    width: "100%",
    height: "79.5%",
    backgroundColor: "#FFFFFF",
    flexDirection: "row",
    alignSelf: "center"
  },
  textStyle: {
    marginLeft: width * 0.003,
    fontSize: width * 0.009,
    color: "#61AAAE",
    alignSelf: "center",
    fontWeight: "bold"
  },
  textStyle1: {
    marginLeft: width * 0.003,
    fontSize: width * 0.008,
    color: "#61AAAE",
    alignSelf: "center",
    fontWeight: "bold"
  },
  view1: {
    width: "98%",
    height: width * 0.35,
    backgroundColor: "red",
    flexDirection: "column",
    alignSelf: "center"
  },
  viewPopup: {
    zIndex: -1,
    width: "98%",
    height: "28%",
    backgroundColor: "#666666",
    flexDirection: "column",
    alignSelf: "center"
  },
  view2: {
    width: "100%",
    height: "11%",
    backgroundColor: "#E6E6E6",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  view3: {
    width: "100%",
    height: "09%",
    backgroundColor: "white",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  headerActionView: {
    width: "18%",
    height: "100%",
    backgroundColor: "transparent",
    flexDirection: "row",
    alignSelf: "flex-end",
    alignItems: "center",
    justifyContent: "space-between"
  },
  actionButton: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "center",
    padding: "0.2%"
  },
  actionButton1: {
    width: "50%",
    height: "100%",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "center"
  },
  buttonView: {
    width: "100%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "center"
  },
  section21: {
    height: "16.66%",
    width: "100%",
    backgroundColor: "white",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  section23: {
    width: "50%",
    height: "100%",
    backgroundColor: "transparent",
    flexDirection: "column",
    justifyContent: "center"
  },

  btnBackground1: {
    marginRight: width * 0.005,
    marginLeft: width * 0.005,
    marginTop: width * 0.001,
    paddingTop: width * 0.002,
    paddingBottom: width * 0.002,
    backgroundColor: "#FFF",
    borderRadius: width * 0.05,
    borderWidth: width * 0.001,
    borderColor: "#61AAAE",
    width: width * 0.06,
    height: width * 0.02,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  actionView: {
    width: "25%",
    height: "100%",
    flexDirection: "row",
    padding: 5,
    justifyContent: "space-between"
  },
  actionViewMain: {
    width: "10%",
    height: "100%",
    flexDirection: "row",
    backgroundColor: "white",
    justifyContent: "center"
  },
  titleMain: {
    fontSize: width * 0.013,
    color: "#4E9197",
    alignSelf: "center",
    fontWeight: "normal"
  },
  titleSub: {
    fontSize: width * 0.011,
    color: "#848484",
    alignSelf: "center",
    fontWeight: "bold"
  },
  titleSub1: {
    fontSize: width * 0.011,
    color: "#ACACAC",
    alignSelf: "center",
    marginRight: 10
  },
  spaceview: {
    width: "0.5%",
    height: "100%",
    flexDirection: "row",
    backgroundColor: "#F1ECEC"
  },
  itemSection1: {
    width: "100%",
    height: "40%",
    flexDirection: "row",
    backgroundColor: "transparent"
  },
  itemSection2: {
    width: "100%",
    height: "15%",
    flexDirection: "row",
    backgroundColor: "transparent"
  },
  itemSection3: {
    width: "100%",
    height: "15%",
    flexDirection: "row",
    backgroundColor: "transparent"
  },
  itemSection4: {
    marginTop: "5%",
    width: "100%",
    height: "1%",
    flexDirection: "row",
    backgroundColor: "#ECECEC"
  },
  itemSection5: {
    width: "100%",
    height: "20%",
    flexDirection: "row",
    backgroundColor: "transparent"
  },
  renderItemStyle: {
    flex: 1,
    minWidth: width * 0.22,
    maxWidth: width * 0.22,
    height: width * 0.18,
    maxHeight: width * 0.18,
    padding: width * 0.01
  },
  renderImageStyle: {
    marginTop: width * 0.01,
    width: "25%",
    height: "60%",
    resizeMode: "contain",
    alignSelf: "flex-start"
  },
  renderSection1: {
    width: "50%",
    height: "100%",
    padding: width * 0.005,
    flexDirection: "column",
    justifyContent: "center"
  },
  renderText1: {
    fontSize: width * 0.007,
    color: "#848484",
    fontWeight: "bold"
  },
  renderText2: {
    fontSize: width * 0.007,
    color: "#ACACAC",
    marginRight: width * 0.007,
    fontWeight: "bold"
  },
  renderSection2: {
    width: "25%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: width * 0.005
  },
  textSection1: {
    width: "20%",
    height: "100%",
    fontSize: width * 0.007,
    color: "#000",
    fontWeight: "bold"
  },

  textSection2: {
    width: "30%",
    height: "100%",
    fontSize: width * 0.007,
    color: "#ACACAC",
    marginRight: width * 0.007,
    fontWeight: "bold"
  },
  textSection3: {
    width: "20%",
    height: "100%",
    fontSize: width * 0.007,
    color: "#000",
    fontWeight: "bold"
  },
  textSection4: {
    width: "80%",
    height: "100%",
    fontSize: width * 0.007,
    color: "#ACACAC",
    marginRight: width * 0.01,
    fontWeight: "bold"
  },
  textSection5: {
    fontSize: width * 0.007,
    color: "#53939A",
    fontWeight: "bold",
    marginLeft: width * 0.004,
    alignSelf: "center"
  },
  viewSecion: {
    width: "37%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: width * 0.005
  },
  viewSection1: {
    width: "25%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: width * 0.005
  },

  list: {
    justifyContent: "center",
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "center"
  },

  mainContainer: {
    width: "92%",
    height: "100%",
    backgroundColor: "green",
    flexDirection: "column"
  },
  header: {
    backgroundColor: "#455A64"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  itemStyle: {
    width: "100%",
    height: "100%",
    flexDirection: "column",
    backgroundColor: "white",

    paddingLeft: width * 0.008,
    paddingRight: width * 0.008,
    borderWidth: 0.5,
    borderRadius: width * 0.008,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#E8E5E5",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1
  }
});
