import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  StatusBar,
  KeyboardAvoidingView,
  View
} from "react-native";

import PopupDialog, {
  DialogTitle,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
  FadeAnimation
} from "react-native-popup-dialog";

import ActiveBatches from "@screen/operationManagment/activeBatches";
import CompletedStages from "@screen/operationManagment/completedStage";
import InBondTransfer from "@screen/operationManagment/inBondTransfer/inBondTransfer";
import Overview from "@screen/operationManagment/overView";
import Packages from "@screen/operationManagment/packages/package";
import PackageDetail from "@screen/operationManagment/packages/packageDetail";
import TransferIn from "@screen/operationManagment/inBondTransfer/transferIn";
import TransferOut from "@screen/operationManagment/inBondTransfer/transferOut";
import ActiveBatchesDetail from "@screen/operationManagment/activeBatchesDetail";

import BottomNavigtion from "@screen/operationManagment/bottomNavigation";
import Header from "@screen/operationManagment/header";

import { TabRouter, StackNavigator } from "react-navigation";

const TabRoute = TabRouter(
  {
    Overview: { screen: Overview },
    ActiveBatches: { screen: ActiveBatches },
    InBondTransfer: { screen: InBondTransfer },
    CompletedStages: { screen: CompletedStages },
    TransferIn: { screen: TransferIn },
    TransferOut: { screen: TransferOut },
    ActiveBatchesDetail: { screen: ActiveBatchesDetail },
    Packages: { screen: Packages },
    PackageDetail: { screen: PackageDetail }
  },
  {
    initialRouteName: "Overview"
  }
);

class TabContentNavigator extends Component {
  constructor(props, context) {
    super(props, context);
    console.log(props);
    this.state = {
      active: props.value.active,
      method: props.method,
      showDialog: props.showDialog,
      isShow: this.props.seeAllBatches
    };
  }

  //this method will not get called first time
  componentWillReceiveProps(newProps) {
    console.log(newProps);
    this.setState({
      active: newProps.value.active,
      isShow: newProps.isShow
    });
  }

  render() {
    const Component = TabRoute.getComponentForRouteName(this.state.active);
    return (
      <Component
        ref={ref => this.props._updateComponent(ref)}
        screenProps={this.state}
        method={this.props.method}
        showDialog={this.props.showDialog}
      />
    );
  }
}

export default class OperationManagment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "Overview",
      dilaog: "",
      seeAllBatches: false
    };
  }

  _selectTab = key => {
    this.setState({ active: key });
    this.setState({ seeAllBatches: false });
  };

  _navigateTo = key => {
    this.setState({ active: key });
  };

  _updateDialogData = value => {
    console.log("child home page called " + value);
    this.child.updateDialogValue(value);
  };

  _seeAllBatches = isShow => {
    this.setState({ seeAllBatches: isShow });
  };
  _showDialog = (key, input) => {
    //  console.log('Input data Called ' + input)
    this.props.showDialog(key, input);
  };

  _updateComponent = ref => {
    this.child = ref;
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar hidden={true} />
        <Header value={this.state} _seeAllBatches={this._seeAllBatches} />
        <TabContentNavigator
          value={this.state}
          method={this._navigateTo}
          showDialog={this._showDialog}
          isShow={this.state.seeAllBatches}
          _updateComponent={this._updateComponent}
        />
        <BottomNavigtion method={this._selectTab} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "92%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF",
    flexDirection: "column"
  },

  mainContainer: {
    width: "92%",
    height: "100%",
    backgroundColor: "green",
    flexDirection: "column"
  },
  header: {
    backgroundColor: "#455A64"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
