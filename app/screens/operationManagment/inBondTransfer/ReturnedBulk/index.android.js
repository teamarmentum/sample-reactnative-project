import React, { Component } from "react";
import { AppRegistry, StyleSheet, Text, StatusBar, View } from "react-native";

export default class ReturnedBulk extends Component {
  // componentDidMount() {
  //   this.props.onRef(this)
  //  }
  //  componentWillUnmount() {
  //   this.props.onRef(undefined)
  //  }
  updateDialogValue(value) {
    console.log("do stuff " + value);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to ReturnedBulk.</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "85%",
    flexDirection: "row",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },

  mainContainer: {
    width: "92%",
    height: "100%",
    backgroundColor: "green",
    flexDirection: "column"
  },
  header: {
    backgroundColor: "#455A64"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
