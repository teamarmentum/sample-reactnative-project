import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  StatusBar,
  TouchableOpacity,
  Dimensions,
  TextInput,
  View
} from "react-native";
import { Switch } from "react-native-switch";

var { height, width } = Dimensions.get("window");
import { TextInputMask } from "react-native-masked-text";
import Icon from "react-native-vector-icons/dist/FontAwesome";
import LinearGradient from "react-native-linear-gradient";
export default class DSP extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      time: true
    };
  }

  _toggel = () => {
    this.setState({ time: !this.state.time });
  };

  // componentDidMount() {
  //   this.props.onRef(this)
  //  }
  //  componentWillUnmount() {
  //   this.props.onRef(undefined)
  //  }
  updateDialogValue(value) {
    console.log("do stuff " + value);
  }

  isValid() {
    // isValid method returns if the inputed value is valid.
    // Ex: if you input 40/02/1990 30:20:20, it will return false
    //	   because in this case, the day and the hour is invalid.
    let valid = this.refs["myDateText"].isValid();

    // get converted value. Using type=datetime, it returns the moment object.
    // If it's using type=money, it returns a Number object.
    let rawValue = this.refs["myDateText"].getRawValue();
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            width: "98%",
            height: "5%",
            backgroundColor: "white",
            flexDirection: "row"
          }}
        >
          <View
            style={{
              width: "33.33%",
              height: "100%",
              backgroundColor: "white",
              flexDirection: "row",
              justifyContent: "space-between",
              paddingLeft: "2%",
              paddingRight: "2%"
            }}
          >
            <Text
              style={{ fontSize: 15, alignSelf: "center", color: "#767676" }}
            >
              TRANSFER DATE
            </Text>
            <TextInputMask
              ref={"myDateText"}
              type={"datetime"}
              placeholder="mm/dd/yyyy"
              style={{ width: "30%", height: "100%" }}
              options={{ format: "DD/MM/YYYY" }}
            />
          </View>
          <View
            style={{
              width: "33.33%",
              height: "100%",
              backgroundColor: "white",
              flexDirection: "row",
              justifyContent: "center",
              paddingLeft: "2%",
              paddingRight: "2%"
            }}
          >
            <Text
              style={{ fontSize: 15, alignSelf: "center", color: "#767676" }}
            >
              TRANSFER DATE
            </Text>
            <TextInputMask
              ref={"myDateText"}
              type={"datetime"}
              placeholder="08:55"
              style={{
                width: "19%",
                height: "100%",
                alignSelf: "center",
                justifyContent: "center",
                flexDirection: "row",
                marginLeft: 15
              }}
              options={{ format: "hh:mm" }}
            />
            <TouchableOpacity
              onPress={() => this._toggel()}
              tyle={{ marginLeft: 1, justifyContent: "center" }}
            >
              <Text
                style={{
                  fontSize: 15,
                  alignSelf: "center",
                  color: "#767676",
                  marginTop: 20
                }}
              >
                {this.state.time ? "AM" : "PM"}
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: "33.33%",
              height: "100%",
              backgroundColor: "transparent",
              flexDirection: "row"
            }}
          />
        </View>

        <View
          style={{
            width: "98%",
            height: 400,
            backgroundColor: "#E6E6E6",
            flexDirection: "column"
          }}
        >
          <View
            style={{
              width: "100%",
              height: 40,
              backgroundColor: "white",
              flexDirection: "column",
              justifyContent: "flex-start",
              alignItems: "flex-start"
            }}
          >
            <Text
              style={{
                fontSize: 18,
                alignSelf: "flex-start",
                color: "#767676",
                marginLeft: 10,
                fontWeight: "bold"
              }}
            >
              Consigner Information
            </Text>
          </View>
          <View
            style={{
              width: "100%",
              height: "100%",
              backgroundColor: "transparent",
              flexDirection: "row",
              justifyContent: "flex-start",
              alignItems: "flex-start"
            }}
          >
            <View
              style={{
                width: "50%",
                height: "100%",
                backgroundColor: "#E6E6E6",
                flexDirection: "column",
                justifyContent: "flex-start",
                alignItems: "center"
              }}
            >
              <View
                style={{
                  width: "100%",
                  height: "12%",
                  flexDirection: "row",
                  justifyContent: "space-between"
                }}
              >
                <Icon
                  name="search"
                  size={width * 0.015}
                  color="#717171"
                  style={{ marginLeft: 30, alignSelf: "center" }}
                />
                <TextInput
                  style={{
                    width: "90%",
                    marginLeft: width * 0.001,
                    fontSize: width * 0.01
                  }}
                  onChangeText={text => this.setState({ search: text })}
                  placeholder="Search"
                  underlineColorAndroid="rgba(0,0,0,0)"
                />
              </View>
              <View
                style={{
                  width: "98%",
                  height: "0.5%",
                  backgroundColor: "#C7C7C7",
                  flexDirection: "row",
                  justifyContent: "center"
                }}
              />

              <View
                style={{
                  width: "100%",
                  height: "13%",
                  backgroundColor: "#E6E6E6",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginTop: 5
                }}
              >
                <Text
                  style={{
                    fontSize: 12,
                    alignSelf: "center",
                    color: "#767676",
                    marginLeft: 10,
                    fontWeight: "bold"
                  }}
                >
                  APPLICATION SERIAL NUMBER
                </Text>
                <TextInput
                  style={{
                    width: "50%",
                    marginLeft: width * 0.055,
                    fontSize: width * 0.009
                  }}
                  onChangeText={text => this.setState({ search: text })}
                  placeholder="Enter Application Number"
                  underlineColorAndroid="rgba(0,0,0,0)"
                />
              </View>
              <View
                style={{
                  width: "98%",
                  height: "0.5%",
                  backgroundColor: "#C7C7C7",
                  flexDirection: "row",
                  justifyContent: "center"
                }}
              />

              <View
                style={{
                  width: "100%",
                  height: 200,
                  backgroundColor: "transparent",
                  flexDirection: "column",
                  justifyContent: "center",
                  marginTop: 10
                }}
              >
                <Text
                  style={{
                    fontSize: 12,
                    alignSelf: "flex-start",
                    color: "#767676",
                    marginLeft: 10,
                    fontWeight: "bold"
                  }}
                >
                  BILLING ADDRESS
                </Text>
                <TextInput
                  style={{
                    textAlignVertical: "top",
                    width: "100%",
                    height: "80%",
                    marginLeft: width * 0.004,
                    fontSize: width * 0.009,
                    marginTop: 5
                  }}
                  onChangeText={text => this.setState({ search: text })}
                  placeholder="Enter Application Number"
                  underlineColorAndroid="rgba(0,0,0,0)"
                />
              </View>
              <View
                style={{
                  width: "98%",
                  height: "0.5%",
                  backgroundColor: "#C7C7C7",
                  flexDirection: "row",
                  justifyContent: "center"
                }}
              />
            </View>

            <View
              style={{
                width: "50%",
                height: "100%",
                backgroundColor: "#E6E6E6",
                flexDirection: "column",
                justifyContent: "flex-start",
                alignItems: "center"
              }}
            >
              <View
                style={{
                  width: "100%",
                  height: "11%",
                  backgroundColor: "#E6E6E6",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginTop: 5
                }}
              >
                <Text
                  style={{
                    fontSize: 12,
                    alignSelf: "center",
                    color: "#767676",
                    marginLeft: 10,
                    fontWeight: "bold",
                    marginTop: 6
                  }}
                >
                  APPLICATION APPROVAL DATE
                </Text>
                <TextInputMask
                  ref={"myDateText"}
                  type={"datetime"}
                  placeholder="mm/dd/yyyy"
                  style={{ width: "50%", height: "100%", marginTop: 7 }}
                  options={{ format: "DD/MM/YYYY" }}
                />
              </View>
              <View
                style={{
                  width: "98%",
                  height: "0.5%",
                  backgroundColor: "#C7C7C7",
                  flexDirection: "row",
                  justifyContent: "center"
                }}
              />

              <View
                style={{
                  width: "100%",
                  height: "13%",
                  backgroundColor: "#E6E6E6",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginTop: 5
                }}
              >
                <Text
                  style={{
                    fontSize: 12,
                    alignSelf: "center",
                    color: "#767676",
                    marginLeft: 10,
                    fontWeight: "bold"
                  }}
                >
                  APPLICATION SERIAL NUMBER
                </Text>
                <TextInput
                  style={{
                    width: "50%",
                    marginLeft: width * 0.055,
                    fontSize: width * 0.009
                  }}
                  onChangeText={text => this.setState({ search: text })}
                  placeholder="Enter DSP Number"
                  underlineColorAndroid="rgba(0,0,0,0)"
                />
              </View>
              <View
                style={{
                  width: "98%",
                  height: "0.5%",
                  backgroundColor: "#C7C7C7",
                  flexDirection: "row",
                  justifyContent: "center"
                }}
              />

              <View
                style={{
                  width: "100%",
                  height: 200,
                  backgroundColor: "transparent",
                  flexDirection: "column",
                  justifyContent: "center",
                  marginTop: 10
                }}
              >
                <Text
                  style={{
                    fontSize: 12,
                    alignSelf: "flex-start",
                    color: "#767676",
                    marginLeft: 10,
                    fontWeight: "bold"
                  }}
                >
                  SHIPPING ADDRESS
                </Text>
                <TextInput
                  style={{
                    textAlignVertical: "top",
                    width: "100%",
                    height: "80%",
                    marginLeft: width * 0.004,
                    fontSize: width * 0.009,
                    marginTop: 5
                  }}
                  onChangeText={text => this.setState({ search: text })}
                  placeholder="Enter Application Number"
                  underlineColorAndroid="rgba(0,0,0,0)"
                />
              </View>
              <View
                style={{
                  width: "98%",
                  height: "0.5%",
                  backgroundColor: "#C7C7C7",
                  flexDirection: "row",
                  justifyContent: "center"
                }}
              />
            </View>
          </View>
        </View>

        <View
          style={{
            width: "98%",
            height: 200,
            backgroundColor: "#E6E6E6",
            flexDirection: "column",
            marginTop: 30
          }}
        >
          <View
            style={{
              width: "100%",
              height: 40,
              backgroundColor: "white",
              flexDirection: "column",
              justifyContent: "flex-start",
              alignItems: "flex-start"
            }}
          >
            <Text
              style={{
                fontSize: 18,
                alignSelf: "flex-start",
                color: "#767676",
                marginLeft: 10,
                fontWeight: "bold"
              }}
            >
              Conveyance
            </Text>
          </View>
          <View
            style={{
              width: "100%",
              height: "100%",
              backgroundColor: "transparent",
              flexDirection: "row",
              justifyContent: "flex-start",
              alignItems: "flex-start"
            }}
          >
            <View
              style={{
                width: "50%",
                height: "100%",
                backgroundColor: "#E6E6E6",
                flexDirection: "column",
                justifyContent: "flex-start",
                alignItems: "center"
              }}
            >
              <View
                style={{
                  width: "100%",
                  height: "20%",
                  backgroundColor: "#E6E6E6",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginTop: 15
                }}
              >
                <Text
                  style={{
                    fontSize: 13,
                    alignSelf: "center",
                    color: "#767676",
                    marginLeft: 10,
                    fontWeight: "bold"
                  }}
                >
                  ENTER CONVEYANCE
                </Text>
                <TextInput
                  style={{
                    width: "50%",
                    marginLeft: width * 0.055,
                    fontSize: width * 0.009,
                    backgroundColor: "transparent"
                  }}
                  onChangeText={text => this.setState({ search: text })}
                  placeholder="Enter DSP Number"
                  underlineColorAndroid="rgba(0,0,0,0)"
                />
              </View>
              <View
                style={{
                  width: "97%",
                  height: "0.5%",
                  backgroundColor: "#C7C7C7",
                  flexDirection: "row",
                  justifyContent: "center"
                }}
              />

              <View
                style={{
                  width: "100%",
                  height: "20%",
                  backgroundColor: "#E6E6E6",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginTop: 15
                }}
              >
                <Text
                  style={{
                    fontSize: 13,
                    alignSelf: "center",
                    color: "#767676",
                    marginLeft: 10,
                    fontWeight: "bold"
                  }}
                >
                  SEALED BY CONSIGNER
                </Text>
                <View
                  style={{
                    width: "100%",
                    height: "20%",
                    flexDirection: "row",
                    marginLeft: "20%"
                  }}
                >
                  <Switch
                    value={true}
                    onValueChange={val => console.log(val)}
                    disabled={false}
                    activeText={" "}
                    inActiveText={" "}
                    backgroundActive={"green"}
                    backgroundInactive={"gray"}
                    circleActiveColor={"#30a566"}
                    circleInActiveColor={"#000000"}
                  />
                </View>
              </View>
              <View
                style={{
                  width: "97%",
                  height: "0.5%",
                  backgroundColor: "#C7C7C7",
                  flexDirection: "row",
                  justifyContent: "center"
                }}
              />
            </View>

            <View
              style={{
                width: "50%",
                height: "100%",
                backgroundColor: "transparent",
                flexDirection: "column",
                justifyContent: "flex-start",
                alignItems: "flex-start"
              }}
            >
              <View
                style={{
                  width: "100%",
                  height: "20%",
                  backgroundColor: "#E6E6E6",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginTop: 15
                }}
              >
                <Text
                  style={{
                    fontSize: 13,
                    alignSelf: "center",
                    color: "#767676",
                    marginLeft: 10,
                    fontWeight: "bold"
                  }}
                >
                  SERIALS # OF SEALS
                </Text>
                <TextInput
                  style={{
                    width: "50%",
                    marginLeft: width * 0.055,
                    fontSize: width * 0.009,
                    backgroundColor: "transparent"
                  }}
                  onChangeText={text => this.setState({ search: text })}
                  placeholder="Enter Serial no. Seals"
                  underlineColorAndroid="rgba(0,0,0,0)"
                />
              </View>
              <View
                style={{
                  width: "97%",
                  height: "0.5%",
                  backgroundColor: "#C7C7C7",
                  flexDirection: "row",
                  justifyContent: "center"
                }}
              />
            </View>
          </View>
        </View>

        <View
          style={{
            width: "98%",
            height: 150,
            backgroundColor: "#E6E6E6",
            flexDirection: "column",
            marginTop: 30
          }}
        >
          <View
            style={{
              width: "100%",
              height: 40,
              backgroundColor: "white",
              flexDirection: "column",
              justifyContent: "flex-start",
              alignItems: "flex-start"
            }}
          >
            <Text
              style={{
                fontSize: 18,
                alignSelf: "flex-start",
                color: "#767676",
                marginLeft: 10,
                fontWeight: "bold"
              }}
            >
              Select Container
            </Text>
          </View>
          <View
            style={{
              width: "100%",
              height: "100%",
              backgroundColor: "white",
              flexDirection: "row",
              justifyContent: "flex-start",
              alignItems: "flex-start"
            }}
          >
            <View
              style={{
                width: "50%",
                height: "100%",
                backgroundColor: "#E6E6E6",
                flexDirection: "row",
                justifyContent: "flex-start",
                alignItems: "flex-start"
              }}
            >
              <View
                style={{
                  width: "100%",
                  height: "40%",
                  backgroundColor: "#E6E6E6",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginTop: 0,
                  alignItems: "center"
                }}
              >
                <Text
                  style={{
                    fontSize: 13,
                    alignSelf: "center",
                    color: "#767676",
                    marginLeft: 10,
                    fontWeight: "bold"
                  }}
                >
                  ENTER CONVEYANCE
                </Text>
                <TextInput
                  style={{
                    width: "50%",
                    marginLeft: width * 0.055,
                    fontSize: width * 0.009,
                    backgroundColor: "transparent"
                  }}
                  onChangeText={text => this.setState({ search: text })}
                  placeholder="Enter DSP Number"
                  underlineColorAndroid="rgba(0,0,0,0)"
                />
              </View>
            </View>

            <View
              style={{
                width: "50%",
                height: "100%",
                backgroundColor: "#E6E6E6",
                flexDirection: "row",
                justifyContent: "flex-start",
                alignItems: "flex-start"
              }}
            >
              <View
                style={{
                  width: "100%",
                  height: "40%",
                  backgroundColor: "#E6E6E6",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginTop: 0,
                  alignItems: "center"
                }}
              >
                <Text
                  style={{
                    fontSize: 13,
                    alignSelf: "center",
                    color: "#767676",
                    marginLeft: 10,
                    fontWeight: "bold"
                  }}
                >
                  ENTER CONVEYANCE
                </Text>
                <TextInput
                  style={{
                    width: "50%",
                    marginLeft: width * 0.055,
                    fontSize: width * 0.009,
                    backgroundColor: "transparent"
                  }}
                  onChangeText={text => this.setState({ search: text })}
                  placeholder="Enter DSP Number"
                  underlineColorAndroid="rgba(0,0,0,0)"
                />
              </View>
            </View>
          </View>
        </View>

        <View
          style={{
            width: "100%",
            height: "15%",
            backgroundColor: "transparent",
            flexDirection: "row",
            justifyContent: "center"
          }}
        >
          <TouchableOpacity
            onPress={() => console.log("clicked")}
            style={{
              width: "15%",
              height: "40%",
              backgroundColor: "#F7F5F5",
              alignSelf: "center",
              borderRadius: width * 0.005,
              borderWidth: 1,
              borderColor: "#008963"
            }}
          >
            <View
              style={{
                width: "100%",
                height: "100%",
                backgroundColor: "transparent",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <Text
                style={{
                  fontSize: width * 0.008,
                  fontWeight: "bold",
                  marginLeft: "10%"
                }}
              >
                See All Batches
              </Text>
            </View>
          </TouchableOpacity>

          <LinearGradient
            colors={["#004F63", "#008963"]}
            style={{
              width: "15%",
              height: "40%",
              backgroundColor: "transparent",
              padding: "1%",
              alignSelf: "center",
              borderRadius: width * 0.005,
              borderWidth: 1,
              borderColor: "#FFF",
              marginLeft: "4%"
            }}
          >
            <TouchableOpacity
              onPress={() => console.log("clicked")}
              style={{
                width: "100%",
                height: "100%",
                backgroundColor: "transparent",
                alignSelf: "center"
              }}
            >
              <View
                style={{
                  width: "100%",
                  height: "100%",
                  backgroundColor: "transparent",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <Text
                  style={{
                    fontSize: width * 0.008,
                    fontWeight: "bold",
                    color: "#fff"
                  }}
                >
                  TRANSFER
                </Text>
              </View>
            </TouchableOpacity>
          </LinearGradient>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "85%",
    flexDirection: "row",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "white"
  },

  mainContainer: {
    width: "92%",
    height: "100%",
    backgroundColor: "green",
    flexDirection: "column"
  },
  header: {
    backgroundColor: "#455A64"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
