import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  StatusBar,
  TouchableOpacity,
  Dimensions,
  TextInput,
  View
} from "react-native";
import { Switch } from "react-native-switch";

var { height, width } = Dimensions.get("window");
import { TextInputMask } from "react-native-masked-text";
import Icon from "react-native-vector-icons/dist/FontAwesome";
import LinearGradient from "react-native-linear-gradient";
export default class DSP extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      time: true
    };
  }

  _toggel = () => {
    this.setState({ time: !this.state.time });
  };

  // componentDidMount() {
  //   this.props.onRef(this)
  //  }
  //  componentWillUnmount() {
  //   this.props.onRef(undefined)
  //  }
  updateDialogValue(value) {
    console.log("do stuff " + value);
  }

  isValid() {
    // isValid method returns if the inputed value is valid.
    // Ex: if you input 40/02/1990 30:20:20, it will return false
    //	   because in this case, the day and the hour is invalid.
    let valid = this.refs["myDateText"].isValid();

    // get converted value. Using type=datetime, it returns the moment object.
    // If it's using type=money, it returns a Number object.
    let rawValue = this.refs["myDateText"].getRawValue();
  }

  render() {
    var item = [];

    for (var i = 0; i < 6; i++) {
      item.push(
        <View
          style={{
            backgroundColor: "transparent",
            height: "48%",
            width: "100%",
            flexDirection: "row",
            justifyContent: "space-between"
          }}
        >
          <View
            style={{
              backgroundColor: "transparent",
              height: "100%",
              width: "49%",
              flexDirection: "row",
              justifyContent: "space-between",
              borderBottomColor: "#D0D0D0",
              borderBottomWidth: 1,
              marginLeft: 5
            }}
          >
            <Text
              style={{
                fontSize: 14,
                alignSelf: "center",
                color: "#767676",
                marginLeft: 10,
                fontWeight: "bold"
              }}
            >
              TYPE OF CONVEYENCE
            </Text>
            <TextInput
              style={[
                styles.textinput4,
                { alignSelf: "center", fontSize: 13, paddingRight: "2%" }
              ]}
              onChangeText={text => this.setState({ search: text })}
              placeholder="Enter Conveyence"
              underlineColorAndroid="rgba(0,0,0,0)"
            />
          </View>
          <View
            style={{
              backgroundColor: "transparent",
              height: "100%",
              width: "49%",
              flexDirection: "row",
              justifyContent: "space-between",
              borderBottomColor: "#D0D0D0",
              borderBottomWidth: 1,
              marginLeft: 5
            }}
          >
            <Text
              style={{
                fontSize: 14,
                alignSelf: "center",
                color: "#767676",
                marginLeft: 10,
                fontWeight: "bold"
              }}
            >
              SERIAL # OF SEALS
            </Text>
            <TextInput
              style={[
                styles.textinput4,
                { alignSelf: "center", fontSize: 13, paddingRight: "2%" }
              ]}
              onChangeText={text => this.setState({ search: text })}
              placeholder="Enter Serial No"
              underlineColorAndroid="rgba(0,0,0,0)"
            />
          </View>
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <View
          style={{
            width: "98%",
            height: "5%",
            backgroundColor: "white",
            flexDirection: "row"
          }}
        >
          <View
            style={{
              width: "40%",
              height: "100%",
              backgroundColor: "white",
              flexDirection: "row",
              justifyContent: "space-between",
              paddingLeft: "2%",
              paddingRight: "2%"
            }}
          >
            <Text
              style={{ fontSize: 15, alignSelf: "center", color: "#767676" }}
            >
              TRANSFER DATE
            </Text>
            <TextInputMask
              ref={"myDateText"}
              type={"datetime"}
              placeholder="mm/dd/yyyy"
              style={{ width: "30%", height: "100%" }}
              options={{ format: "DD/MM/YYYY" }}
            />
          </View>
          <View
            style={{
              width: "40%",
              height: "100%",
              backgroundColor: "white",
              flexDirection: "row",
              justifyContent: "center",
              paddingLeft: "2%",
              paddingRight: "2%"
            }}
          >
            <Text
              style={{ fontSize: 15, alignSelf: "center", color: "#767676" }}
            >
              TRANSFER DATE
            </Text>
            <TextInputMask
              ref={"myDateText"}
              type={"datetime"}
              placeholder="08:55"
              style={{
                width: "19%",
                height: "100%",
                alignSelf: "center",
                justifyContent: "center",
                flexDirection: "row",
                marginLeft: 15
              }}
              options={{ format: "hh:mm" }}
            />
            <TouchableOpacity
              onPress={() => this._toggel()}
              tyle={{
                marginLeft: 1,
                justifyContent: "center",
                alignItems: "center",
                alignSelf: "center"
              }}
            >
              <Text
                style={{
                  fontSize: 15,
                  alignSelf: "center",
                  color: "#767676",
                  marginTop: 20
                }}
              >
                {this.state.time ? "AM" : "PM"}
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.section1}>
          <View style={styles.titleView}>
            <Text style={styles.title1}>Consigner Information</Text>
          </View>
          <View style={styles.view1}>
            <View style={styles.view11}>
              <View style={styles.view111}>
                <Icon
                  name="search"
                  size={width * 0.015}
                  color="#717171"
                  style={{ marginLeft: 30, alignSelf: "center" }}
                />
                <TextInput
                  style={styles.inputtext6}
                  onChangeText={text => this.setState({ search: text })}
                  placeholder="Search"
                  underlineColorAndroid="rgba(0,0,0,0)"
                />
              </View>
              <View style={styles.view112} />

              <View style={styles.view113}>
                <Text style={styles.text1}>APPLICATION SERIAL NUMBER</Text>
                <TextInput
                  style={styles.inputtext7}
                  onChangeText={text => this.setState({ search: text })}
                  placeholder="Application #"
                  underlineColorAndroid="rgba(0,0,0,0)"
                />
              </View>
              <View style={styles.view114} />

              <View style={styles.view115}>
                <Text style={styles.text2}>BILLING ADDRESS</Text>
                <TextInput
                  style={styles.textinput1}
                  onChangeText={text => this.setState({ search: text })}
                  placeholder="BILLING ADDRESS"
                  underlineColorAndroid="rgba(0,0,0,0)"
                />
              </View>
              <View style={styles.view114} />
            </View>

            <View style={styles.view12}>
              <View style={styles.view121}>
                <Text style={styles.text8}>APPLICATION APPROVAL DATE</Text>
                <TextInputMask
                  ref={"myDateText"}
                  type={"datetime"}
                  placeholder="mm/dd/yyyy"
                  style={{ width: "50%", height: "100%", marginTop: 7 }}
                  options={{ format: "DD/MM/YYYY" }}
                />
              </View>
              <View style={styles.view122} />

              <View style={styles.view124}>
                <Text
                  style={{
                    fontSize: 14,
                    alignSelf: "center",
                    color: "#767676",
                    marginLeft: 10,
                    fontWeight: "bold"
                  }}
                >
                  APPLICATION SERIAL NUMBER
                </Text>
                <TextInput
                  style={styles.inputtext7}
                  onChangeText={text => this.setState({ search: text })}
                  placeholder="Enter DSP Number"
                  underlineColorAndroid="rgba(0,0,0,0)"
                />
              </View>
              <View style={styles.view123} />

              <View style={styles.view125}>
                <Text
                  style={{
                    fontSize: 14,
                    alignSelf: "flex-start",
                    color: "#767676",
                    marginLeft: 10,
                    fontWeight: "bold"
                  }}
                >
                  SHIPPING ADDRESS
                </Text>
                <TextInput
                  style={styles.textinput1}
                  onChangeText={text => this.setState({ search: text })}
                  placeholder="SHIPPING ADDRESS"
                  underlineColorAndroid="rgba(0,0,0,0)"
                />
              </View>
              <View style={styles.view126} />
            </View>
          </View>
        </View>

        <View
          style={{
            width: "98%",
            height: 200,
            backgroundColor: "#E6E6E6",
            flexDirection: "column",
            marginTop: 30
          }}
        >
          <View style={styles.view21}>
            <Text style={[styles.title1, { marginTop: 10 }]}>Conveyance</Text>
          </View>
          <View
            style={{
              backgroundColor: "transparent",
              height: "80%",
              width: "100%",
              flexDirection: "column",
              justifyContent: "flex-start",
              alignItems: "flex-start"
            }}
          >
            <View
              style={{
                backgroundColor: "transparent",
                height: "48%",
                width: "100%",
                flexDirection: "row",
                justifyContent: "space-between"
              }}
            >
              <View
                style={{
                  backgroundColor: "transparent",
                  height: "100%",
                  width: "49%",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  borderBottomColor: "#D0D0D0",
                  borderBottomWidth: 1,
                  marginLeft: 5
                }}
              >
                <Text
                  style={{
                    fontSize: 14,
                    alignSelf: "center",
                    color: "#767676",
                    marginLeft: 10,
                    fontWeight: "bold"
                  }}
                >
                  TYPE OF CONVEYENCE
                </Text>
                <TextInput
                  style={[
                    styles.textinput4,
                    { alignSelf: "center", fontSize: 13, paddingRight: "2%" }
                  ]}
                  onChangeText={text => this.setState({ search: text })}
                  placeholder="Enter Conveyence"
                  underlineColorAndroid="rgba(0,0,0,0)"
                />
              </View>
              <View
                style={{
                  backgroundColor: "transparent",
                  height: "100%",
                  width: "49%",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  borderBottomColor: "#D0D0D0",
                  borderBottomWidth: 1,
                  marginLeft: 5
                }}
              >
                <Text
                  style={{
                    fontSize: 14,
                    alignSelf: "center",
                    color: "#767676",
                    marginLeft: 10,
                    fontWeight: "bold"
                  }}
                >
                  SERIAL # OF SEALS
                </Text>
                <TextInput
                  style={[
                    styles.textinput4,
                    { alignSelf: "center", fontSize: 13, paddingRight: "2%" }
                  ]}
                  onChangeText={text => this.setState({ search: text })}
                  placeholder="Enter Serial No"
                  underlineColorAndroid="rgba(0,0,0,0)"
                />
              </View>
            </View>

            <View
              style={{
                backgroundColor: "transparent",
                height: "48%",
                width: "100%",
                flexDirection: "row",
                justifyContent: "space-between"
              }}
            >
              <View
                style={{
                  backgroundColor: "transparent",
                  height: "100%",
                  width: "49%",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  borderBottomColor: "#D0D0D0",
                  borderBottomWidth: 1,
                  marginLeft: 5
                }}
              >
                <Text
                  style={{
                    fontSize: 14,
                    alignSelf: "center",
                    color: "#767676",
                    marginLeft: 10,
                    fontWeight: "bold"
                  }}
                >
                  SEALED BY COSIGNER
                </Text>
                <View
                  style={{
                    width: "40%",
                    height: "100%",
                    alignSelf: "center",
                    justifyContent: "center",
                    flexDirection: "column",
                    alignItems: "center"
                  }}
                >
                  <Switch
                    value={true}
                    onValueChange={val => console.log(val)}
                    disabled={false}
                    activeText={" "}
                    inActiveText={" "}
                    styles={{
                      alignSelf: "center",
                      marginLeft: -40,
                      paddingBottom: 3
                    }}
                    backgroundActive={"green"}
                    backgroundInactive={"#E5E5E5"}
                    circleActiveColor={"#30a566"}
                    circleInActiveColor={"#007265"}
                  />
                </View>
              </View>
              <View
                style={{
                  backgroundColor: "transparent",
                  height: "100%",
                  width: "49%",
                  flexDirection: "row",
                  justifyContent: "space-between"
                }}
              />
            </View>
          </View>
        </View>

        <View
          style={{
            width: "98%",
            height: 400,
            backgroundColor: "#E6E6E6",
            flexDirection: "column",
            marginTop: 30
          }}
        >
          <View style={[styles.view21, { height: "10%" }]}>
            <Text style={[styles.title1, { marginTop: 10 }]}>
              Spirits Received
            </Text>
          </View>

          <View
            style={{
              backgroundColor: "transparent",
              height: "15%",
              width: "100%",
              flexDirection: "row",
              justifyContent: "space-between"
            }}
          >
            <View
              style={{
                backgroundColor: "transparent",
                height: "100%",
                width: "49%",
                flexDirection: "row",
                justifyContent: "space-between",
                borderBottomColor: "#D0D0D0",
                borderBottomWidth: 1,
                marginLeft: 5
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  alignSelf: "center",
                  color: "#767676",
                  marginLeft: 10,
                  fontWeight: "bold"
                }}
              >
                TYPE OF CONVEYENCE
              </Text>
              <TextInput
                style={[
                  styles.textinput4,
                  { alignSelf: "center", fontSize: 13, paddingRight: "2%" }
                ]}
                onChangeText={text => this.setState({ search: text })}
                placeholder="Enter Conveyence"
                underlineColorAndroid="rgba(0,0,0,0)"
              />
            </View>
            <View
              style={{
                backgroundColor: "transparent",
                height: "100%",
                width: "49%",
                flexDirection: "row",
                justifyContent: "space-between",
                borderBottomColor: "#D0D0D0",
                borderBottomWidth: 1,
                marginLeft: 5
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  alignSelf: "center",
                  color: "#767676",
                  marginLeft: 10,
                  fontWeight: "bold"
                }}
              >
                SERIAL # OF SEALS
              </Text>
              <TextInput
                style={[
                  styles.textinput4,
                  { alignSelf: "center", fontSize: 13, paddingRight: "2%" }
                ]}
                onChangeText={text => this.setState({ search: text })}
                placeholder="Enter Serial No"
                underlineColorAndroid="rgba(0,0,0,0)"
              />
            </View>
          </View>

          <View
            style={{
              backgroundColor: "transparent",
              height: "15%",
              width: "100%",
              flexDirection: "row",
              justifyContent: "space-between"
            }}
          >
            <View
              style={{
                backgroundColor: "transparent",
                height: "100%",
                width: "49%",
                flexDirection: "row",
                justifyContent: "space-between",
                borderBottomColor: "#D0D0D0",
                borderBottomWidth: 1,
                marginLeft: 5
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  alignSelf: "center",
                  color: "#767676",
                  marginLeft: 10,
                  fontWeight: "bold"
                }}
              >
                TYPE OF CONVEYENCE
              </Text>
              <TextInput
                style={[
                  styles.textinput4,
                  { alignSelf: "center", fontSize: 13, paddingRight: "2%" }
                ]}
                onChangeText={text => this.setState({ search: text })}
                placeholder="Enter Conveyence"
                underlineColorAndroid="rgba(0,0,0,0)"
              />
            </View>
            <View
              style={{
                backgroundColor: "transparent",
                height: "100%",
                width: "49%",
                flexDirection: "row",
                justifyContent: "space-between",
                borderBottomColor: "#D0D0D0",
                borderBottomWidth: 1,
                marginLeft: 5
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  alignSelf: "center",
                  color: "#767676",
                  marginLeft: 10,
                  fontWeight: "bold"
                }}
              >
                SERIAL # OF SEALS
              </Text>
              <TextInput
                style={[
                  styles.textinput4,
                  { alignSelf: "center", fontSize: 13, paddingRight: "2%" }
                ]}
                onChangeText={text => this.setState({ search: text })}
                placeholder="Enter Serial No"
                underlineColorAndroid="rgba(0,0,0,0)"
              />
            </View>
          </View>

          <View
            style={{
              backgroundColor: "transparent",
              height: "15%",
              width: "100%",
              flexDirection: "row",
              justifyContent: "space-between"
            }}
          >
            <View
              style={{
                backgroundColor: "transparent",
                height: "100%",
                width: "49%",
                flexDirection: "row",
                justifyContent: "space-between",
                borderBottomColor: "#D0D0D0",
                borderBottomWidth: 1,
                marginLeft: 5
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  alignSelf: "center",
                  color: "#767676",
                  marginLeft: 10,
                  fontWeight: "bold"
                }}
              >
                TYPE OF CONVEYENCE
              </Text>
              <TextInput
                style={[
                  styles.textinput4,
                  { alignSelf: "center", fontSize: 13, paddingRight: "2%" }
                ]}
                onChangeText={text => this.setState({ search: text })}
                placeholder="Enter Conveyence"
                underlineColorAndroid="rgba(0,0,0,0)"
              />
            </View>
            <View
              style={{
                backgroundColor: "transparent",
                height: "100%",
                width: "49%",
                flexDirection: "row",
                justifyContent: "space-between",
                borderBottomColor: "#D0D0D0",
                borderBottomWidth: 1,
                marginLeft: 5
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  alignSelf: "center",
                  color: "#767676",
                  marginLeft: 10,
                  fontWeight: "bold"
                }}
              >
                SERIAL # OF SEALS
              </Text>
              <TextInput
                style={[
                  styles.textinput4,
                  { alignSelf: "center", fontSize: 13, paddingRight: "2%" }
                ]}
                onChangeText={text => this.setState({ search: text })}
                placeholder="Enter Serial No"
                underlineColorAndroid="rgba(0,0,0,0)"
              />
            </View>
          </View>

          <View
            style={{
              backgroundColor: "transparent",
              height: "15%",
              width: "100%",
              flexDirection: "row",
              justifyContent: "space-between"
            }}
          >
            <View
              style={{
                backgroundColor: "transparent",
                height: "100%",
                width: "49%",
                flexDirection: "row",
                justifyContent: "space-between",
                borderBottomColor: "#D0D0D0",
                borderBottomWidth: 1,
                marginLeft: 5
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  alignSelf: "center",
                  color: "#767676",
                  marginLeft: 10,
                  fontWeight: "bold"
                }}
              >
                TYPE OF CONVEYENCE
              </Text>
              <TextInput
                style={[
                  styles.textinput4,
                  { alignSelf: "center", fontSize: 13, paddingRight: "2%" }
                ]}
                onChangeText={text => this.setState({ search: text })}
                placeholder="Enter Conveyence"
                underlineColorAndroid="rgba(0,0,0,0)"
              />
            </View>
            <View
              style={{
                backgroundColor: "transparent",
                height: "100%",
                width: "49%",
                flexDirection: "row",
                justifyContent: "space-between",
                borderBottomColor: "#D0D0D0",
                borderBottomWidth: 1,
                marginLeft: 5
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  alignSelf: "center",
                  color: "#767676",
                  marginLeft: 10,
                  fontWeight: "bold"
                }}
              >
                SERIAL # OF SEALS
              </Text>
              <TextInput
                style={[
                  styles.textinput4,
                  { alignSelf: "center", fontSize: 13, paddingRight: "2%" }
                ]}
                onChangeText={text => this.setState({ search: text })}
                placeholder="Enter Serial No"
                underlineColorAndroid="rgba(0,0,0,0)"
              />
            </View>
          </View>

          <View
            style={{
              backgroundColor: "transparent",
              height: "15%",
              width: "100%",
              flexDirection: "row",
              justifyContent: "space-between"
            }}
          >
            <View
              style={{
                backgroundColor: "transparent",
                height: "100%",
                width: "49%",
                flexDirection: "row",
                justifyContent: "space-between",
                borderBottomColor: "#D0D0D0",
                borderBottomWidth: 1,
                marginLeft: 5
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  alignSelf: "center",
                  color: "#767676",
                  marginLeft: 10,
                  fontWeight: "bold"
                }}
              >
                TYPE OF CONVEYENCE
              </Text>
              <TextInput
                style={[
                  styles.textinput4,
                  { alignSelf: "center", fontSize: 13, paddingRight: "2%" }
                ]}
                onChangeText={text => this.setState({ search: text })}
                placeholder="Enter Conveyence"
                underlineColorAndroid="rgba(0,0,0,0)"
              />
            </View>
            <View
              style={{
                backgroundColor: "transparent",
                height: "100%",
                width: "49%",
                flexDirection: "row",
                justifyContent: "space-between",
                borderBottomColor: "#D0D0D0",
                borderBottomWidth: 1,
                marginLeft: 5
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  alignSelf: "center",
                  color: "#767676",
                  marginLeft: 10,
                  fontWeight: "bold"
                }}
              >
                SERIAL # OF SEALS
              </Text>
              <TextInput
                style={[
                  styles.textinput4,
                  { alignSelf: "center", fontSize: 13, paddingRight: "2%" }
                ]}
                onChangeText={text => this.setState({ search: text })}
                placeholder="Enter Serial No"
                underlineColorAndroid="rgba(0,0,0,0)"
              />
            </View>
          </View>

          <View
            style={{
              backgroundColor: "transparent",
              height: "15%",
              width: "100%",
              flexDirection: "row",
              justifyContent: "space-between"
            }}
          >
            <View
              style={{
                backgroundColor: "transparent",
                height: "100%",
                width: "49%",
                flexDirection: "row",
                justifyContent: "space-between",
                borderBottomColor: "#D0D0D0",
                borderBottomWidth: 1,
                marginLeft: 5
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  alignSelf: "center",
                  color: "#767676",
                  marginLeft: 10,
                  fontWeight: "bold"
                }}
              >
                TYPE OF CONVEYENCE
              </Text>
              <TextInput
                style={[
                  styles.textinput4,
                  { alignSelf: "center", fontSize: 13, paddingRight: "2%" }
                ]}
                onChangeText={text => this.setState({ search: text })}
                placeholder="Enter Conveyence"
                underlineColorAndroid="rgba(0,0,0,0)"
              />
            </View>
            <View
              style={{
                backgroundColor: "transparent",
                height: "100%",
                width: "49%",
                flexDirection: "row",
                justifyContent: "space-between",
                borderBottomColor: "#D0D0D0",
                borderBottomWidth: 1,
                marginLeft: 5
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  alignSelf: "center",
                  color: "#767676",
                  marginLeft: 10,
                  fontWeight: "bold"
                }}
              >
                SERIAL # OF SEALS
              </Text>
              <TextInput
                style={[
                  styles.textinput4,
                  { alignSelf: "center", fontSize: 13, paddingRight: "2%" }
                ]}
                onChangeText={text => this.setState({ search: text })}
                placeholder="Enter Serial No"
                underlineColorAndroid="rgba(0,0,0,0)"
              />
            </View>
          </View>
        </View>

        <View style={styles.section4}>
          <View style={styles.btnBackground1}>
            <TouchableOpacity
              onPress={() => this.setState({ next: false })}
              style={styles.actionButton}
            >
              <Text style={styles.cancelText}> CANCEL </Text>
            </TouchableOpacity>
          </View>

          <View style={styles.btnBackground}>
            <LinearGradient
              colors={["#008963", "#004F63"]}
              style={{
                marginRight: 10,
                marginLeft: 10,
                marginTop: 0,
                marginBottom: 5,
                paddingTop: 10,
                paddingBottom: 10,
                borderRadius: width * 0.03,
                borderColor: "#008963",
                width: width * 0.155,
                height: width * 0.04,
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <TouchableOpacity
                onPress={() => this.setState({ next: true })}
                style={styles.actionButton}
              >
                <Text style={styles.transferText}>TRANSFER</Text>
              </TouchableOpacity>
            </LinearGradient>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "85%",
    flexDirection: "row",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "white"
  },

  mainContainer: {
    width: "92%",
    height: "100%",
    backgroundColor: "green",
    flexDirection: "column"
  },
  header: {
    backgroundColor: "#455A64"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },

  title1: {
    fontSize: 15,
    alignSelf: "flex-start",
    color: "#767676",
    marginLeft: 2,
    fontWeight: "bold"
  },
  section1: {
    width: "98%",
    height: 400,
    backgroundColor: "#E6E6E6",
    flexDirection: "column",
    alignSelf: "center"
  },
  section2: {
    width: "96%",
    height: 150,
    backgroundColor: "#E6E6E6",
    flexDirection: "column",
    marginTop: 50,
    alignSelf: "center"
  },
  section3: {
    width: "96%",
    height: 100,
    backgroundColor: "#E6E6E6",
    flexDirection: "column",
    marginTop: 10,
    alignSelf: "center"
  },
  view1: {
    width: "100%",
    height: "100%",
    backgroundColor: "transparent",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "flex-start"
  },
  view11: {
    width: "50%",
    height: "100%",
    backgroundColor: "#E6E6E6",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  view111: {
    width: "100%",
    height: "12%",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  view112: {
    width: "96%",
    height: "0.5%",
    backgroundColor: "#C7C7C7",
    flexDirection: "row",
    justifyContent: "center"
  },
  view113: {
    width: "100%",
    height: "13%",
    backgroundColor: "#E6E6E6",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 5
  },
  view114: {
    width: "96%",
    height: "0.5%",
    backgroundColor: "#C7C7C7",
    flexDirection: "row",
    justifyContent: "center"
  },
  view115: {
    width: "96%",
    height: 200,
    backgroundColor: "transparent",
    flexDirection: "column",
    justifyContent: "center",
    marginTop: 10,
    alignSelf: "center"
  },
  section4: {
    width: "96%",
    height: "10%",
    backgroundColor: "transparent",
    flexDirection: "row",
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center"
  },

  mainView: {
    width: "96%",
    height: 500,
    backgroundColor: "red",
    alignSelf: "center",
    flexDirection: "column"
  },
  subView: {
    width: "100%",
    height: "5%",
    backgroundColor: "#E6E6E6",
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: "2%",
    paddingTop: "1%"
  },
  subView2: {
    width: "98%",
    height: "5%",
    backgroundColor: "white",
    flexDirection: "row"
  },

  viewInput: {
    width: "33.33%",
    height: "100%",
    backgroundColor: "white",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: "2%",
    paddingRight: "2%",
    borderColor: "#EAEAEA",
    borderBottomWidth: 2,
    marginLeft: "3%"
  },
  timeText: {
    fontSize: 15,
    alignSelf: "center",
    color: "#767676",
    marginTop: 20
  },
  titleView: {
    width: "100%",
    height: 40,
    paddingTop: 15,
    backgroundColor: "white",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start"
  },

  textInput: {
    width: "19%",
    height: "100%",
    alignSelf: "center",
    justifyContent: "center",
    flexDirection: "row",
    marginLeft: 12,
    fontSize: 15
  },

  emptySection: {
    width: "33.33%",
    height: "100%",
    backgroundColor: "transparent",
    flexDirection: "row"
  },
  title: {
    fontSize: 15,
    alignSelf: "center",
    color: "#767676"
  },
  text1: {
    fontSize: 12,
    alignSelf: "center",
    color: "#767676",
    marginLeft: 10,
    fontWeight: "bold"
  },
  text2: {
    fontSize: 12,
    alignSelf: "flex-start",
    color: "#767676",
    marginLeft: 10,
    fontWeight: "bold"
  },
  text3: {
    fontSize: 13,
    alignSelf: "center",
    color: "#767676",
    marginLeft: 10,
    fontWeight: "bold"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  titleMain: {
    fontSize: width * 0.015,
    color: "#4E9197",
    alignSelf: "flex-start",
    fontWeight: "bold"
  },
  textinput1: {
    textAlignVertical: "top",
    width: "100%",
    height: "80%",
    marginLeft: width * 0.004,
    fontSize: width * 0.013,
    marginTop: 5
  },
  view12: {
    width: "50%",
    height: "100%",
    backgroundColor: "#E6E6E6",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  view121: {
    width: "100%",
    height: "11%",
    backgroundColor: "#E6E6E6",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 5
  },
  view122: {
    width: "98%",
    height: "0.5%",
    backgroundColor: "#C7C7C7",
    flexDirection: "row",
    justifyContent: "center"
  },
  view123: {
    width: "96%",
    height: "0.5%",
    backgroundColor: "#C7C7C7",
    flexDirection: "row",
    justifyContent: "center"
  },
  view124: {
    width: "100%",
    height: "13%",
    backgroundColor: "#E6E6E6",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 5
  },
  view125: {
    width: "100%",
    height: 200,
    backgroundColor: "transparent",
    flexDirection: "column",
    justifyContent: "center",
    marginTop: 10
  },
  view126: {
    width: "96%",
    height: "0.5%",
    backgroundColor: "#C7C7C7",
    flexDirection: "row",
    justifyContent: "center"
  },
  view21: {
    width: "100%",
    height: "20%",
    backgroundColor: "white",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start"
  },
  view22: {
    width: "100%",
    height: "100%",
    backgroundColor: "transparent",
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "flex-start"
  },
  view221: {
    width: "50%",
    height: "100%",
    backgroundColor: "#E6E6E6",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  view222: {
    width: "100%",
    height: "20%",
    backgroundColor: "#E6E6E6",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 15
  },
  view223: {
    width: "96%",
    height: "0.5%",
    backgroundColor: "#C7C7C7",
    flexDirection: "row",
    justifyContent: "center"
  },
  view224: {
    width: "100%",
    height: "20%",
    backgroundColor: "red",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 15
  },
  view225: {
    width: "50%",
    height: "100%",
    backgroundColor: "transparent",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start"
  },
  view226: {
    width: "100%",
    height: "20%",
    backgroundColor: "green",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 15
  },
  section31: {
    width: "100%",
    height: "15%",
    backgroundColor: "white",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start"
  },
  section321: {
    width: "50%",
    height: "100%",
    backgroundColor: "#E6E6E6",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "flex-start"
  },
  section322: {
    width: "100%",
    height: "40%",
    backgroundColor: "#E6E6E6",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 0,
    alignItems: "center"
  },
  section323: {
    width: "50%",
    height: "100%",
    backgroundColor: "#E6E6E6",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "flex-start"
  },
  section324: {
    width: "100%",
    height: "40%",
    backgroundColor: "red",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 0,
    alignItems: "center"
  },
  input1: {
    width: "50%",
    marginLeft: width * 0.055,
    fontSize: width * 0.009,
    backgroundColor: "transparent"
  },
  text3: {
    fontSize: 16,
    alignSelf: "flex-start",
    color: "#767676",
    marginLeft: 10,
    fontWeight: "bold"
  },
  text4: {
    fontSize: 13,
    alignSelf: "center",
    color: "#767676",
    marginLeft: 10,
    fontWeight: "bold"
  },
  text5: {
    fontSize: 13,
    alignSelf: "center",
    color: "#767676",
    marginLeft: 10,
    fontWeight: "bold"
  },
  view6: {
    width: "100%",
    height: "20%",
    flexDirection: "row",
    marginLeft: "20%"
  },
  input4: {
    width: "50%",
    marginLeft: width * 0.055,
    fontSize: width * 0.009,
    backgroundColor: "transparent"
  },
  text6: {
    fontSize: 18,
    alignSelf: "flex-start",
    color: "#767676",
    marginLeft: 10,
    fontWeight: "bold"
  },
  textinput4: {
    fontSize: 18,
    alignSelf: "flex-start",
    color: "#767676",
    marginLeft: 10,
    fontWeight: "bold"
  },
  textinput6: {
    fontSize: 18,
    alignSelf: "flex-start",
    color: "#767676",
    marginLeft: 10,
    fontWeight: "bold"
  },
  btn1: {
    width: "15%",
    height: "40%",
    backgroundColor: "#F7F5F5",
    alignSelf: "center",
    borderRadius: width * 0.005,
    borderWidth: 1,
    borderColor: "#008963"
  },
  btn2: {
    width: "15%",
    height: "40%",
    backgroundColor: "transparent",
    padding: "1%",
    alignSelf: "center",
    borderRadius: width * 0.005,
    borderWidth: 1,
    borderColor: "#FFF",
    marginLeft: "4%"
  },
  btn21: {
    width: "100%",
    height: "100%",
    backgroundColor: "transparent",
    alignSelf: "center"
  },
  btnView1: {
    width: "100%",
    height: "100%",
    backgroundColor: "transparent",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  btnView2: {
    width: "100%",
    height: "100%",
    backgroundColor: "transparent",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  btntext1: {
    fontSize: width * 0.011,
    fontWeight: "bold",
    marginLeft: "10%"
  },
  btntext2: {
    fontSize: width * 0.011,
    fontWeight: "bold",
    color: "#fff"
  },
  inputtext6: {
    width: "90%",
    marginLeft: width * 0.001,
    fontSize: width * 0.015,
    textAlign: "left"
  },
  inputtext7: {
    width: "50%",
    marginLeft: width * 0.055,
    fontSize: width * 0.013
  },
  text8: {
    fontSize: 12,
    alignSelf: "center",
    color: "#767676",
    marginLeft: 10,
    fontWeight: "bold",
    marginTop: 6
  },
  btnBackground: {
    marginRight: 10,
    marginLeft: 10,
    marginTop: 0,
    marginBottom: 5,
    paddingTop: 10,
    paddingBottom: 10,
    borderRadius: width * 0.03,
    borderWidth: 1,
    borderColor: "#fff",
    width: width * 0.155,
    height: width * 0.04,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  btnBackground1: {
    marginRight: 10,
    marginLeft: 10,
    marginTop: 0,
    marginBottom: 5,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: "#fff",
    borderRadius: width * 0.03,
    borderWidth: 2,
    borderColor: "#008963",
    width: width * 0.155,
    height: width * 0.04,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  transferText: {
    fontSize: 15,
    color: "white",
    alignSelf: "center",
    fontWeight: "normal",
    paddingLeft: "0.1%"
  }
});
