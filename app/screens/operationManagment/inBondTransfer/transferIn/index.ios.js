import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  StatusBar,
  TouchableOpacity,
  View,
  Dimensions,
  ScrollView,
  KeyboardAvoidingView
} from "react-native";

import { TabRouter, StackNavigator } from "react-navigation";
var { height, width } = Dimensions.get("window");

import Tabs from "@screen/operationManagment/inBondTransfer/tabs";

import DSP from "@screen/operationManagment/inBondTransfer/DSP";
import CustomImport from "@screen/operationManagment/inBondTransfer/CustomImport";
import ReturnedBulk from "@screen/operationManagment/inBondTransfer/ReturnedBulk";

const TabRoute = TabRouter(
  {
    DSP: { screen: DSP },
    CUSTOMIMPORT: { screen: DSP },
    RETURNEDBULK: { screen: DSP }
  },
  {
    initialRouteName: "DSP"
  }
);

class TabContentNavigator extends Component {
  constructor(props, context) {
    super(props, context);
    //console.log(props);
    this.state = {
      active: props.value.active
    };
  }

  //this method will not get called first time
  componentWillReceiveProps(newProps) {
    //console.log(this.state);
    this.setState({
      active: newProps.value.active
    });
  }

  render() {
    const Component = TabRoute.getComponentForRouteName(this.state.active);
    return <Component screenProps={this.state} />;
  }
}

export default class TransferIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "DSP",
      seeAllBatches: false
    };
    this._selectTab = this._selectTab.bind(this);
  }

  _selectTab(key) {
    this.setState({ active: key });
  }

  updateDialogValue(value) {
    console.log("do stuff " + value);
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          style={{
            marginLeft: "0.3%",
            marginRight: "0.3%",
            width: "100%",
            alignSelf: "center"
          }}
        >
          <View
            style={{
              width: "96%",
              height: 1380,
              backgroundColor: "white",
              alignSelf: "center",
              flexDirection: "column"
            }}
          >
            <View style={styles.cardStyle}>
              <View style={styles.actionViewMain}>
                <Text style={styles.titleMain}>IN BOND SPIRITS TRANSFER</Text>
              </View>

              <View style={styles.actionView}>
                <Text style={styles.titleSub}>
                  {" "}
                  Receive Vessels In-Bond from another DSP
                </Text>
              </View>
            </View>

            <Tabs method={this._selectTab} />
            <TabContentNavigator value={this.state} />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "85%",
    flexDirection: "row",
    flexDirection: "column",
    backgroundColor: "#F1ECEC"
  },
  contentContainer: {
    paddingVertical: 20
  },

  actionView: {
    width: "75%",
    height: "100%",
    flexDirection: "row",
    padding: 5,
    backgroundColor: "transparent",
    justifyContent: "flex-start"
  },
  actionViewMain: {
    width: "20%",
    height: "100%",
    flexDirection: "row",
    backgroundColor: "transparent",
    marginLeft: "1%",
    justifyContent: "flex-start"
  },
  titleMain: {
    fontSize: width * 0.01,
    color: "#4E9197",
    alignSelf: "center",
    fontWeight: "bold"
  },
  titleSub: {
    fontSize: width * 0.011,
    color: "#848484",
    alignSelf: "center",
    fontWeight: "normal"
  },
  titleSub1: {
    fontSize: width * 0.011,
    color: "#ACACAC",
    alignSelf: "center",
    marginRight: 10
  },
  spaceview: {
    width: "0.5%",
    height: "100%",
    flexDirection: "row",
    backgroundColor: "#F1ECEC"
  },
  itemSection1: {
    width: "100%",
    height: "40%",
    flexDirection: "row",
    backgroundColor: "transparent"
  },
  itemSection2: {
    width: "100%",
    height: "15%",
    flexDirection: "row",
    backgroundColor: "transparent"
  },
  itemSection3: {
    width: "100%",
    height: "15%",
    flexDirection: "row",
    backgroundColor: "transparent"
  },
  itemSection4: {
    marginTop: "5%",
    width: "100%",
    height: "1%",
    flexDirection: "row",
    backgroundColor: "#ECECEC"
  },
  itemSection5: {
    width: "100%",
    height: "20%",
    flexDirection: "row",
    backgroundColor: "transparent"
  },
  renderItemStyle: {
    flex: 1,
    minWidth: width * 0.22,
    maxWidth: width * 0.22,
    height: width * 0.18,
    maxHeight: width * 0.18,
    padding: width * 0.01
  },
  renderImageStyle: {
    marginTop: width * 0.01,
    width: "25%",
    height: "60%",
    resizeMode: "contain",
    alignSelf: "flex-start"
  },
  renderSection1: {
    width: "50%",
    height: "100%",
    padding: width * 0.005,
    flexDirection: "column",
    justifyContent: "center"
  },
  renderText1: {
    fontSize: width * 0.007,
    color: "#848484",
    fontWeight: "bold"
  },
  renderText2: {
    fontSize: width * 0.007,
    color: "#ACACAC",
    marginRight: width * 0.007,
    fontWeight: "bold"
  },
  renderSection2: {
    width: "25%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: width * 0.005
  },
  textSection1: {
    width: "20%",
    height: "100%",
    fontSize: width * 0.007,
    color: "#000",
    fontWeight: "bold"
  },

  textSection2: {
    width: "30%",
    height: "100%",
    fontSize: width * 0.007,
    color: "#ACACAC",
    marginRight: width * 0.007,
    fontWeight: "bold"
  },
  textSection3: {
    width: "20%",
    height: "100%",
    fontSize: width * 0.007,
    color: "#000",
    fontWeight: "bold"
  },
  textSection4: {
    width: "80%",
    height: "100%",
    fontSize: width * 0.007,
    color: "#ACACAC",
    marginRight: width * 0.01,
    fontWeight: "bold"
  },
  textSection5: {
    fontSize: width * 0.007,
    color: "#53939A",
    fontWeight: "bold",
    marginLeft: width * 0.004,
    alignSelf: "center"
  },
  viewSecion: {
    width: "37%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: width * 0.005
  },
  viewSection1: {
    width: "25%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: width * 0.005
  },

  list: {
    justifyContent: "center",
    flexDirection: "row",
    flexWrap: "wrap",
    margin: width * 0.012,
    alignItems: "center"
  },

  mainContainer: {
    width: "92%",
    height: "100%",
    backgroundColor: "green",
    flexDirection: "column"
  },
  header: {
    backgroundColor: "#455A64"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  itemStyle: {
    width: "100%",
    height: "100%",
    flexDirection: "column",
    backgroundColor: "#E6E6E6",

    paddingLeft: width * 0.008,
    paddingRight: width * 0.008,
    borderWidth: 0.5,
    borderRadius: width * 0.008,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#E8E5E5",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  cardStyle: {
    justifyContent: "space-between",
    backgroundColor: "#E6E6E6",
    width: "100%",
    height: "5%",
    flexDirection: "row",
    backgroundColor: "#E6E6E6",
    alignSelf: "center",
    paddingLeft: width * 0.004,
    paddingRight: width * 0.004,
    borderWidth: 0.5,
    borderRadius: width * 0.003,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#E8E5E5",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1
  }
});
