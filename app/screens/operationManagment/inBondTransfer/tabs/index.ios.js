import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  StatusBar,
  TouchableOpacity,
  Image,
  Dimensions,
  View
} from "react-native";
import Icon from "react-native-vector-icons/dist/Octicons";
import Icon1 from "react-native-vector-icons/dist/EvilIcons";
import Icon2 from "react-native-vector-icons/dist/Ionicons";
import Icon3 from "react-native-vector-icons/dist/MaterialIcons";

import LinearGradient from "react-native-linear-gradient";

var { height, width } = Dimensions.get("window");

export default class Tab extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      active: "DSP"
    };
    this._selectTab = this._selectTab.bind(this);
  }

  updateDialogValue(value) {
    console.log("do stuff " + value);
  }

  _selectTab(key) {
    this.setState({ active: key });
    this.props.method(key);
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.bottomTabselection}
          onPress={() => this._selectTab("DSP")}
        >
          <View style={styles.bootmParent}>
            <View style={styles.buttonText}>
              <Text
                style={
                  this.state.active != "DSP"
                    ? styles.bottomText
                    : styles.bottomTextSelected
                }
              >
                DSP
              </Text>
            </View>
            {this.state.active === "DSP" && <View style={styles.iconView} />}
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.bottomTabselection}
          onPress={() => this._selectTab("CUSTOMIMPORT")}
        >
          <View style={styles.bootmParent}>
            <View style={styles.buttonText}>
              <Text
                style={
                  this.state.active != "CUSTOMIMPORT"
                    ? styles.bottomText
                    : styles.bottomTextSelected
                }
              >
                CUSTOM/IMPORT
              </Text>
            </View>
            {this.state.active === "CUSTOMIMPORT" && (
              <View style={styles.iconView} />
            )}
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.bottomTabselection}
          onPress={() => this._selectTab("RETURNEDBULK")}
        >
          <View style={styles.bootmParent}>
            <View style={styles.buttonText}>
              <Text
                style={
                  this.state.active != "RETURNEDBULK"
                    ? styles.bottomText
                    : styles.bottomTextSelected
                }
              >
                RETURNED BULK
              </Text>
            </View>
            {this.state.active === "RETURNEDBULK" && (
              <View style={styles.iconView} />
            )}
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "98%",
    height: 50,
    flexDirection: "row",
    backgroundColor: "#E6E6E6",
    alignSelf: "center",
    marginTop: "1%"
  },
  iconView: {
    height: "25%",
    width: "100%",
    flexDirection: "row",
    justifyContent: "flex-end",
    backgroundColor: "#38B1B0",
    alignSelf: "flex-end",
    marginTop: 9
  },

  bootmParent: {
    height: "100%",
    width: "100%",
    flexDirection: "column",
    justifyContent: "flex-start"
  },

  buttonText: {
    height: "77%",
    width: "100%",
    flexDirection: "column",
    justifyContent: "center",
    backgroundColor: "transparent",
    marginTop: 0
  },
  mainContainer: {
    width: "92%",
    height: "100%",
    backgroundColor: "green",
    flexDirection: "column"
  },
  header: {
    backgroundColor: "#455A64"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  bottomTabselection: {
    width: "33.33%",
    height: "100%",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
    paddingLeft: 5
  },
  bottomTabselected: {
    flex: 0.245,
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "transparent",
    paddingLeft: 5
  },
  bottomText: {
    fontSize: width * 0.011,
    color: "#ADACAC",
    alignSelf: "center",
    fontWeight: "normal"
  },
  bottomTextSelected: {
    color: "#76C8C7",
    fontSize: width * 0.011,
    alignSelf: "center",
    fontWeight: "bold"
  }
});
