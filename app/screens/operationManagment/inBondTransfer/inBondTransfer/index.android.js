import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  Image,
  StatusBar,
  View,
  Dimensions,
  TouchableOpacity
} from "react-native";
var { height, width } = Dimensions.get("window");
import LinearGradient from "react-native-linear-gradient";

export default class InBondTransfer extends Component {
  // componentDidMount() {
  //   this.props.onRef(this)
  //  }
  //  componentWillUnmount() {
  //   this.props.onRef(undefined)
  //  }
  updateDialogValue(value) {
    console.log("do stuff " + value);
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            width: "95%",
            height: "95%",
            margin: "15%",
            backgroundColor: "white",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "white"
          }}
        >
          <View
            style={{
              width: "100%",
              height: "25%",
              backgroundColor: "transparent",
              padding: "10%",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text style={styles.titleMain}>INBOND SPIRITS TRANSFER</Text>
            <Text style={styles.titleMain1}>
              Receive Vessels In-Bond From Other DSP
            </Text>
          </View>

          <View
            style={{ width: "80%", height: "0.3%", backgroundColor: "#C5C5C5" }}
          />

          <View
            style={{
              width: "100%",
              height: "33%",
              backgroundColor: "transparent",
              padding: "10%",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text
              style={[
                styles.titleMain,
                { fontSize: width * 0.03, fontWeight: "normal" }
              ]}
            >
              Oh No!
            </Text>
            <Text
              style={[
                styles.titleMain1,
                {
                  fontSize: width * 0.012,
                  fontWeight: "normal",
                  color: "#A0A0A0"
                }
              ]}
            >
              No In-Bond Transfers of Hooch ? If you receive your spirits from
              other DSP or Customs/Import then you will need to create your
              first Bonded Premise Transfer.
            </Text>
          </View>
          <View
            style={{ width: "80%", height: "0.3%", backgroundColor: "#C5C5C5" }}
          />

          <View
            style={{
              width: "100%",
              height: "33%",
              backgroundColor: "transparent",
              padding: "10%",
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <LinearGradient
              colors={["#008963", "#004F63"]}
              style={styles.btnBackground}
            >
              <TouchableOpacity
                onPress={() => this.props.method("TransferIn")}
                style={{
                  width: "100%",
                  height: "100%",
                  alignItems: "center",
                  flexDirection: "column",
                  justifyContent: "center"
                }}
              >
                <Text
                  style={{
                    fontSize: width * 0.01,
                    color: "white",
                    alignSelf: "center",
                    fontWeight: "normal",
                    paddingLeft: "2%"
                  }}
                >
                  TRANSFER IN
                </Text>
              </TouchableOpacity>
            </LinearGradient>

            <LinearGradient
              colors={["#008963", "#004F63"]}
              style={styles.btnBackground}
            >
              <TouchableOpacity
                onPress={() => this.props.method("TransferOut")}
                style={{
                  width: "100%",
                  height: "100%",
                  alignItems: "center",
                  flexDirection: "column",
                  justifyContent: "center"
                }}
              >
                <Text
                  style={{
                    fontSize: width * 0.01,
                    color: "white",
                    alignSelf: "center",
                    fontWeight: "normal",
                    paddingLeft: "2%"
                  }}
                >
                  TRANSFER OUT
                </Text>
              </TouchableOpacity>
            </LinearGradient>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "85%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F1ECEC"
  },

  mainContainer: {
    width: "92%",
    height: "100%",
    backgroundColor: "green",
    flexDirection: "column"
  },
  btnBackground: {
    marginRight: width * 0.04,
    marginLeft: width * 0.04,
    marginTop: width * 0.01,
    paddingTop: width * 0.015,
    paddingBottom: width * 0.015,
    backgroundColor: "#008963",
    borderRadius: width * 0.03,
    borderWidth: 1,
    borderColor: "#fff",
    width: width * 0.15,
    height: width * 0.055,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  header: {
    backgroundColor: "#455A64"
  },
  titleMain: {
    fontSize: width * 0.013,
    color: "#4E9197",
    alignSelf: "center",
    fontWeight: "bold",
    paddingLeft: "2%"
  },
  titleMain1: {
    marginTop: "2%",
    fontSize: width * 0.013,
    color: "#A0A0A0",
    alignSelf: "center",
    fontWeight: "bold",
    paddingLeft: "2%"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
