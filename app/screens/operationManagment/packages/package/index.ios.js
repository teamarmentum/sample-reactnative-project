import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  View
} from "react-native";

var { height, width } = Dimensions.get("window");

import Icon4 from "react-native-vector-icons/dist/FontAwesome";
import Icon3 from "react-native-vector-icons/dist/MaterialIcons";

export default class Packages extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.cardStyle}>
          <View style={styles.actionViewMain}>
            <Text style={styles.titleMain}>PACKAGES</Text>
          </View>

          <View style={styles.spaceview} />

          <View style={styles.actionView}>
            <Text style={styles.titleSub}>SELECT FINISHED PRODUCT </Text>
            <TouchableOpacity
              style={{
                width: "35%",
                height: "100%",
                backgroundColor: "transparent",
                flexDirection: "row",
                alignItems: "center",
                marginLeft: 10
              }}
            >
              <View
                style={{
                  width: "100%",
                  height: "100%",
                  backgroundColor: "transparent",
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                <Text style={{ fontSize: 12, fontWeight: "bold" }}>
                  Whiskey
                </Text>
                <View
                  style={{
                    width: "50%",
                    height: "100%",
                    backgroundColor: "transparent",
                    flexDirection: "column",
                    alignItems: "center",
                    justifyContent: "center",
                    marginLeft: -15
                  }}
                >
                  <Icon4
                    name="sort-down"
                    size={width * 0.013}
                    color="#000"
                    style={{ marginTop: -10, marginLeft: -8 }}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            width: "99%",
            height: "89%",
            backgroundColor: "transparent",
            alignSelf: "center",
            marginTop: "1%",
            flexDirection: "column",
            justifyContent: "flex-start"
          }}
        >
          <View
            style={{
              width: "100%",
              height: "6%",
              backgroundColor: "transparent",
              flexDirection: "row"
            }}
          >
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "bold",
                  alignSelf: "center",
                  color: "black"
                }}
              >
                Status
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "bold",
                  alignSelf: "center",
                  color: "black"
                }}
              >
                Serial #
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "bold",
                  alignSelf: "center",
                  color: "black"
                }}
              >
                Packaged Date
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "bold",
                  alignSelf: "center",
                  color: "black"
                }}
              >
                Finished Product
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "bold",
                  alignSelf: "center",
                  color: "black"
                }}
              >
                Total Units
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "bold",
                  alignSelf: "center",
                  color: "black"
                }}
              >
                Staff
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row",
                color: "black"
              }}
            />
          </View>

          <View
            style={{
              width: "100%",
              height: "0.2%",
              backgroundColor: "#F1ECEC",
              flexDirection: "row"
            }}
          />
          {/*Item View 2 Starts*/}

          <TouchableOpacity
            onPress={() => this.props.method("PackageDetail")}
            style={{
              width: "100%",
              height: "6%",
              backgroundColor: "red",
              flexDirection: "row"
            }}
          >
            <View
              style={{
                width: "100%",
                height: "100%",
                backgroundColor: "white",
                flexDirection: "row"
              }}
            >
              <View
                style={{
                  width: "15%",
                  height: "100%",
                  backgroundColor: "white",
                  justifyContent: "center",
                  flexDirection: "row"
                }}
              >
                <Text
                  style={{
                    fontSize: 12,
                    fontWeight: "normal",
                    alignSelf: "center"
                  }}
                >
                  Completed
                </Text>
              </View>
              <View
                style={{
                  width: "15%",
                  height: "100%",
                  backgroundColor: "white",
                  justifyContent: "center",
                  flexDirection: "row"
                }}
              >
                <Text
                  style={{
                    fontSize: 12,
                    fontWeight: "normal",
                    alignSelf: "center"
                  }}
                >
                  2187456892
                </Text>
              </View>
              <View
                style={{
                  width: "15%",
                  height: "100%",
                  backgroundColor: "white",
                  justifyContent: "center",
                  flexDirection: "row"
                }}
              >
                <Text
                  style={{
                    fontSize: 12,
                    fontWeight: "normal",
                    alignSelf: "center"
                  }}
                >
                  05/12/2017
                </Text>
              </View>
              <View
                style={{
                  width: "15%",
                  height: "100%",
                  backgroundColor: "white",
                  justifyContent: "center",
                  flexDirection: "row"
                }}
              >
                <Text
                  style={{
                    fontSize: 12,
                    fontWeight: "normal",
                    alignSelf: "center"
                  }}
                >
                  Wishkey
                </Text>
              </View>
              <View
                style={{
                  width: "15%",
                  height: "100%",
                  backgroundColor: "white",
                  justifyContent: "center",
                  flexDirection: "row"
                }}
              >
                <Text
                  style={{
                    fontSize: 12,
                    fontWeight: "normal",
                    alignSelf: "center"
                  }}
                >
                  25
                </Text>
              </View>
              <View
                style={{
                  width: "15%",
                  height: "100%",
                  backgroundColor: "white",
                  justifyContent: "center",
                  flexDirection: "row"
                }}
              >
                <Text
                  style={{
                    fontSize: 12,
                    fontWeight: "normal",
                    alignSelf: "center"
                  }}
                >
                  Wendell Burgess
                </Text>
              </View>
              <View
                style={{
                  width: "15%",
                  height: "100%",
                  backgroundColor: "white",
                  justifyContent: "center",
                  flexDirection: "row"
                }}
              >
                <TouchableOpacity>
                  <Icon4
                    name="file-text-o"
                    size={width * 0.016}
                    color="#717171"
                    style={{ marginTop: 3 }}
                  />
                </TouchableOpacity>
                <View
                  style={{
                    width: "0.2%",
                    height: "100%",
                    backgroundColor: "#717171",
                    marginLeft: 5
                  }}
                />
                <TouchableOpacity>
                  <Icon3 name="refresh" size={width * 0.02} color="#717171" />
                </TouchableOpacity>
              </View>
            </View>
          </TouchableOpacity>

          {/*Item View2  Ends*/}

          <View
            style={{
              width: "100%",
              height: "0.2%",
              backgroundColor: "#F1ECEC",
              flexDirection: "row"
            }}
          />
          {/*Item View 2 Starts*/}

          <View
            style={{
              width: "100%",
              height: "6%",
              backgroundColor: "white",
              flexDirection: "row"
            }}
          >
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                Completed
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                2187456892
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                05/12/2017
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                Whiskey
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                35
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                Hugh Cibbs
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <TouchableOpacity>
                <Icon4
                  name="file-text-o"
                  size={width * 0.016}
                  color="#717171"
                  style={{ marginTop: 3 }}
                />
              </TouchableOpacity>
              <View
                style={{
                  width: "0.2%",
                  height: "100%",
                  backgroundColor: "#717171",
                  marginLeft: 5
                }}
              />
              <TouchableOpacity>
                <Icon3 name="refresh" size={width * 0.02} color="#717171" />
              </TouchableOpacity>
            </View>
          </View>

          {/*Item View2  Ends*/}

          <View
            style={{
              width: "100%",
              height: "0.2%",
              backgroundColor: "#F1ECEC",
              flexDirection: "row"
            }}
          />
          {/*Item View 2 Starts*/}

          <View
            style={{
              width: "100%",
              height: "6%",
              backgroundColor: "white",
              flexDirection: "row"
            }}
          >
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                Completed
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                2187456892
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                05/12/2017
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                Whiskey
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                35
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                Hugh Cibbs
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <TouchableOpacity>
                <Icon4
                  name="file-text-o"
                  size={width * 0.016}
                  color="#717171"
                  style={{ marginTop: 3 }}
                />
              </TouchableOpacity>
              <View
                style={{
                  width: "0.2%",
                  height: "100%",
                  backgroundColor: "#717171",
                  marginLeft: 5
                }}
              />
              <TouchableOpacity>
                <Icon3 name="refresh" size={width * 0.02} color="#717171" />
              </TouchableOpacity>
            </View>
          </View>

          {/*Item View2  Ends*/}

          <View
            style={{
              width: "100%",
              height: "0.2%",
              backgroundColor: "#F1ECEC",
              flexDirection: "row"
            }}
          />
          {/*Item View 2 Starts*/}

          <View
            style={{
              width: "100%",
              height: "6%",
              backgroundColor: "white",
              flexDirection: "row"
            }}
          >
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                Completed
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                2187456896
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                05/12/2017
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                Whiskey
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                40
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                Wendell Burgess
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <TouchableOpacity>
                <Icon4
                  name="file-text-o"
                  size={width * 0.016}
                  color="#717171"
                  style={{ marginTop: 3 }}
                />
              </TouchableOpacity>
              <View
                style={{
                  width: "0.2%",
                  height: "100%",
                  backgroundColor: "#717171",
                  marginLeft: 5
                }}
              />
              <TouchableOpacity>
                <Icon3 name="refresh" size={width * 0.02} color="#717171" />
              </TouchableOpacity>
            </View>
          </View>

          {/*Item View2  Ends*/}

          <View
            style={{
              width: "100%",
              height: "0.2%",
              backgroundColor: "#F1ECEC",
              flexDirection: "row"
            }}
          />
          {/*Item View 2 Starts*/}

          <View
            style={{
              width: "100%",
              height: "6%",
              backgroundColor: "white",
              flexDirection: "row"
            }}
          >
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                Completed
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                2187456899
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                05/12/2017
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                Whiskey
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                35
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  alignSelf: "center"
                }}
              >
                Wendell Burgess
              </Text>
            </View>
            <View
              style={{
                width: "15%",
                height: "100%",
                backgroundColor: "white",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <TouchableOpacity>
                <Icon4
                  name="file-text-o"
                  size={width * 0.016}
                  color="#717171"
                  style={{ marginTop: 3 }}
                />
              </TouchableOpacity>
              <View
                style={{
                  width: "0.2%",
                  height: "100%",
                  backgroundColor: "#717171",
                  marginLeft: 5
                }}
              />
              <TouchableOpacity>
                <Icon3 name="refresh" size={width * 0.02} color="#717171" />
              </TouchableOpacity>
            </View>
          </View>

          {/*Item View2  Ends*/}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "85%",
    flexDirection: "row",
    flexDirection: "column",
    backgroundColor: "#F1ECEC"
  },
  mainContainer: {
    width: "92%",
    height: "100%",
    backgroundColor: "green",
    flexDirection: "column"
  },
  header: {
    backgroundColor: "#455A64"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  actionViewMain: {
    width: "50%",
    height: "100%",
    flexDirection: "row",
    backgroundColor: "white",
    justifyContent: "flex-start"
  },
  titleMain: {
    fontSize: width * 0.01,
    color: "#4E9197",
    alignSelf: "center",
    fontWeight: "bold",
    paddingLeft: "2%"
  },
  titleSub: {
    fontSize: width * 0.011,
    color: "#848484",
    alignSelf: "center",
    fontWeight: "bold"
  },
  titleSub1: {
    fontSize: width * 0.011,
    color: "#ACACAC",
    alignSelf: "center",
    marginRight: 10
  },
  spaceview: {
    width: "0.5%",
    height: "100%",
    flexDirection: "row",
    backgroundColor: "#F1ECEC"
  },
  itemSection1: {
    width: "100%",
    height: "40%",
    flexDirection: "row",
    backgroundColor: "transparent"
  },
  itemSection2: {
    width: "100%",
    height: "15%",
    flexDirection: "row",
    backgroundColor: "transparent"
  },
  itemSection3: {
    width: "100%",
    height: "15%",
    flexDirection: "row",
    backgroundColor: "transparent"
  },
  itemSection4: {
    marginTop: "5%",
    width: "100%",
    height: "1%",
    flexDirection: "row",
    backgroundColor: "#ECECEC"
  },
  itemSection5: {
    width: "100%",
    height: "20%",
    flexDirection: "row",
    backgroundColor: "transparent"
  },
  renderItemStyle: {
    flex: 1,
    minWidth: width * 0.22,
    maxWidth: width * 0.22,
    height: width * 0.18,
    maxHeight: width * 0.18,
    padding: width * 0.01
  },
  renderImageStyle: {
    marginTop: width * 0.01,
    width: "25%",
    height: "60%",
    resizeMode: "contain",
    alignSelf: "flex-start"
  },
  renderSection1: {
    width: "50%",
    height: "100%",
    padding: width * 0.005,
    flexDirection: "column",
    justifyContent: "center"
  },
  renderText1: {
    fontSize: width * 0.007,
    color: "#848484",
    fontWeight: "bold"
  },
  renderText2: {
    fontSize: width * 0.007,
    color: "#ACACAC",
    marginRight: width * 0.007,
    fontWeight: "bold"
  },
  renderSection2: {
    width: "25%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: width * 0.005
  },
  textSection1: {
    width: "20%",
    height: "100%",
    fontSize: width * 0.007,
    color: "#000",
    fontWeight: "bold"
  },

  textSection2: {
    width: "30%",
    height: "100%",
    fontSize: width * 0.007,
    color: "#ACACAC",
    marginRight: width * 0.007,
    fontWeight: "bold"
  },
  textSection3: {
    width: "20%",
    height: "100%",
    fontSize: width * 0.007,
    color: "#000",
    fontWeight: "bold"
  },
  textSection4: {
    width: "80%",
    height: "100%",
    fontSize: width * 0.007,
    color: "#ACACAC",
    marginRight: width * 0.01,
    fontWeight: "bold"
  },
  textSection5: {
    fontSize: width * 0.007,
    color: "#53939A",
    fontWeight: "bold",
    marginLeft: width * 0.004,
    alignSelf: "center"
  },
  viewSecion: {
    width: "37%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: width * 0.005
  },
  viewSection1: {
    width: "25%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: width * 0.005
  },

  list: {
    justifyContent: "center",
    flexDirection: "row",
    flexWrap: "wrap",
    margin: width * 0.012,
    alignItems: "center"
  },

  mainContainer: {
    width: "92%",
    height: "100%",
    backgroundColor: "green",
    flexDirection: "column"
  },
  header: {
    backgroundColor: "#455A64"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  itemStyle: {
    width: "100%",
    height: "100%",
    flexDirection: "column",
    backgroundColor: "white",

    paddingLeft: width * 0.008,
    paddingRight: width * 0.008,
    borderWidth: 0.5,
    borderRadius: width * 0.008,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#E8E5E5",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1
  },
  actionView: {
    width: "45%",
    height: "100%",
    flexDirection: "row",
    padding: 5,
    justifyContent: "space-between",
    backgroundColor: "white"
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  cardStyle: {
    justifyContent: "space-between",
    backgroundColor: "#FFF",
    width: "99%",
    height: "7%",
    flexDirection: "row",
    backgroundColor: "white",
    alignSelf: "center",
    paddingLeft: width * 0.004,
    paddingRight: width * 0.004,
    borderWidth: 0.5,
    borderRadius: width * 0.003,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#E8E5E5",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1
  }
});
