import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  StatusBar,
  ScrollView,
  View
} from "react-native";

export default class packageDetail extends Component {
  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{ marginLeft: "2%", marginRight: "2%" }}>
          <View style={styles.mainView}>
            <View style={styles.section1}>
              <View style={styles.section11}>
                <Text style={styles.headerText}> PACKAGING DETAILS</Text>
              </View>

              <View style={styles.section12}>
                <View style={styles.section121}>
                  <View style={styles.section1211}>
                    <View style={styles.section12111}>
                      <Text style={styles.text1}> BATCH NAME</Text>
                    </View>
                    <View style={styles.section12111}>
                      <Text style={styles.text2}>170815.32</Text>
                    </View>
                  </View>

                  <View style={styles.section1211}>
                    <View style={styles.section12111}>
                      <Text style={styles.text1}> IN HOUSE BATCH NAME</Text>
                    </View>
                    <View style={styles.section12111}>
                      <Text style={styles.text2}>DEMO</Text>
                    </View>
                  </View>

                  <View style={styles.section1211}>
                    <View style={styles.section12111}>
                      <Text style={styles.text1}>BATCH START DATE</Text>
                    </View>
                    <View style={styles.section12111}>
                      <Text style={styles.text2}>05/11/2017 11:20 AM</Text>
                    </View>
                  </View>

                  <View style={styles.section1211}>
                    <View style={styles.section12111}>
                      <Text style={styles.text1}>BATCH COMPLETED DATE</Text>
                    </View>
                    <View style={styles.section12111}>
                      <Text style={styles.text2}>05/11/2017 11:20 AM</Text>
                    </View>
                  </View>
                </View>

                <View style={styles.section122}>
                  <View style={styles.section1211}>
                    <View style={styles.section12111}>
                      <Text style={styles.text1}> BATCH NAME1</Text>
                    </View>
                    <View style={styles.section12111}>
                      <Text style={styles.text2}>170815.32</Text>
                    </View>
                  </View>

                  <View style={styles.section1211}>
                    <View style={styles.section12111}>
                      <Text style={styles.text1}> IN HOUSE BATCH NAME</Text>
                    </View>
                    <View style={styles.section12111}>
                      <Text style={styles.text2}>DEMO</Text>
                    </View>
                  </View>

                  <View style={styles.section1211}>
                    <View style={styles.section12111}>
                      <Text style={styles.text1}>BATCH START DATE</Text>
                    </View>
                    <View style={styles.section12111}>
                      <Text style={styles.text2}>05/11/2017 11:20 AM</Text>
                    </View>
                  </View>

                  <View style={styles.section1211}>
                    <View style={styles.section12111}>
                      <Text style={styles.text1}>BATCH COMPLETED DATE</Text>
                    </View>
                    <View style={styles.section12111}>
                      <Text style={styles.text2}>05/11/2017 11:20 AM</Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
            <View style={styles.section2}>
              <View style={styles.section21}>
                <View style={styles.section211}>
                  <View style={styles.sectionHeaderView}>
                    <Text style={styles.headerText}>
                      {" "}
                      FINISHED PRODUCT DETAILS
                    </Text>
                  </View>
                  <View style={styles.section2112}>
                    <View style={styles.section21121}>
                      <View style={styles.section21121a}>
                        <View style={styles.section12111}>
                          <Text style={styles.text1}>NAME</Text>
                        </View>
                        <View style={styles.section12111}>
                          <Text style={styles.text2}>Whiskey</Text>
                        </View>
                      </View>
                      <View style={styles.spaceView} />

                      <View style={styles.section21121a}>
                        <View style={styles.section12111}>
                          <Text style={styles.text1}>CLASS/TYPE</Text>
                        </View>
                        <View style={styles.section12111}>
                          <Text style={styles.text2}>BOURBONE WHISHKEY</Text>
                        </View>
                      </View>
                      <View style={styles.spaceView} />
                    </View>

                    <View style={styles.section21121}>
                      <View style={styles.section21121a}>
                        <View style={styles.section12111}>
                          <Text style={styles.text1}>PROOF</Text>
                        </View>
                        <View style={styles.section12111}>
                          <Text style={styles.text2}>80.90</Text>
                        </View>
                      </View>
                      <View style={styles.spaceView} />

                      <View style={styles.section21121a}>
                        <View style={styles.section12111}>
                          <Text style={styles.text1}>Volume</Text>
                        </View>
                        <View style={styles.section12111}>
                          <Text style={styles.text2}>750.00 ml</Text>
                        </View>
                      </View>
                      <View style={styles.spaceView} />
                    </View>
                  </View>
                </View>
                <View style={styles.section212}>
                  <View style={styles.sectionHeaderView}>
                    <Text style={styles.headerText}> VESSEL/BATCH DETAILS</Text>
                  </View>
                  <View style={styles.section2112}>
                    <View style={styles.section21121}>
                      <View style={styles.section21121a}>
                        <View style={styles.section12111}>
                          <Text style={styles.text1}>BATCH NAME</Text>
                        </View>
                        <View style={styles.section12111}>
                          <Text style={styles.text2}>170701.01</Text>
                        </View>
                      </View>
                      <View style={styles.spaceView} />

                      <View style={styles.section21121a}>
                        <View style={styles.section12111}>
                          <Text style={styles.text1}>OBSERVED PRROF</Text>
                        </View>
                        <View style={styles.section12111}>
                          <Text style={styles.text2}>80.90</Text>
                        </View>
                      </View>
                      <View style={styles.spaceView} />

                      <View style={styles.section21121a}>
                        <View style={styles.section12111}>
                          <Text style={styles.text1}>TRUE PROOF</Text>
                        </View>
                        <View style={styles.section12111}>
                          <Text style={styles.text2}>80.00</Text>
                        </View>
                      </View>
                      <View style={styles.spaceView} />
                    </View>

                    <View style={styles.section21121}>
                      <View style={styles.section21121a}>
                        <View style={styles.section12111}>
                          <Text style={styles.text1}>VESSEL NAME</Text>
                        </View>
                        <View style={styles.section12111}>
                          <Text style={styles.text2}>Bottling Tank</Text>
                        </View>
                      </View>
                      <View style={styles.spaceView} />

                      <View style={styles.section21121a}>
                        <View style={styles.section12111}>
                          <Text style={styles.text1}>TEMPERATURE</Text>
                        </View>
                        <View style={styles.section12111}>
                          <Text style={styles.text2}>60.00</Text>
                        </View>
                      </View>
                      <View style={styles.spaceView} />
                    </View>
                  </View>
                </View>
              </View>

              <View style={styles.section22}>
                <View style={styles.sectionHeaderView}>
                  <Text
                    style={[
                      styles.headerText,
                      { marginLeft: "5%", marginTop: "-10%" }
                    ]}
                  >
                    COST SUMMARY
                  </Text>
                </View>

                <View
                  style={[
                    styles.section21121a,
                    {
                      height: "10%",
                      backgroundColor: "transparent",
                      marginTop: "-10%"
                    }
                  ]}
                >
                  <View style={styles.section12111}>
                    <Text style={styles.text1}>RAW MATERIALS</Text>
                  </View>
                  <View style={styles.section12111}>
                    <Text style={styles.text2}>403.45</Text>
                  </View>
                </View>
                <View style={styles.spaceView} />

                <View
                  style={[
                    styles.section21121a,
                    { height: "10%", backgroundColor: "transparent" }
                  ]}
                >
                  <View style={styles.section12111}>
                    <Text style={styles.text1}>LABOUR $0.00</Text>
                  </View>
                  <View style={styles.section12111}>
                    <Text style={styles.text2}>60.00</Text>
                  </View>
                </View>
                <View style={styles.spaceView} />

                <View
                  style={[
                    styles.section21121a,
                    { height: "10%", backgroundColor: "transparent" }
                  ]}
                >
                  <View style={styles.section12111}>
                    <Text style={styles.text1}>PAKAGING </Text>
                  </View>
                  <View style={styles.section12111}>
                    <Text style={styles.text2}>0.00</Text>
                  </View>
                </View>
                <View style={styles.spaceView} />

                <View
                  style={[
                    styles.section21121a,
                    { height: "10%", backgroundColor: "transparent" }
                  ]}
                >
                  <View style={styles.section12111}>
                    <Text style={styles.text1}>ENERGY $0.00</Text>
                  </View>
                  <View style={styles.section12111}>
                    <Text style={styles.text2}>Bottling Tank</Text>
                  </View>
                </View>
                <View style={styles.spaceView} />

                <View
                  style={[
                    styles.section21121a,
                    { height: "10%", backgroundColor: "transparent" }
                  ]}
                >
                  <View style={styles.section12111}>
                    <Text style={styles.text1}>TAXES</Text>
                  </View>
                  <View style={styles.section12111}>
                    <Text style={styles.text2}>$406.51</Text>
                  </View>
                </View>
                <View style={styles.spaceView} />

                <View
                  style={[
                    styles.section21121a,
                    { height: "10%", backgroundColor: "#009D9C" }
                  ]}
                >
                  <View
                    style={[
                      styles.section12111,
                      { justifyContent: "flex-end" }
                    ]}
                  >
                    <Text style={[styles.text1, { color: "white" }]}>
                      TOTAL
                    </Text>
                  </View>
                  <View style={styles.section12111}>
                    <Text
                      style={[
                        styles.text2,
                        { color: "white", marginRight: ".5%" }
                      ]}
                    >
                      $809.97
                    </Text>
                  </View>
                </View>
                <View style={styles.spaceView} />

                <View
                  style={[
                    styles.section21121a,
                    { height: "10%", backgroundColor: "transparent" }
                  ]}
                >
                  <View style={styles.section12111}>
                    <Text style={styles.text1}>PER PG</Text>
                  </View>
                  <View style={styles.section12111}>
                    <Text style={styles.text2}>$13.40</Text>
                  </View>
                </View>
                <View style={styles.spaceView} />

                <View
                  style={[
                    styles.section21121a,
                    { height: "10%", backgroundColor: "transparent" }
                  ]}
                >
                  <View style={styles.section12111}>
                    <Text style={styles.text1}>PER UNIT</Text>
                  </View>
                  <View style={styles.section12111}>
                    <Text style={styles.text2}>$4.26</Text>
                  </View>
                </View>
              </View>
            </View>
            <View style={styles.section3}>
              <View style={styles.sectionHeaderView}>
                <Text style={[styles.headerText]}>COST SUMMARY</Text>
              </View>

              <View style={styles.section31}>
                <View style={styles.section312}>
                  <Text
                    style={[
                      styles.text2,
                      { fontSize: 17, alignSelf: "center", color: "#505050" }
                    ]}
                  >
                    TEST #
                  </Text>
                </View>
                <View style={styles.section313}>
                  <Text
                    style={[
                      styles.text2,
                      { fontSize: 17, alignSelf: "center", color: "#505050" }
                    ]}
                  >
                    Proof And Variation Percentage
                  </Text>
                </View>
                <View style={styles.section313}>
                  <Text
                    style={[
                      styles.text2,
                      { fontSize: 17, alignSelf: "center", color: "#505050" }
                    ]}
                  >
                    Fill Volume And Variation Percentage
                  </Text>
                </View>
                <View
                  style={{
                    width: "25%",
                    height: "100%",
                    backgroundColor: "white",
                    flexDirection: "row"
                  }}
                />
              </View>

              <View style={styles.spaceView} />

              <View style={styles.section31}>
                <View style={styles.section312}>
                  <Text
                    style={[
                      styles.text2,
                      { fontSize: 17, alignSelf: "center", color: "#C0C0C0" }
                    ]}
                  >
                    1
                  </Text>
                </View>
                <View style={styles.section313}>
                  <Text
                    style={[
                      styles.text2,
                      { fontSize: 17, alignSelf: "center", color: "#C0C0C0" }
                    ]}
                  >
                    80.00 Proof/0.000096
                  </Text>
                </View>
                <View style={styles.section313}>
                  <Text
                    style={[
                      styles.text2,
                      { fontSize: 17, alignSelf: "center", color: "#C0C0C0" }
                    ]}
                  >
                    75.00 ML (0.00 Grams) 0.000096
                  </Text>
                </View>
                <View style={styles.section315} />
              </View>

              <View style={styles.spaceView} />

              <View style={styles.section31}>
                <View style={styles.section312}>
                  <Text
                    style={[
                      styles.text2,
                      { fontSize: 17, alignSelf: "center", color: "#C0C0C0" }
                    ]}
                  >
                    2
                  </Text>
                </View>
                <View style={styles.section313}>
                  <Text
                    style={[
                      styles.text2,
                      { fontSize: 17, alignSelf: "center", color: "#C0C0C0" }
                    ]}
                  >
                    80.00 Proof/0.000096
                  </Text>
                </View>
                <View style={styles.section313}>
                  <Text
                    style={[
                      styles.text2,
                      { fontSize: 17, alignSelf: "center", color: "#C0C0C0" }
                    ]}
                  >
                    75.00 ML (0.00 Grams) 0.000096
                  </Text>
                </View>
                <View style={styles.section315} />
              </View>

              <View style={styles.spaceView} />
            </View>

            <View style={styles.section4}>
              <View style={styles.sectionHeaderView}>
                <Text style={styles.headerText}>ASSEMBLY ITEM CONSUMED</Text>
              </View>

              <View style={styles.section31}>
                <View style={styles.section316}>
                  <Text
                    style={[
                      styles.text2,
                      { fontSize: 17, alignSelf: "center", color: "#505050" }
                    ]}
                  >
                    Inventry Item
                  </Text>
                </View>
                <View style={styles.section317}>
                  <Text
                    style={[
                      styles.text2,
                      { fontSize: 17, alignSelf: "center", color: "#505050" }
                    ]}
                  >
                    Dependent Count
                  </Text>
                </View>
                <View style={styles.section317}>
                  <Text
                    style={[
                      styles.text2,
                      { fontSize: 17, alignSelf: "center", color: "#505050" }
                    ]}
                  >
                    Average Cost Per Unit
                  </Text>
                </View>
                <View style={styles.section317}>
                  <Text
                    style={[
                      styles.text2,
                      { fontSize: 17, alignSelf: "center", color: "#505050" }
                    ]}
                  >
                    Total Cost Per Unit
                  </Text>
                </View>
              </View>

              <View style={styles.spaceView} />

              <View style={styles.section31}>
                <View style={styles.section316}>
                  <Text
                    style={[
                      styles.text2,
                      { fontSize: 17, alignSelf: "center", color: "#C0C0C0" }
                    ]}
                  >
                    1
                  </Text>
                </View>
                <View style={styles.section317}>
                  <Text
                    style={[
                      styles.text2,
                      { fontSize: 17, alignSelf: "center", color: "#C0C0C0" }
                    ]}
                  >
                    80.00 Proof/0.000096
                  </Text>
                </View>
                <View style={styles.section317}>
                  <Text
                    style={[
                      styles.text2,
                      { fontSize: 17, alignSelf: "center", color: "#C0C0C0" }
                    ]}
                  >
                    75.00 ML (0.00 Grams) 0.000096
                  </Text>
                </View>
                <View style={styles.section317}>
                  <Text
                    style={[
                      styles.text2,
                      { fontSize: 17, alignSelf: "center", color: "#C0C0C0" }
                    ]}
                  >
                    75.00 ML (0.00 Grams) 0.000096
                  </Text>
                </View>
              </View>

              <View style={styles.spaceView} />
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "85%",
    flexDirection: "row",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F1ECEC"
  },

  mainView: {
    backgroundColor: "#EFEAEA",
    width: "100%",
    flexDirection: "column",
    height: 1300,
    justifyContent: "space-between"
  },
  spaceView: {
    width: "95%",
    height: "0.5%",
    backgroundColor: "#F1ECEC",
    justifyContent: "center"
  },

  mainContainer: {
    width: "92%",
    height: "100%",
    backgroundColor: "transparent",
    flexDirection: "column"
  },
  header: {
    backgroundColor: "#455A64"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  section1: {
    width: "100%",
    height: "19.5%",
    backgroundColor: "white",
    flexDirection: "column",
    justifyContent: "space-between"
  },
  section2: {
    width: "100%",
    height: "39%",
    backgroundColor: "#E6E6E6",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  section3: {
    width: "100%",
    height: "19.5%",
    backgroundColor: "white",
    flexDirection: "column",
    justifyContent: "flex-start"
  },
  section4: {
    width: "100%",
    height: "19.5%",
    backgroundColor: "white",
    flexDirection: "column",
    justifyContent: "flex-start"
  },
  section21: {
    width: "69%",
    height: "100%",
    backgroundColor: "#E6E6E6",
    flexDirection: "column",
    justifyContent: "space-between"
  },
  section22: {
    width: "30%",
    height: "100%",
    backgroundColor: "white",
    padding: "1%",
    flexDirection: "column"
  },
  section211: {
    width: "100%",
    height: "49.5%",
    backgroundColor: "white",
    flexDirection: "column"
  },
  section212: {
    width: "100%",
    height: "49.5%",
    backgroundColor: "white"
  },
  section11: {
    width: "100%",
    height: "17%",
    backgroundColor: "#E6E6E6",
    flexDirection: "column",
    justifyContent: "center",
    padding: "0.2%"
  },
  section12: {
    width: "100%",
    height: "83%",
    backgroundColor: "transparent",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  headerText: {
    fontSize: 17,
    alignSelf: "flex-start",
    color: "#52B6B5",
    fontWeight: "bold",
    marginLeft: "1%"
  },
  section121: {
    width: "50%",
    height: "100%",
    backgroundColor: "#F1ECEC",
    flexDirection: "column",
    justifyContent: "space-between"
  },
  section122: {
    width: "50%",
    height: "100%",
    backgroundColor: "#F1ECEC",
    flexDirection: "column",
    justifyContent: "space-between"
  },
  section1211: {
    width: "99%",
    height: "24.5%",
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "white",
    alignSelf: "center",
    padding: "1%",
    alignItems: "center"
  },
  section12111: {
    width: "50%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "flex-start",
    backgroundColor: "transparent",
    alignSelf: "center",
    padding: "1%",
    alignItems: "center"
  },
  text1: {
    fontSize: 17,
    color: "#B3B3B3",
    fontWeight: "bold"
  },
  text2: {
    fontSize: 15,
    color: "#8C8C8C",
    fontWeight: "bold"
  },
  sectionHeaderView: {
    height: "20%",
    width: "100%",
    flexDirection: "column",
    justifyContent: "center",
    backgroundColor: "transparent"
  },
  section2112: {
    height: "80%",
    width: "100%",
    flexDirection: "row",
    justifyContent: "flex-start",
    padding: "1%",
    backgroundColor: "transparent"
  },
  section21121: {
    height: "100%",
    width: "50%",
    flexDirection: "column",
    justifyContent: "flex-start",
    backgroundColor: "transparent"
  },
  section21121a: {
    width: "100%",
    height: "20%",
    backgroundColor: "transparent",
    flexDirection: "row",
    padding: "0.5%"
  },
  section21122: {
    height: "100%",
    width: "50%",
    flexDirection: "row",
    justifyContent: "flex-start",
    backgroundColor: "transparent"
  },
  section31: {
    width: "100%",
    height: "20%",
    backgroundColor: "white",
    flexDirection: "row"
  },
  section312: {
    width: "15%",
    height: "100%",
    backgroundColor: "white",
    flexDirection: "row",
    justifyContent: "flex-start",
    paddingLeft: "2%"
  },
  section313: {
    width: "30%",
    height: "100%",
    backgroundColor: "white",
    flexDirection: "row",
    justifyContent: "flex-start",
    paddingLeft: "2%"
  },
  section314: {
    width: "25%",
    height: "100%",
    backgroundColor: "white",
    flexDirection: "row"
  },
  section315: {
    width: "25%",
    height: "100%",
    backgroundColor: "white",
    flexDirection: "row"
  },
  section316: {
    width: "19%",
    height: "100%",
    backgroundColor: "white",
    flexDirection: "row",
    justifyContent: "flex-start",
    paddingLeft: "2%"
  },
  section317: {
    width: "27%",
    height: "100%",
    backgroundColor: "white",
    flexDirection: "row",
    justifyContent: "flex-start",
    paddingLeft: "2%"
  }
});
