import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  StatusBar,
  TextInput,
  Dimensions,
  TouchableOpacity,
  View
} from "react-native";

var { height, width } = Dimensions.get("window");
import Icon from "react-native-vector-icons/dist/FontAwesome";
import Icon4 from "react-native-vector-icons/dist/FontAwesome";
import LinearGradient from "react-native-linear-gradient";

export default class Header extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      seeAllBatches: false
    };

    this._seeAllBatches = this._seeAllBatches.bind(this);
  }

  componentWillReceiveProps(newProps) {
    console.log("" + newProps.value.seeAllBatches);
    console.log("value reset");
    console.log(newProps);
    this.setState({ seeAllBatches: newProps.value.seeAllBatches });
  }

  _seeAllBatches() {
    this.setState({ seeAllBatches: !this.state.seeAllBatches });
    this.props._seeAllBatches(!this.state.seeAllBatches);
  }

  render() {
    //_seeAllBatches
    const SearchIcon = (
      <Icon name="search" size={width * 0.02} color="#717171" />
    );
    console.log(this.props.value);
    return (
      <View style={styles.container}>
        <View style={styles.headerView}>
          <Text style={styles.welcome}>Operations Managment</Text>
        </View>
        <View style={styles.operationView}>
          {this.props.value.active !== "Overview" &&
            this.props.value.active !== "PackageDetail" && (
              <View style={styles.searchStyle}>
                {SearchIcon}
                <TextInput
                  style={{
                    width: "90%",
                    marginLeft: width * 0.001,
                    fontSize: width * 0.011
                  }}
                  onChangeText={text => this.setState({ search: text })}
                  placeholder="Enter Batch Name or Number"
                  underlineColorAndroid="rgba(0,0,0,0)"
                />
              </View>
            )}

          {this.props.value.active === "ActiveBatchesDetail" &&
            !this.state.seeAllBatches && (
              <TouchableOpacity
                onPress={() => this._seeAllBatches()}
                style={{
                  width: "25%",
                  height: "70%",
                  backgroundColor: "#F7F5F5",
                  padding: "2%",
                  alignSelf: "center",
                  borderRadius: width * 0.005,
                  borderWidth: 1,
                  borderColor: "#FFF"
                }}
              >
                <View
                  style={{
                    width: "100%",
                    height: "100%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      fontSize: width * 0.008,
                      fontWeight: "bold",
                      marginLeft: "10%"
                    }}
                  >
                    See All Batches
                  </Text>
                  <Icon4
                    name="angle-down"
                    size={width * 0.016}
                    color="#000"
                    style={{ alignSelf: "center", marginLeft: "4%" }}
                  />
                </View>
              </TouchableOpacity>
            )}
          {this.props.value.active === "ActiveBatchesDetail" &&
            this.state.seeAllBatches && (
              <LinearGradient
                colors={["#004F63", "#008963"]}
                style={{
                  width: "25%",
                  height: "70%",
                  backgroundColor: "transparent",
                  padding: "2%",
                  alignSelf: "center",
                  borderRadius: width * 0.005,
                  borderWidth: 1,
                  borderColor: "#FFF"
                }}
              >
                <TouchableOpacity
                  onPress={() => this._seeAllBatches()}
                  style={{
                    width: "100%",
                    height: "100%",
                    backgroundColor: "transparent",
                    alignSelf: "center"
                  }}
                >
                  <View
                    style={{
                      width: "100%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      alignItems: "center"
                    }}
                  >
                    <Text
                      style={{
                        fontSize: width * 0.008,
                        fontWeight: "bold",
                        marginLeft: "10%",
                        color: "#fff"
                      }}
                    >
                      See All Batches
                    </Text>
                    <Icon4
                      name="angle-down"
                      size={width * 0.016}
                      color="#fff"
                      style={{ alignSelf: "center", marginLeft: "4%" }}
                    />
                  </View>
                </TouchableOpacity>
              </LinearGradient>
            )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingLeft: "1%",
    paddingRight: "1%",
    width: "100%",
    height: "8%",
    flexDirection: "row",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#F1ECEC"
  },
  searchStyle: {
    height: "80%",
    width: "50%",
    flexDirection: "row",
    marginLeft: width * 0.015,
    margin: width * 0.008,
    backgroundColor: "#F7F5F5",
    alignSelf: "center",
    alignItems: "center",

    paddingLeft: width * 0.008,
    paddingRight: width * 0.008,
    borderWidth: width * 0.0005,
    borderRadius: width * 0.015,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#E8E5E5",
    shadowOffset: { width: 0, height: width * 0.002 },
    shadowOpacity: 0.8,
    shadowRadius: width * 0.002,
    elevation: width * 0.001
  },

  headerView: {
    width: "40%",
    height: "100%",
    backgroundColor: "transparent",
    flexDirection: "row"
  },

  mainContainer: {
    width: "92%",
    height: "100%",
    backgroundColor: "green",
    flexDirection: "column"
  },
  header: {
    backgroundColor: "#455A64"
  },
  welcome: {
    alignSelf: "flex-start",
    fontSize: width * 0.025,
    textAlign: "center",
    color: "#A9A8A8",
    fontWeight: "normal",
    margin: width * 0.01
  },
  operationView: {
    width: "60%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "flex-end"
  }
});
