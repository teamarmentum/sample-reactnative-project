import React, { Component } from "react";
import { AppRegistry, StyleSheet, Text, StatusBar, View } from "react-native";
import ActionButton from "react-native-action-button";

import Icon from "react-native-vector-icons/Ionicons";

export default class OverView extends Component {
  updateDialogValue(value) {
    console.log("do stuff " + value);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to OverView.</Text>
        <ActionButton buttonColor="#E39700">
          <ActionButton.Item
            buttonColor="#9b59b6"
            title="CREATE NEW BATCH"
            onPress={() => this.props.showDialog("CreateNewBatch", "Mukesh")}
          >
            <Icon name="md-create" style={styles.actionButtonIcon} />
          </ActionButton.Item>
        </ActionButton>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "85%",
    flexDirection: "row",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },

  mainContainer: {
    width: "92%",
    height: "100%",
    backgroundColor: "green",
    flexDirection: "column"
  },
  header: {
    backgroundColor: "#455A64"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
