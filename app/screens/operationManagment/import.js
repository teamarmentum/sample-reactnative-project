import React, { Component } from 'react';
import {AppRegistry,StyleSheet,Text,StatusBar,View} from 'react-native';

import ActiveBatches from '@screen/operationManagment/activeBatches';
import CompletedStages from '@screen/operationManagment/completedStage';
import InBondTransfer from '@screen/operationManagment/inBondTransfer';
import Overview from '@screen/operationManagment/overView';
import Packages from '@screen/operationManagment/package';

import BottomNavigtion from './bottomNavigation';
import { TabRouter ,StackNavigator} from 'react-navigation';
