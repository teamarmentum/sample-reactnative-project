import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  StatusBar,
  TouchableOpacity,
  Image,
  Dimensions,
  View
} from "react-native";
import Icon from "react-native-vector-icons/dist/Octicons";
import Icon1 from "react-native-vector-icons/dist/EvilIcons";
import Icon2 from "react-native-vector-icons/dist/Ionicons";
import Icon3 from "react-native-vector-icons/dist/MaterialIcons";

import LinearGradient from "react-native-linear-gradient";

var { height, width } = Dimensions.get("window");

export default class BottomNavigtion extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      active: "Overview"
    };
    this._selectTab = this._selectTab.bind(this);
  }

  _selectTab(key) {
    this.setState({ active: key });
    this.props.method(key);
  }

  render() {
    return (
      <LinearGradient colors={["#004F63", "#008963"]} style={styles.container}>
        <TouchableOpacity
          style={styles.bottomTabselection}
          onPress={() => this._selectTab("Overview")}
        >
          <View style={styles.boootmParent}>
            <View style={styles.iconView}>
              {this.state.active === "Overview" && (
                <Icon
                  name="triangle-down"
                  size={width * 0.018}
                  color="#CD800C"
                  style={{ marginTop: -(width * 0.005) }}
                />
              )}
            </View>
            <View style={styles.buttonText}>
              <Text
                style={
                  this.state.active != "Overview"
                    ? styles.bottomText
                    : styles.bottomTextSelected
                }
              >
                Overview
              </Text>
            </View>
          </View>
        </TouchableOpacity>

        <View
          style={{
            flex: 0.002,
            flexDirection: "row",
            backgroundColor: "white"
          }}
        />

        <TouchableOpacity
          style={styles.bottomTabselection}
          onPress={() => this._selectTab("ActiveBatches")}
        >
          <View style={styles.boootmParent}>
            <View style={styles.iconView}>
              {this.state.active === "ActiveBatches" && (
                <Icon
                  name="triangle-down"
                  size={width * 0.018}
                  color="#CD800C"
                  style={{ marginTop: -(width * 0.005) }}
                />
              )}
            </View>
            <View style={styles.buttonText}>
              <Text
                style={
                  this.state.active != "ActiveBatches"
                    ? styles.bottomText
                    : styles.bottomTextSelected
                }
              >
                Active Batches
              </Text>
            </View>
          </View>
        </TouchableOpacity>

        <View
          style={{
            flex: 0.002,
            flexDirection: "row",
            backgroundColor: "white"
          }}
        />

        <TouchableOpacity
          style={styles.bottomTabselection}
          onPress={() => this._selectTab("InBondTransfer")}
        >
          <View style={styles.boootmParent}>
            <View style={styles.iconView}>
              {this.state.active === "InBondTransfer" && (
                <Icon
                  name="triangle-down"
                  size={width * 0.018}
                  color="#CD800C"
                  style={{ marginTop: -(width * 0.005) }}
                />
              )}
            </View>
            <View style={styles.buttonText}>
              <Text
                style={
                  this.state.active != "InBondTransfer"
                    ? styles.bottomText
                    : styles.bottomTextSelected
                }
              >
                In Bond Transfer
              </Text>
            </View>
          </View>
        </TouchableOpacity>

        <View
          style={{
            flex: 0.002,
            flexDirection: "row",
            backgroundColor: "white"
          }}
        />

        <TouchableOpacity
          style={styles.bottomTabselection}
          onPress={() => this._selectTab("CompletedStages")}
        >
          <View style={styles.boootmParent}>
            <View style={styles.iconView}>
              {this.state.active === "CompletedStages" && (
                <Icon
                  name="triangle-down"
                  size={width * 0.018}
                  color="#CD800C"
                  style={{ marginTop: -(width * 0.005) }}
                />
              )}
            </View>
            <View style={styles.buttonText}>
              <Text
                style={
                  this.state.active != "CompletedStages"
                    ? styles.bottomText
                    : styles.bottomTextSelected
                }
              >
                Completed Stages
              </Text>
            </View>
          </View>
        </TouchableOpacity>

        <View
          style={{
            flex: 0.002,
            flexDirection: "row",
            backgroundColor: "white"
          }}
        />

        <TouchableOpacity
          style={styles.bottomTabselection}
          onPress={() => this._selectTab("Packages")}
        >
          <View style={styles.boootmParent}>
            <View style={styles.iconView}>
              {this.state.active === "Packages" && (
                <Icon
                  name="triangle-down"
                  size={width * 0.018}
                  color="#CD800C"
                  style={{ marginTop: -(width * 0.005) }}
                />
              )}
            </View>
            <View style={styles.buttonText}>
              <Text
                style={
                  this.state.active != "Packages"
                    ? styles.bottomText
                    : styles.bottomTextSelected
                }
              >
                Packages
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    height: "100%",
    flexDirection: "row"
  },
  iconView: {
    height: "30%",
    width: "100%",
    flexDirection: "row",
    justifyContent: "center"
  },

  boootmParent: {
    height: "100%",
    width: "100%",
    flexDirection: "column",
    justifyContent: "flex-start"
  },

  buttonText: {
    height: "80%",
    width: "100%",
    flexDirection: "column",
    justifyContent: "flex-start",
    paddingTop: width * 0.003,
    marginTop: -(width * 0.003)
  },
  mainContainer: {
    width: "92%",
    height: "100%",
    backgroundColor: "green",
    flexDirection: "column"
  },
  header: {
    backgroundColor: "#455A64"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  bottomTabselection: {
    flex: 0.245,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
    paddingLeft: 5
  },
  bottomTabselected: {
    flex: 0.245,
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "transparent",
    paddingLeft: 5
  },
  bottomText: {
    fontSize: width * 0.011,
    color: "#FFF",
    alignSelf: "center",
    fontWeight: "normal"
  },
  bottomTextSelected: {
    color: "#A3B760",
    fontSize: width * 0.011,
    alignSelf: "center",
    fontWeight: "normal"
  }
});
