import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  KeyboardAvoidingView,
  TouchableOpacity,
  Keyboard,
  Image,
  TextInput,
  Dimensions,
  View,
  TouchableHighlight,
  StatusBar,
  ScrollView
} from "react-native";
import { Actions } from "react-native-router-flux";
import Orientation from "react-native-orientation";
import { connect } from "react-redux";
import { addCard, addValues } from "../actions";
import { Container, Header, Content, Card, CardItem, Body } from "native-base";
import Icon from "react-native-vector-icons/dist/FontAwesome";
import Icon1 from "react-native-vector-icons/dist/EvilIcons";
import { ActionCreators } from "../actions";
import { bindActionCreators } from "redux";
import { getSizeInDp } from "@lib/Dimensions";
//import { getSizeInDp } from '../lib/Dimensions';
var { height, width } = Dimensions.get("window");
import Modal from "react-native-simple-modal";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import BarrelManagement from "@screen/barrelManagement/home";
import Dashboard from "@screen/dashboard/home";
import Inventory from "@screen/inventory/home";
import OperationManagment from "@screen/operationManagment/home";
import Sales from "@screen/sales/home";

import NavigationBar from "@screen/navigationBar";
import PopupDialog, {
  DialogTitle,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
  FadeAnimation
} from "react-native-popup-dialog";

import { TabRouter, StackNavigator } from "react-navigation";
import {
  Transfer,
  Record,
  AddIngredients,
  AddNote,
  DumpTowaste,
  EditActiveBatch,
  DeleteBatch,
  DeleteBatchHistory,
  ProofSpirits,
  GaugeSpirits,
  FillBarrel,
  Withdraw,
  CreateNewBatch
} from "@dialog";

const TabRoute = TabRouter(
  {
    BarrelManagement: { screen: BarrelManagement },
    Dashboard: { screen: Dashboard },
    Inventory: { screen: Inventory },
    OperationManagment: { screen: OperationManagment },
    Sales: { screen: Sales }
  },
  {
    initialRouteName: "BarrelManagement"
  }
);

const DialogRoute = TabRouter(
  {
    Transfer: { screen: Transfer },
    Record: { screen: Record },
    AddIngredients: { screen: AddIngredients },
    AddNote: { screen: AddNote },
    DumpTowaste: { screen: DumpTowaste },
    EditActiveBatch: { screen: EditActiveBatch },
    DeleteBatch: { screen: DeleteBatch },
    DeleteBatchHistory: { screen: DeleteBatchHistory },
    ProofSpirits: { screen: ProofSpirits },
    GaugeSpirits: { screen: GaugeSpirits },
    FillBarrel: { screen: FillBarrel },
    Withdraw: { screen: Withdraw },
    CreateNewBatch: { screen: CreateNewBatch }
  },
  {
    initialRouteName: "Transfer"
  }
);

class DialogContentNavigator extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      dialog: props.value.dialog,
      input: props.value.Input,
      method: props.method,
      open: false
    };
  }

  componentWillReceiveProps(newProps) {
    this.setState({
      dialog: newProps.value.dialog
    });
  }

  render() {
    const Component = DialogRoute.getComponentForRouteName(this.state.dialog);
    return (
      <Component
        data={this.state}
        method={this.props.method}
        sendData={this.props.sendData}
      />
    );
  }
}

class TabContentNavigator extends Component {
  constructor(props, context) {
    super(props, context);
    //console.log(props);
    this.state = {
      active: props.value.active,
      key: props.value.key,
      method: props.method
    };
  }

  //this method will not get called first time
  componentWillReceiveProps(newProps) {
    console.log(newProps);
    this.setState({
      active: newProps.value.active,
      key: newProps.value.key
    });
  }

  render() {
    const Component = TabRoute.getComponentForRouteName(this.state.active);
    return (
      <Component
        ref={ref => this.props._getChildRef(ref)}
        screenProps={this.state}
        method={this.props.method}
        showDialog={this.props.showDialog}
      />
    );
  }
}

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "Dashboard",
      key: "Dashboard",
      dialog: "Transfer"
    };
  }

  _selectTab = key => {
    this.setState({ active: key, key: key });
  };

  _navigateTo = (path, key) => {};

  /* Method For Dailog Configuration Starts */

  _showDialogMain = (key, input) => {
    this.setState({ dialog: key, open: true, Input: input });
    //  console.log('show dialog '+ key +" "+input);
    //  this.popupDialog.show();
  };

  _getDialogData = value => {
    //  this.popupDialog.dismiss();
    this.setState({ open: false });

    if (value !== undefined) {
      //  console.log(typeof(value));
      this.child1._updateDialogData(value);
    }
  };

  _getChildRef = ref => {
    this.child1 = ref;
  };

  _getWidthHeight() {
    switch (this.state.dialog) {
      case "Transfer":
        return { width: "75%", height: "80%" };
        break;

      case "Record":
        return { width: "60%", height: "70%" };
        break;

      case "AddIngredients":
        return { width: "60%", height: "60%" };
        break;

      case "DumpTowaste":
        return { width: "60%", height: "70%" };
        break;

      case "EditActiveBatch":
        return { width: "50%", height: "50%" };
        break;

      case "EditActiveBatch":
        return { width: "50%", height: "50%" };
        break;

      case "GaugeSpirits":
        return { width: "60%", height: "60%" };
        break;

      case "ProofSpirits":
        return { width: "60%", height: "60%" };
        break;

      case "CreateNewBatch":
        return { width: "70%", height: "85%" };
        break;

      case "FillBarrel":
        return { width: "85%", height: "90%" };
        break;

      default:
        return { width: "55%", height: "50%" };
    }
  }

  /* Method For Dailog Configuration Ends */

  render() {
    return (
      <View style={styles.container}>
        <StatusBar hidden={true} />
        <NavigationBar method={this._selectTab} />

        <TabContentNavigator
          value={this.state}
          key={this.state}
          method={this._navigateTo}
          showDialog={this._showDialogMain}
          _getChildRef={this._getChildRef}
        />

        <Modal
          open={this.state.open}
          offset={this.state.offset}
          overlayBackground={"rgba(0, 0, 0, 0.75)"}
          animationDuration={200}
          animationTension={40}
          modalDidOpen={() => undefined}
          modalDidClose={() => undefined}
          closeOnTouchOutside={true}
          containerStyle={{
            justifyContent: "center",
            alignSelf: "center"
          }}
          modalStyle={{
            borderRadius: 0,
            padding: -10,
            backgroundColor: "#FFF",
            alignSelf: "center",
            width: this._getWidthHeight().width,
            height: this._getWidthHeight().height
          }}
          disableOnBackPress={false}
          style={{ alignItems: "center" }}
        >
          <DialogContentNavigator
            value={this.state}
            sendData={this._getDialogData}
            popupDialog={this.popupDialog}
          />
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    height: "100%",
    flexDirection: "row"
  },
  keyContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  centerView: {
    flex: 0.95,
    width: "55%",
    alignItems: "center",
    backgroundColor: "transparent"
  },
  applogo: {
    justifyContent: "center",
    alignItems: "center",
    flex: 0.3
  },
  loginView: {
    backgroundColor: "transparent",
    flex: 0.6,
    width: "70%",
    height: "75%"
  },

  buttonBackground: {
    borderRadius: 40,
    backgroundColor: "#24767E"
  },

  button: {
    width: "45%",
    height: "45%",
    borderRadius: 50,
    flexDirection: "row",
    backgroundColor: "#25777F",
    borderColor: "#48BBEC",
    margin: 20,
    justifyContent: "center"
  },
  submitText: {
    color: "#fff",
    textAlign: "center"
  },
  viewContainer: {
    backgroundColor: "#2E9298",
    borderRadius: 7,
    padding: 10,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 0.8,
    shadowOpacity: 1.0
  },

  inputView: {
    width: "90%",
    height: "30%",
    flexDirection: "row",
    marginLeft: "8%",
    marginRight: "10%",
    justifyContent: "flex-start",
    alignItems: "center",
    borderBottomWidth: 0.5,
    borderColor: "#EBEBEB"
  },
  inputText: {
    height: "100%",
    width: "100%",
    textAlign: "center",
    fontSize: width * 0.017
  },
  buttonViewBackground: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: "33%",
    marginTop: "2%",
    marginBottom: "2%"
  },
  buttonText: {
    margin: "7%",
    alignSelf: "center",
    color: "#FFF",
    fontSize: width * 0.016
  },
  forgotView: {
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "flex-end"
  },
  forgotBtnBack: {
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "flex-end"
  },
  forgotPassText: {
    color: "#24767E",
    marginTop: 10,
    fontSize: width * 0.012
  },

  applogo: {
    width: "90%",
    height: "90%",
    margin: 10,
    resizeMode: "contain"
  },
  applogoView: {
    width: "50%",
    height: "25%",
    marginTop: 10,
    marginBottom: 15,
    justifyContent: "center",
    alignItems: "center"
  },
  cardStyle: {
    width: "100%",
    height: "85%",
    paddingTop: "1%",
    paddingLeft: "5%",
    paddingRight: "5%",
    position: "relative",
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "white",

    borderWidth: 0.5,
    borderRadius: 2,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#E8E5E5",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1
  },
  cardStyle1: {
    width: "100%",
    height: "65%",
    paddingTop: "1%",
    paddingLeft: "5%",
    paddingRight: "5%",
    position: "relative",
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "white",

    borderWidth: 0.5,
    borderRadius: 2,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#E8E5E5",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1
  }
});
//export default Login
//export default Login
function mapDispatchToprops(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(state => {
  return {
    loginData: state.loginData
  };
}, mapDispatchToprops)(Home);
