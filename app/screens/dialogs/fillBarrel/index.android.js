import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  StatusBar,
  TouchableOpacity,
  Dimensions,
  TextInput,
  View
} from "react-native";

import { Dropdown } from "react-native-material-dropdown";
import { TextInputMask } from "react-native-masked-text";
import ModalDropdown from "react-native-modal-dropdown";

var { height, width } = Dimensions.get("window");
import ActionButton from "react-native-action-button";
import Icon from "react-native-vector-icons/dist/EvilIcons";
import Icon1 from "react-native-vector-icons/dist/FontAwesome";
import { Switch } from "react-native-switch";
import LinearGradient from "react-native-linear-gradient";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

export default class FillBarrel extends Component {
  updateDialogValue(value) {
    console.log("do stuff " + value);
  }

  constructor(props, context) {
    super(props, context);
    this.state = {
      page: 0,
      isShow: true
    };
  }

  goBack = () => {
    switch (this.state.page) {
      case 2:
        this.setState({ page: 1 });
        break;
      default:
        this.setState({ page: 0 });
    }
  };

  goNext = () => {
    switch (this.state.page) {
      case 0:
        this.setState({ page: 1 });
        break;
      case 1:
        this.setState({ page: 2 });
        break;
      default:
        this.setState({ page: 0 });
    }
  };

  getStep1View() {
    let data = [
      { value: "Specific" },
      { value: "Option 1" },
      { value: "Option 2" }
    ];
    return (
      <View style={styles.step}>
        <View
          style={[styles.step, { height: "50%", backgroundColor: "#F2F2F2" }]}
        >
          <View
            style={[
              styles.step,
              {
                height: "10%",
                backgroundColor: "transparent",
                justifyContent: "flex-end",
                flexDirection: "row",
                alignItems: "center"
              }
            ]}
          >
            <Text
              style={[
                styles.smallText,
                {
                  color: "#000",
                  fontSize: 15,
                  fontWeight: "normal",
                  marginRight: 5
                }
              ]}
            >
              FILTER
            </Text>
            <Icon1
              name="filter"
              size={width * 0.017}
              color="#000"
              style={{ alignSelf: "flex-end", marginTop: 3, marginRight: 10 }}
            />
          </View>

          <View
            style={[
              styles.step,
              {
                width: "100%",
                backgroundColor: "transparent",
                flexDirection: "row"
              }
            ]}
          >
            <View
              style={[
                styles.step,
                {
                  width: "33.33%",
                  height: "100%",
                  backgroundColor: "#F2F2F2",
                  flexDirection: "column"
                }
              ]}
            >
              <View
                style={[
                  styles.step,
                  {
                    height: "33.33%",
                    width: "95%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    borderColor: "grey",
                    borderBottomWidth: 1,
                    alignSelf: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "#000",
                      fontSize: 16,
                      fontWeight: "normal",
                      alignSelf: "center",
                      paddingLeft: "2%"
                    }
                  ]}
                >
                  ID/NAME{" "}
                </Text>
                <TextInput
                  style={{
                    width: "50%",
                    marginLeft: width * 0.001,
                    fontSize: width * 0.011,
                    textAlign: "right"
                  }}
                  onChangeText={text => this.setState({ search: text })}
                  placeholder="ID/NAME"
                  underlineColorAndroid="rgba(0,0,0,0)"
                />
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "33.33%",
                    width: "95%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    borderColor: "grey",
                    borderBottomWidth: 1,
                    alignSelf: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "#000",
                      fontSize: 16,
                      fontWeight: "normal",
                      alignSelf: "center",
                      paddingLeft: "2%"
                    }
                  ]}
                >
                  CONDITION{" "}
                </Text>
                <Dropdown
                  value="ANY"
                  data={data}
                  inputContainerStyle={{ borderBottomColor: "transparent" }}
                  pickerStyle={{ marginTop: 90 }}
                  containerStyle={{
                    width: "20%",
                    alignSelf: "center",
                    marginTop: -20,
                    fontSize: 15,
                    marginLeft: 15
                  }}
                />
              </View>

              <View
                style={[
                  styles.step,
                  {
                    height: "33.33%",
                    width: "95%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    borderColor: "grey",
                    borderBottomWidth: 1,
                    alignSelf: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "#000",
                      fontSize: 16,
                      fontWeight: "normal",
                      alignSelf: "center",
                      paddingLeft: "2%"
                    }
                  ]}
                >
                  WOOD-SPECIES
                </Text>
                <Dropdown
                  value="ANY"
                  data={data}
                  inputContainerStyle={{ borderBottomColor: "transparent" }}
                  pickerStyle={{ marginTop: 90 }}
                  containerStyle={{
                    width: "20%",
                    alignSelf: "center",
                    marginTop: -20,
                    fontSize: 15,
                    marginLeft: 15
                  }}
                />
              </View>
            </View>
            <View
              style={[
                styles.step,
                {
                  width: "33.33%",
                  height: "100%",
                  backgroundColor: "#F2F2F2",
                  flexDirection: "column"
                }
              ]}
            >
              <View
                style={[
                  styles.step,
                  {
                    height: "33.33%",
                    width: "95%",
                    backgroundColor: "#F2F2F2",
                    flexDirection: "row",
                    borderColor: "grey",
                    borderBottomWidth: 1,
                    alignSelf: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "#000",
                      fontSize: 16,
                      fontWeight: "normal",
                      alignSelf: "center",
                      paddingLeft: "2%"
                    }
                  ]}
                >
                  IN USE{" "}
                </Text>
                <Dropdown
                  value="NO"
                  data={data}
                  inputContainerStyle={{ borderBottomColor: "transparent" }}
                  pickerStyle={{ marginTop: 90 }}
                  containerStyle={{
                    width: "20%",
                    alignSelf: "center",
                    marginTop: -20,
                    fontSize: 15,
                    marginLeft: 15
                  }}
                />
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "33.33%",
                    width: "95%",
                    backgroundColor: "#F2F2F2",
                    flexDirection: "row",
                    borderColor: "grey",
                    borderBottomWidth: 1,
                    alignSelf: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "#000",
                      fontSize: 16,
                      fontWeight: "normal",
                      alignSelf: "center",
                      paddingLeft: "2%"
                    }
                  ]}
                >
                  CAPACITY{" "}
                </Text>
                <Dropdown
                  value="ANY"
                  data={data}
                  inputContainerStyle={{ borderBottomColor: "transparent" }}
                  pickerStyle={{ marginTop: 90 }}
                  containerStyle={{
                    width: "20%",
                    alignSelf: "center",
                    marginTop: -20,
                    fontSize: 15,
                    marginLeft: 15
                  }}
                />
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "33.33%",
                    width: "95%",
                    backgroundColor: "#F2F2F2",
                    flexDirection: "row",
                    borderColor: "grey",
                    borderBottomWidth: 1,
                    alignSelf: "center"
                  }
                ]}
              />
            </View>
            <View
              style={[
                styles.step,
                {
                  width: "33.33%",
                  height: "100%",
                  backgroundColor: "#F2F2F2",
                  flexDirection: "column"
                }
              ]}
            >
              <View
                style={[
                  styles.step,
                  {
                    height: "33.33%",
                    width: "95%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    borderColor: "grey",
                    borderBottomWidth: 1,
                    alignSelf: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "#000",
                      fontSize: 16,
                      fontWeight: "normal",
                      alignSelf: "center",
                      paddingLeft: "2%"
                    }
                  ]}
                >
                  PARTIALLY FILED{" "}
                </Text>
                <Dropdown
                  value="ANY"
                  data={data}
                  inputContainerStyle={{ borderBottomColor: "transparent" }}
                  pickerStyle={{ marginTop: 90 }}
                  containerStyle={{
                    width: "20%",
                    alignSelf: "center",
                    marginTop: -20,
                    fontSize: 15,
                    marginLeft: 15
                  }}
                />
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "33.33%",
                    width: "95%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    borderColor: "grey",
                    borderBottomWidth: 1,
                    alignSelf: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "#000",
                      fontSize: 16,
                      fontWeight: "normal",
                      alignSelf: "center",
                      paddingLeft: "2%"
                    }
                  ]}
                >
                  CHAR LEVEL{" "}
                </Text>
                <Dropdown
                  value="ANY"
                  data={data}
                  inputContainerStyle={{ borderBottomColor: "transparent" }}
                  pickerStyle={{ marginTop: 90 }}
                  containerStyle={{
                    width: "20%",
                    alignSelf: "center",
                    marginTop: -20,
                    fontSize: 15,
                    marginLeft: 15
                  }}
                />
              </View>

              <View
                style={[
                  styles.step,
                  {
                    height: "33.33%",
                    width: "95%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    borderColor: "grey",
                    borderBottomWidth: 1,
                    alignSelf: "center",
                    justifyContent: "flex-end"
                  }
                ]}
              >
                <View
                  style={[
                    styles.btnBackground1,
                    { alignSelf: "flex-end", width: 120 }
                  ]}
                >
                  <TouchableOpacity
                    onPress={() => this.goBack()}
                    style={styles.actionButton}
                  >
                    <Text style={styles.cancelText}> SEARCH </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </View>

        <View
          style={[
            styles.step,
            { height: "50%", backgroundColor: "transparent" }
          ]}
        >
          <View
            style={[
              styles.step,
              {
                height: "10%",
                backgroundColor: "white",
                justifyContent: "flex-start",
                flexDirection: "row",
                alignItems: "center"
              }
            ]}
          >
            <Text
              style={[
                styles.cancelText,
                { marginLeft: 3, color: "grey", fontSize: 15 }
              ]}
            >
              {" "}
              BARRELS (Select and Proceed){" "}
            </Text>
          </View>
          <View
            style={[
              styles.step,
              {
                height: "90%",
                backgroundColor: "white",
                justifyContent: "flex-start",
                flexDirection: "column"
              }
            ]}
          >
            <View
              style={[
                styles.step,
                {
                  height: "20%",
                  backgroundColor: "transparent",
                  justifyContent: "flex-start",
                  flexDirection: "row",
                  alignItems: "center",
                  borderColor: "grey",
                  borderBottomWidth: 1
                }
              ]}
            >
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "5%",
                    backgroundColor: "transparent",
                    justifyContent: "flex-start",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              />

              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    {
                      marginLeft: 3,
                      color: "#000",
                      alignSelf: "center",
                      fontWeight: "bold"
                    }
                  ]}
                >
                  {" "}
                  Name/Id{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    {
                      marginLeft: 3,
                      color: "#000",
                      alignSelf: "center",
                      fontWeight: "bold"
                    }
                  ]}
                >
                  {" "}
                  Condition{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    {
                      marginLeft: 3,
                      color: "#000",
                      alignSelf: "center",
                      fontWeight: "bold"
                    }
                  ]}
                >
                  {" "}
                  Capacity{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    {
                      marginLeft: 3,
                      color: "#000",
                      alignSelf: "center",
                      fontWeight: "bold"
                    }
                  ]}
                >
                  {" "}
                  Wood{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15.83%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    {
                      marginLeft: 3,
                      color: "#000",
                      alignSelf: "center",
                      fontWeight: "bold"
                    }
                  ]}
                >
                  {" "}
                  Char{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    {
                      marginLeft: 3,
                      color: "#000",
                      alignSelf: "center",
                      fontWeight: "bold"
                    }
                  ]}
                >
                  {" "}
                  Toasted{" "}
                </Text>
              </View>
            </View>

            <LinearGradient
              colors={["#008963", "#004F63"]}
              style={[
                styles.step,
                {
                  height: "20%",
                  backgroundColor: "transparent",
                  justifyContent: "flex-start",
                  flexDirection: "row",
                  alignItems: "center",
                  borderColor: "grey",
                  borderBottomWidth: 1
                }
              ]}
            >
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "5%",
                    backgroundColor: "transparent",
                    justifyContent: "flex-start",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              />

              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#FFF", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  254545155
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#FFF", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  New{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#FFF", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  30 Gallon{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#FFF", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  Ameriacn Oak{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#FFF", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  #4{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#FFF", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  No{" "}
                </Text>
              </View>
            </LinearGradient>

            <View
              style={[
                styles.step,
                {
                  height: "20%",
                  backgroundColor: "transparent",
                  justifyContent: "flex-start",
                  flexDirection: "row",
                  alignItems: "center",
                  borderColor: "grey",
                  borderBottomWidth: 1
                }
              ]}
            >
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "5%",
                    backgroundColor: "transparent",
                    justifyContent: "flex-start",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              />

              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#000", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  254545155
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#000", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  New{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#000", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  30 Gallon{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#000", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  Ameriacn Oak{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#000", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  #4{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#000", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  No{" "}
                </Text>
              </View>
            </View>

            <View
              style={[
                styles.step,
                {
                  height: "20%",
                  backgroundColor: "transparent",
                  justifyContent: "flex-start",
                  flexDirection: "row",
                  alignItems: "center",
                  borderColor: "grey",
                  borderBottomWidth: 1
                }
              ]}
            >
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "5%",
                    backgroundColor: "transparent",
                    justifyContent: "flex-start",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              />

              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#000", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  254545155
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#000", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  New{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#000", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  30 Gallon{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#000", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  Ameriacn Oak{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#000", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  #4{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#000", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  No{" "}
                </Text>
              </View>
            </View>

            <View
              style={[
                styles.step,
                {
                  height: "20%",
                  backgroundColor: "transparent",
                  justifyContent: "flex-start",
                  flexDirection: "row",
                  alignItems: "center",
                  borderColor: "grey",
                  borderBottomWidth: 1
                }
              ]}
            >
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "5%",
                    backgroundColor: "transparent",
                    justifyContent: "flex-start",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              />

              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#000", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  254545155
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#000", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  New{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#000", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  30 Gallon{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#000", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  Ameriacn Oak{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#000", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  #4{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#000", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  No{" "}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }

  getStep2View() {
    let data = [
      { value: "Specific" },
      { value: "Option 1" },
      { value: "Option 2" }
    ];
    return (
      <View style={styles.step}>
        <View
          style={[
            styles.step,
            { height: "15%", backgroundColor: "transparent" }
          ]}
        >
          <View
            style={[
              styles.step,
              {
                height: "100%",
                backgroundColor: "transparent",
                justifyContent: "flex-start",
                flexDirection: "column"
              }
            ]}
          >
            <View
              style={[
                styles.step,
                {
                  height: "50%",
                  backgroundColor: "transparent",
                  justifyContent: "flex-start",
                  flexDirection: "row",
                  alignItems: "center",
                  borderColor: "grey",
                  borderBottomWidth: 1
                }
              ]}
            >
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "5%",
                    backgroundColor: "transparent",
                    justifyContent: "flex-start",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              />

              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    {
                      marginLeft: 3,
                      color: "#000",
                      alignSelf: "center",
                      fontWeight: "bold"
                    }
                  ]}
                >
                  {" "}
                  Name/Id{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    {
                      marginLeft: 3,
                      color: "#000",
                      alignSelf: "center",
                      fontWeight: "bold"
                    }
                  ]}
                >
                  {" "}
                  Condition{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    {
                      marginLeft: 3,
                      color: "#000",
                      alignSelf: "center",
                      fontWeight: "bold"
                    }
                  ]}
                >
                  {" "}
                  Capacity{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    {
                      marginLeft: 3,
                      color: "#000",
                      alignSelf: "center",
                      fontWeight: "bold"
                    }
                  ]}
                >
                  {" "}
                  Wood{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15.83%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    {
                      marginLeft: 3,
                      color: "#000",
                      alignSelf: "center",
                      fontWeight: "bold"
                    }
                  ]}
                >
                  {" "}
                  Char{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    {
                      marginLeft: 3,
                      color: "#000",
                      alignSelf: "center",
                      fontWeight: "bold"
                    }
                  ]}
                >
                  {" "}
                  Toasted{" "}
                </Text>
              </View>
            </View>

            <LinearGradient
              colors={["#008963", "#004F63"]}
              style={[
                styles.step,
                {
                  height: "50%",
                  backgroundColor: "transparent",
                  justifyContent: "flex-start",
                  flexDirection: "row",
                  alignItems: "center",
                  borderColor: "grey",
                  borderBottomWidth: 1
                }
              ]}
            >
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "5%",
                    backgroundColor: "transparent",
                    justifyContent: "flex-start",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              />

              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#FFF", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  254545155
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#FFF", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  New{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#FFF", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  30 Gallon{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#FFF", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  Ameriacn Oak{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#FFF", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  #4{" "}
                </Text>
              </View>
              <View
                style={[
                  styles.step,
                  {
                    height: "100%",
                    width: "15%",
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.cancelText,
                    { marginLeft: 3, color: "#FFF", alignSelf: "center" }
                  ]}
                >
                  {" "}
                  No{" "}
                </Text>
              </View>
            </LinearGradient>
          </View>
        </View>

        <View
          style={[
            styles.step,
            {
              height: "85%",
              width: "100%",
              backgroundColor: "transparent",
              flexDirection: "row",
              paddingTop: "2%"
            }
          ]}
        >
          <View
            style={[
              styles.step,
              {
                height: "100%",
                width: "33.33%",
                backgroundColor: "transparent",
                flexDirection: "column",
                justifyContent: "flex-start",
                alignItems: "center"
              }
            ]}
          >
            <View
              style={{
                width: "90%",
                height: "15%",
                flexDirection: "row",
                justifyContent: "space-between",
                borderColor: "grey",
                borderBottomWidth: 1
              }}
            >
              <Text style={styles.section212Text}>DATE</Text>

              <TextInputMask
                ref={"myDateText"}
                type={"datetime"}
                underlineColorAndroid="rgba(0,0,0,0)"
                placeholder="mm/dd/yyyy"
                style={{
                  width: "60%",
                  height: "100%",
                  backgroundColor: "transparent",
                  textAlign: "right"
                }}
                options={{ format: "DD/MM/YYYY" }}
              />
            </View>

            <View
              style={{
                width: "90%",
                height: "15%",
                flexDirection: "row",
                justifyContent: "space-between",
                borderColor: "grey",
                borderBottomWidth: 1
              }}
            >
              <Text style={styles.section212Text}>IN HOUSE BARREL ID</Text>

              <TextInput
                style={{
                  width: "50%",
                  marginLeft: width * 0.001,
                  fontSize: width * 0.011,
                  textAlign: "right"
                }}
                onChangeText={text => this.setState({ search: text })}
                placeholder="ID"
                underlineColorAndroid="rgba(0,0,0,0)"
              />
            </View>
          </View>
          <View
            style={[
              styles.step,
              {
                height: "100%",
                width: "33.33%",
                backgroundColor: "transparent",
                flexDirection: "column",
                justifyContent: "flex-start",
                alignItems: "center"
              }
            ]}
          >
            <View
              style={{
                width: "90%",
                height: "15%",
                flexDirection: "row",
                justifyContent: "space-between",
                borderColor: "grey",
                borderBottomWidth: 1
              }}
            >
              <Text style={styles.section212Text}>TIME</Text>

              <TextInputMask
                ref={"myDateText"}
                type={"datetime"}
                underlineColorAndroid="rgba(0,0,0,0)"
                placeholder="08:55"
                style={styles.section2123Input}
                options={{ format: "hh:mm" }}
              />
              <View style={styles.dropStyle} />
              <Dropdown
                value="AM"
                data={data}
                inputContainerStyle={{ borderBottomColor: "transparent" }}
                pickerStyle={{ marginTop: 90 }}
                containerStyle={{
                  width: "20%",
                  alignSelf: "center",
                  marginTop: -20,
                  fontSize: 15
                }}
              />
            </View>

            <View
              style={{
                width: "90%",
                height: "15%",
                flexDirection: "row",
                justifyContent: "space-between",
                borderColor: "grey",
                borderBottomWidth: 1
              }}
            >
              <Text style={styles.section212Text}>TOTAL FILL</Text>

              <TextInput
                style={{
                  width: "30%",
                  marginLeft: width * 0.001,
                  fontSize: width * 0.011,
                  textAlign: "right"
                }}
                onChangeText={text => this.setState({ search: text })}
                placeholder="Reading"
                underlineColorAndroid="rgba(0,0,0,0)"
              />
              <View style={styles.dropStyle} />
              <Dropdown
                value="GALLON"
                data={data}
                inputContainerStyle={{ borderBottomColor: "transparent" }}
                pickerStyle={{ marginTop: 90 }}
                containerStyle={{
                  width: "30%",
                  alignSelf: "center",
                  marginTop: -20,
                  fontSize: 15
                }}
              />
            </View>
          </View>

          <View
            style={[
              styles.step,
              {
                height: "100%",
                width: "33.33%",
                backgroundColor: "transparent",
                flexDirection: "column",
                justifyContent: "flex-start",
                alignItems: "center"
              }
            ]}
          >
            <View
              style={{
                width: "90%",
                height: "15%",
                flexDirection: "row",
                justifyContent: "space-between",
                borderColor: "grey",
                borderBottomWidth: 1
              }}
            >
              <Text style={styles.section212Text}>TOTAL FILL LOSS</Text>

              <TextInput
                style={{
                  width: "30%",
                  marginLeft: width * 0.001,
                  fontSize: width * 0.011,
                  textAlign: "right"
                }}
                onChangeText={text => this.setState({ search: text })}
                placeholder="Reading"
                underlineColorAndroid="rgba(0,0,0,0)"
              />
              <View style={styles.dropStyle} />
              <Dropdown
                value="GALLON"
                data={data}
                inputContainerStyle={{ borderBottomColor: "transparent" }}
                pickerStyle={{ marginTop: 90 }}
                containerStyle={{
                  width: "30%",
                  alignSelf: "center",
                  marginTop: -20,
                  fontSize: 15
                }}
              />
            </View>

            <View
              style={{
                width: "90%",
                height: "15%",
                flexDirection: "row",
                justifyContent: "space-between",
                borderColor: "grey",
                borderBottomWidth: 1
              }}
            >
              <Text style={styles.section212Text}>BARREL LOCATION</Text>

              <TextInput
                style={{
                  width: "40%",
                  marginLeft: width * 0.001,
                  fontSize: width * 0.011,
                  textAlign: "right"
                }}
                onChangeText={text => this.setState({ search: text })}
                placeholder="Reading"
                underlineColorAndroid="rgba(0,0,0,0)"
              />
            </View>
          </View>
        </View>
      </View>
    );
  }

  getStep3View() {
    let data = [
      { value: "Specific" },
      { value: "Option 1" },
      { value: "Option 2" }
    ];
    return (
      <View style={styles.step}>
        <TouchableOpacity
          onPress={() => this.setState({ isShow: !this.state.isShow })}
          style={{
            width: "100%",
            height: "10%",
            backgroundColor: "transparent"
          }}
        >
          <View
            style={{
              width: "100%",
              height: "100%",
              backgroundColor: "transparent",
              flexDirection: "row",
              justifyContent: "space-between"
            }}
          >
            <View
              style={{
                width: "33.33%",
                height: "100%",
                backgroundColor: "transparent",
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text
                style={[
                  styles.smallText,
                  {
                    color: "#5ABCBB",
                    fontSize: 16,
                    fontWeight: "bold",
                    marginRight: 5
                  }
                ]}
              >
                LOT DETAILS
              </Text>
            </View>
            <View
              style={{
                width: "33.33%",
                height: "100%",
                backgroundColor: "transparent",
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text
                style={[
                  styles.smallText,
                  {
                    color: "#5ABCBB",
                    fontSize: 16,
                    fontWeight: "bold",
                    marginRight: 5
                  }
                ]}
              >
                SPIRIT DETAILS
              </Text>
            </View>

            <View
              style={{
                width: "33.33%",
                height: "100%",
                backgroundColor: "transparent",
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center"
              }}
            >
              <Text
                style={[
                  styles.smallText,
                  {
                    color: "#5ABCBB",
                    fontSize: 16,
                    fontWeight: "bold",
                    marginRight: 5
                  }
                ]}
              >
                SPIRIT DETAILS
              </Text>
              <Icon1
                name={!this.state.isShow ? "chevron-up" : "chevron-down"}
                size={width * 0.017}
                color="grey"
                style={{ alignSelf: "center", marginTop: 3, marginRight: 10 }}
              />
            </View>
          </View>
        </TouchableOpacity>
        {this.state.isShow && (
          <View
            style={{
              width: "100%",
              height: "40%",
              backgroundColor: "transparent",
              flexDirection: "row"
            }}
          >
            <View
              style={{
                width: "33.33%",
                height: "100%",
                backgroundColor: "transparent",
                flexDirection: "column",
                justifyContent: "flex-start",
                alignItems: "center"
              }}
            >
              <View
                style={{
                  width: "95%",
                  height: "20%",
                  backgroundColor: "transparent",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                  borderColor: "grey",
                  borderBottomWidth: 0.5
                }}
              >
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "grey",
                      fontSize: 16,
                      fontWeight: "normal",
                      marginRight: 5
                    }
                  ]}
                >
                  CLASS/TYPE
                </Text>
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "grey",
                      fontSize: 16,
                      fontWeight: "bold",
                      marginRight: 5
                    }
                  ]}
                >
                  Taquilla
                </Text>
              </View>

              <View
                style={{
                  width: "95%",
                  height: "20%",
                  backgroundColor: "transparent",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                  borderColor: "grey",
                  borderBottomWidth: 0.5
                }}
              >
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "grey",
                      fontSize: 16,
                      fontWeight: "normal",
                      marginRight: 5
                    }
                  ]}
                >
                  TOTAL VOLUME
                </Text>
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "grey",
                      fontSize: 16,
                      fontWeight: "bold",
                      marginRight: 5
                    }
                  ]}
                >
                  10 Gallons
                </Text>
              </View>

              <View
                style={{
                  width: "95%",
                  height: "20%",
                  backgroundColor: "transparent",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                  borderColor: "grey",
                  borderBottomWidth: 0.5
                }}
              >
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "grey",
                      fontSize: 16,
                      fontWeight: "normal",
                      marginRight: 5
                    }
                  ]}
                >
                  TOTAL PROOF GALLON
                </Text>
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "grey",
                      fontSize: 16,
                      fontWeight: "bold",
                      marginRight: 5
                    }
                  ]}
                >
                  0.43
                </Text>
              </View>
            </View>
            <View
              style={{
                width: "33.33%",
                height: "100%",
                backgroundColor: "transparent",
                flexDirection: "column",
                justifyContent: "flex-start",
                alignItems: "center"
              }}
            >
              <View
                style={{
                  width: "95%",
                  height: "20%",
                  backgroundColor: "transparent",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                  borderColor: "grey",
                  borderBottomWidth: 0.5
                }}
              >
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "grey",
                      fontSize: 16,
                      fontWeight: "normal",
                      marginRight: 5
                    }
                  ]}
                >
                  LOT NUMBER
                </Text>
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "grey",
                      fontSize: 16,
                      fontWeight: "bold",
                      marginRight: 5
                    }
                  ]}
                >
                  17H10B
                </Text>
              </View>

              <View
                style={{
                  width: "95%",
                  height: "20%",
                  backgroundColor: "transparent",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                  borderColor: "grey",
                  borderBottomWidth: 0.5
                }}
              >
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "grey",
                      fontSize: 16,
                      fontWeight: "normal",
                      marginRight: 5
                    }
                  ]}
                >
                  BARREL COUNT
                </Text>
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "grey",
                      fontSize: 16,
                      fontWeight: "bold",
                      marginRight: 5
                    }
                  ]}
                >
                  In Progress
                </Text>
              </View>

              <View
                style={{
                  width: "95%",
                  height: "20%",
                  backgroundColor: "transparent",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                  borderColor: "grey",
                  borderBottomWidth: 0.5
                }}
              >
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "grey",
                      fontSize: 16,
                      fontWeight: "normal",
                      marginRight: 5
                    }
                  ]}
                >
                  DATE & TIME
                </Text>
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "grey",
                      fontSize: 16,
                      fontWeight: "bold",
                      marginRight: 5
                    }
                  ]}
                >
                  05/11/2017
                </Text>
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "grey",
                      fontSize: 16,
                      fontWeight: "bold",
                      marginRight: 5
                    }
                  ]}
                >
                  07:30 AM
                </Text>
              </View>

              <View
                style={{
                  width: "95%",
                  height: "20%",
                  backgroundColor: "transparent",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                  borderColor: "grey",
                  borderBottomWidth: 0.5
                }}
              >
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "grey",
                      fontSize: 16,
                      fontWeight: "normal",
                      marginRight: 5
                    }
                  ]}
                >
                  HEAD CARDS
                </Text>
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "#3A8FCC",
                      fontSize: 16,
                      fontWeight: "bold",
                      marginRight: 5
                    }
                  ]}
                >
                  Download PDF
                </Text>
              </View>
            </View>
            <View
              style={{
                width: "33.33%",
                height: "100%",
                backgroundColor: "transparent",
                flexDirection: "row",
                justifyContent: "flex-start",
                alignItems: "flex-start"
              }}
            >
              <View
                style={{
                  width: "95%",
                  height: "20%",
                  backgroundColor: "transparent",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                  borderColor: "grey",
                  borderBottomWidth: 0.5
                }}
              >
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "grey",
                      fontSize: 16,
                      fontWeight: "normal",
                      marginRight: 5
                    }
                  ]}
                >
                  LOT NUMBER
                </Text>
                <Text
                  style={[
                    styles.smallText,
                    {
                      color: "grey",
                      fontSize: 16,
                      fontWeight: "bold",
                      marginRight: 5
                    }
                  ]}
                >
                  17H10B
                </Text>
              </View>
            </View>
          </View>
        )}
        <View
          style={{
            width: "100%",
            height: this.state.isShow ? "45%" : "80%",
            backgroundColor: "transparent",
            flexDirection: "row"
          }}
        >
          <LinearGradient
            colors={["#008963", "#004F63"]}
            style={{
              width: "10%",
              height: "100%",
              backgroundColor: "transparent",
              flexDirection: "column",
              justifyContent: "flex-start",
              alignItems: "center"
            }}
          >
            <View
              style={{
                width: "100%",
                height: 40,
                backgroundColor: "transparent",
                flexDirection: "row",
                justifyContent: "flex-start",
                alignItems: "center",
                borderColor: "white",
                borderBottomWidth: 0.5
              }}
            >
              <Text
                style={[
                  styles.smallText,
                  {
                    color: "white",
                    fontSize: 12,
                    fontWeight: "bold",
                    marginRight: 5
                  }
                ]}
              >
                Barrels
              </Text>
            </View>

            <View
              style={{
                width: "100%",
                height: 40,
                backgroundColor: "transparent",
                flexDirection: "row",
                justifyContent: "flex-start",
                alignItems: "center",
                borderColor: "white",
                borderBottomWidth: 0.5
              }}
            >
              <Text
                style={[
                  styles.smallText,
                  {
                    color: "white",
                    fontSize: 12,
                    fontWeight: "bold",
                    marginRight: 5
                  }
                ]}
              >
                Serial #
              </Text>
            </View>

            <View
              style={{
                width: "100%",
                height: 40,
                backgroundColor: "transparent",
                flexDirection: "row",
                justifyContent: "flex-start",
                alignItems: "center",
                borderColor: "white",
                borderBottomWidth: 0.5
              }}
            >
              <Text
                style={[
                  styles.smallText,
                  {
                    color: "white",
                    fontSize: 12,
                    fontWeight: "bold",
                    marginRight: 5
                  }
                ]}
              >
                Name
              </Text>
            </View>

            <View
              style={{
                width: "100%",
                height: 40,
                backgroundColor: "transparent",
                flexDirection: "row",
                justifyContent: "flex-start",
                alignItems: "center",
                borderColor: "white",
                borderBottomWidth: 0.3
              }}
            >
              <Text
                style={[
                  styles.smallText,
                  {
                    color: "white",
                    fontSize: 12,
                    fontWeight: "bold",
                    marginRight: 5
                  }
                ]}
              >
                Filled Gallons
              </Text>
            </View>
            <View
              style={{
                width: "100%",
                height: 40,
                backgroundColor: "transparent",
                flexDirection: "row",
                justifyContent: "flex-start",
                alignItems: "center",
                borderColor: "white",
                borderBottomWidth: 0.3
              }}
            >
              <Text
                style={[
                  styles.smallText,
                  {
                    color: "white",
                    fontSize: 12,
                    fontWeight: "bold",
                    marginRight: 5
                  }
                ]}
              >
                Filled Pounds
              </Text>
            </View>
          </LinearGradient>
          <LinearGradient
            colors={["#008963", "#004F63"]}
            style={{
              width: "90%",
              height: "100%",
              backgroundColor: "green",
              flexDirection: "column",
              justifyContent: "flex-start",
              alignItems: "center"
            }}
          >
            <View
              style={{
                width: "100%",
                height: 40,
                backgroundColor: "transparent",
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <View
                style={{
                  width: "100%",
                  height: "100%",
                  backgroundColor: "transparent",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center"
                }}
              >
                <View
                  style={{
                    width: "16.66%",
                    height: "100%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                    borderColor: "white",
                    borderBottomWidth: 0.5
                  }}
                >
                  <Text
                    style={[
                      styles.smallText,
                      {
                        color: "white",
                        fontSize: 12,
                        fontWeight: "bold",
                        marginRight: 5
                      }
                    ]}
                  >
                    Barrel 1
                  </Text>
                </View>

                <View
                  style={{
                    width: "16.66%",
                    height: "100%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                    borderColor: "white",
                    borderBottomWidth: 0.5
                  }}
                >
                  <Text
                    style={[
                      styles.smallText,
                      {
                        color: "white",
                        fontSize: 12,
                        fontWeight: "bold",
                        marginRight: 5
                      }
                    ]}
                  >
                    Barrel 2
                  </Text>
                </View>
                <View
                  style={{
                    width: "16.66%",
                    height: "100%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                    borderColor: "white",
                    borderBottomWidth: 0.5
                  }}
                >
                  <Text
                    style={[
                      styles.smallText,
                      {
                        color: "white",
                        fontSize: 12,
                        fontWeight: "bold",
                        marginRight: 5
                      }
                    ]}
                  >
                    Barrel 3
                  </Text>
                </View>
                <View
                  style={{
                    width: "16.66%",
                    height: "100%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                    borderColor: "white",
                    borderBottomWidth: 0.5
                  }}
                >
                  <Text
                    style={[
                      styles.smallText,
                      {
                        color: "white",
                        fontSize: 12,
                        fontWeight: "bold",
                        marginRight: 5
                      }
                    ]}
                  >
                    Barrel 4
                  </Text>
                </View>
                <View
                  style={{
                    width: "16.66%",
                    height: "100%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                    borderColor: "white",
                    borderBottomWidth: 0.5
                  }}
                >
                  <Text
                    style={[
                      styles.smallText,
                      {
                        color: "white",
                        fontSize: 12,
                        fontWeight: "bold",
                        marginRight: 5
                      }
                    ]}
                  >
                    Barrel 5
                  </Text>
                </View>

                <View
                  style={{
                    width: "16.66%",
                    height: "100%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    justifyContent: "flex-start",
                    alignItems: "center",
                    borderColor: "white",
                    borderBottomWidth: 0.5
                  }}
                >
                  <Text
                    style={[
                      styles.smallText,
                      {
                        color: "white",
                        fontSize: 12,
                        fontWeight: "bold",
                        marginRight: 5
                      }
                    ]}
                  >
                    Barrel 6
                  </Text>
                </View>
              </View>
            </View>

            <View
              style={{
                width: "100%",
                height: "90%",
                backgroundColor: "white",
                flexDirection: "column",
                justifyContent: "flex-start",
                alignItems: "center"
              }}
            >
              <View
                style={{
                  width: "100%",
                  height: 40,
                  backgroundColor: "white",
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <View
                  style={{
                    width: "100%",
                    height: "100%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center"
                  }}
                >
                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      17hb01 1
                    </Text>
                  </View>

                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      17hb01 2
                    </Text>
                  </View>
                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      17hb01 3
                    </Text>
                  </View>
                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      17hb01 4
                    </Text>
                  </View>
                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      17hb01 5
                    </Text>
                  </View>

                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "flex-start",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      17hb01 6
                    </Text>
                  </View>
                </View>
              </View>
              <View
                style={{
                  width: "100%",
                  height: 40,
                  backgroundColor: "white",
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <View
                  style={{
                    width: "100%",
                    height: "100%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center"
                  }}
                >
                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      BRL 170803
                    </Text>
                  </View>

                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      BRL 170803 2
                    </Text>
                  </View>
                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      BRL 170803 3
                    </Text>
                  </View>
                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      BRL 170803 4
                    </Text>
                  </View>
                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      BRL 170803 5
                    </Text>
                  </View>

                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "flex-start",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      BRL 170803 6
                    </Text>
                  </View>
                </View>
              </View>

              <View
                style={{
                  width: "100%",
                  height: 40,
                  backgroundColor: "white",
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <View
                  style={{
                    width: "100%",
                    height: "100%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center"
                  }}
                >
                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      10
                    </Text>
                  </View>

                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      10
                    </Text>
                  </View>
                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      10
                    </Text>
                  </View>
                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      10
                    </Text>
                  </View>
                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      10
                    </Text>
                  </View>

                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "flex-start",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      10
                    </Text>
                  </View>
                </View>
              </View>

              <View
                style={{
                  width: "100%",
                  height: 40,
                  backgroundColor: "white",
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <View
                  style={{
                    width: "100%",
                    height: "100%",
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center"
                  }}
                >
                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      8.016
                    </Text>
                  </View>

                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      8.016
                    </Text>
                  </View>
                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      8.016
                    </Text>
                  </View>
                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      8.016
                    </Text>
                  </View>
                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      8.016
                    </Text>
                  </View>

                  <View
                    style={{
                      width: "16.66%",
                      height: "100%",
                      backgroundColor: "transparent",
                      flexDirection: "row",
                      justifyContent: "flex-start",
                      alignItems: "center",
                      borderColor: "white",
                      borderBottomWidth: 0.5
                    }}
                  >
                    <Text
                      style={[
                        styles.smallText,
                        {
                          color: "#A0A0A0",
                          fontSize: 12,
                          fontWeight: "bold",
                          marginRight: 5
                        }
                      ]}
                    >
                      8.016
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </LinearGradient>
        </View>

        <View
          style={{
            width: "100%",
            height: this.state.isShow ? 30 : 50,
            backgroundColor: "transparent",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            paddingRight: "6%"
          }}
        >
          <Text
            style={[
              styles.smallText,
              {
                color: "#A0A0A0",
                fontSize: 12,
                fontWeight: "bold",
                marginRight: 5
              }
            ]}
          >
            ACTION
          </Text>
          <Text
            style={[
              styles.smallText,
              {
                color: "#60D4CC",
                fontSize: 12,
                fontWeight: "bold",
                marginRight: 5
              }
            ]}
          >
            VIEW DETAIL
          </Text>
          <Text
            style={[
              styles.smallText,
              {
                color: "#60D4CC",
                fontSize: 12,
                fontWeight: "bold",
                marginRight: 5
              }
            ]}
          >
            VIEW DETAIL
          </Text>
          <Text
            style={[
              styles.smallText,
              {
                color: "#60D4CC",
                fontSize: 12,
                fontWeight: "bold",
                marginRight: 5
              }
            ]}
          >
            VIEW DETAIL
          </Text>
          <Text
            style={[
              styles.smallText,
              {
                color: "#60D4CC",
                fontSize: 12,
                fontWeight: "bold",
                marginRight: 5
              }
            ]}
          >
            VIEW DETAIL
          </Text>
          <Text
            style={[
              styles.smallText,
              {
                color: "#60D4CC",
                fontSize: 12,
                fontWeight: "bold",
                marginRight: 5
              }
            ]}
          >
            VIEW DETAIL
          </Text>
          <Text
            style={[
              styles.smallText,
              {
                color: "#60D4CC",
                fontSize: 12,
                fontWeight: "bold",
                marginRight: 5
              }
            ]}
          >
            VIEW DETAIL
          </Text>
        </View>
      </View>
    );
  }

  render() {
    let data = [
      { value: "Specific" },
      { value: "Option 1" },
      { value: "Option 2" }
    ];
    return (
      <KeyboardAwareScrollView
        scrollEnabled={false}
        contentContainerStyle={styles.container}
      >
        <LinearGradient colors={["#004F63", "#008963"]} style={styles.header}>
          <Text style={styles.headerText}> Fill Barrel </Text>
          <TouchableOpacity onPress={() => this.props.sendData(undefined)}>
            <Icon
              name="close"
              size={width * 0.025}
              color="#fff"
              style={{ alignSelf: "center", marginTop: 5 }}
            />
          </TouchableOpacity>
        </LinearGradient>

        <View style={styles.section2}>
          <View style={styles.section21}>
            {this.state.page === 0 && (
              <View style={styles.step1View}>
                <LinearGradient
                  colors={["#008963", "#004F63"]}
                  style={styles.circleViewActive}
                >
                  <Text style={[styles.smallText, { color: "#FFF" }]}>1</Text>
                </LinearGradient>
                <Text
                  style={[
                    styles.smallText,
                    { color: "#000", fontSize: 10, fontWeight: "normal" }
                  ]}
                >
                  SELECT BARREL{" "}
                </Text>
              </View>
            )}

            {this.state.page != 0 && (
              <View style={styles.step1View}>
                <View style={styles.circleViewInActive}>
                  <Text style={styles.smallText}>1</Text>
                </View>
                <Text
                  style={[
                    styles.smallText,
                    { color: "grey", fontSize: 10, fontWeight: "normal" }
                  ]}
                >
                  SELECT BARREL
                </Text>
              </View>
            )}

            <View style={styles.verticalLine} />

            {this.state.page === 1 && (
              <View style={styles.step1View}>
                <LinearGradient
                  colors={["#008963", "#004F63"]}
                  style={styles.circleViewActive}
                >
                  <Text style={[styles.smallText, { color: "#FFF" }]}>2</Text>
                </LinearGradient>
                <Text
                  style={[
                    styles.smallText,
                    { color: "#000", fontSize: 10, fontWeight: "normal" }
                  ]}
                >
                  FILL BARREL DETAILS{" "}
                </Text>
              </View>
            )}

            {this.state.page != 1 && (
              <View style={styles.step1View}>
                <View style={styles.circleViewInActive}>
                  <Text style={styles.smallText}>2</Text>
                </View>
                <Text
                  style={[
                    styles.smallText,
                    { color: "grey", fontSize: 10, fontWeight: "normal" }
                  ]}
                >
                  FILL BARREL DETAILS
                </Text>
              </View>
            )}

            <View style={styles.verticalLine} />

            {this.state.page === 2 && (
              <View style={styles.step1View}>
                <LinearGradient
                  colors={["#008963", "#004F63"]}
                  style={styles.circleViewActive}
                >
                  <Text style={[styles.smallText, { color: "#FFF" }]}>3</Text>
                </LinearGradient>
                <Text
                  style={[
                    styles.smallText,
                    { color: "#000", fontSize: 10, fontWeight: "normal" }
                  ]}
                >
                  FILL BARREL DETAILS{" "}
                </Text>
              </View>
            )}

            {this.state.page != 2 && (
              <View style={styles.step1View}>
                <View style={styles.circleViewInActive}>
                  <Text style={styles.smallText}>3</Text>
                </View>
                <Text
                  style={[
                    styles.smallText,
                    { color: "grey", fontSize: 10, fontWeight: "normal" }
                  ]}
                >
                  FILL BARREL DETAILS
                </Text>
              </View>
            )}
          </View>

          {this.state.page === 0
            ? this.getStep1View()
            : this.state.page === 1 ? this.getStep2View() : this.getStep3View()}
        </View>

        <View style={styles.section23}>
          {this.state.page > 0 && (
            <View style={styles.btnBackground1}>
              <TouchableOpacity
                onPress={() => this.goBack()}
                style={styles.actionButton}
              >
                <Text style={styles.cancelText}>
                  {" "}
                  {this.state.page > 0 ? "BACK" : "CANCEL"}{" "}
                </Text>
              </TouchableOpacity>
            </View>
          )}

          <LinearGradient
            colors={["#008963", "#004F63"]}
            style={styles.btnBackground}
          >
            <TouchableOpacity
              onPress={() => this.goNext()}
              style={styles.actionButton}
            >
              <Text style={styles.transferText}>
                {this.state.page === 2 ? "START BATCH" : "NEXT"}
              </Text>
            </TouchableOpacity>
          </LinearGradient>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#fff"
  },
  section23: {
    width: "100%",
    height: "13%",
    backgroundColor: "#DBDBDB",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  btnBackground: {
    marginRight: 10,
    marginLeft: 10,
    marginTop: 0,
    marginBottom: 5,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: "#008963",
    borderRadius: width * 0.03,
    borderWidth: 1,
    borderColor: "#fff",
    width: width * 0.155,
    height: width * 0.04,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  circleViewActive: {
    marginRight: 1,
    marginLeft: 1,
    marginTop: 0,
    marginBottom: 5,
    paddingTop: 1,
    paddingBottom: 1,
    backgroundColor: "#008963",
    borderRadius: 20,
    borderWidth: 1,
    borderColor: "transparent",
    width: 40,
    height: 40,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center"
  },
  circleViewInActive: {
    marginRight: 1,
    marginLeft: 1,
    marginTop: 0,
    marginBottom: 0,
    paddingTop: 1,
    paddingBottom: 1,
    backgroundColor: "white",
    borderRadius: 20,
    borderWidth: 2,
    borderColor: "grey",
    width: 40,
    height: 40,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center"
  },

  btnBackground1: {
    marginRight: 10,
    marginLeft: 10,
    marginTop: 0,
    marginBottom: 5,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: "#fff",
    borderRadius: width * 0.03,
    borderWidth: 2,
    borderColor: "#008963",
    width: width * 0.155,
    height: width * 0.04,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  actionButton: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "center",
    backgroundColor: "transparent"
  },
  cancelText: {
    fontSize: 15,
    color: "black",
    alignSelf: "center",
    fontWeight: "normal",
    paddingLeft: "0.1%"
  },
  transferText: {
    fontSize: 15,
    color: "white",
    alignSelf: "center",
    fontWeight: "normal",
    paddingLeft: "0.1%"
  },

  mainContainer: {
    width: "92%",
    height: "100%",
    backgroundColor: "green",
    flexDirection: "column"
  },
  header: {
    width: "100%",
    height: "8%",
    flexDirection: "row",
    padding: "1%",
    justifyContent: "space-between"
  },
  headerText: {
    fontSize: 18,
    fontWeight: "normal",
    color: "#ABD5CF",
    alignSelf: "center"
  },
  section2: {
    width: "100%",
    height: "80%",
    backgroundColor: "transparent",
    flexDirection: "column",
    justifyContent: "center"
  },
  section21: {
    width: "100%",
    height: "100%",
    backgroundColor: "white",
    flexDirection: "column",
    justifyContent: "flex-start",
    paddingTop: "2%",
    paddingBottom: "2%"
  },
  verticalSpace: {
    width: ".2%",
    height: "70%",
    backgroundColor: "#999999",
    alignSelf: "center"
  },
  section22: {
    width: "29.5%",
    height: "100%",
    backgroundColor: "yellow"
  },
  rowView: {
    width: "94%",
    height: "11%",
    marginLeft: "1%",
    marginRight: "1%",
    backgroundColor: "white",
    borderColor: "grey",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    alignSelf: "center",
    borderBottomWidth: 1
  },
  section3: {
    width: "29.5%",
    height: "100%",
    backgroundColor: "transparent",
    flexDirection: "column",
    justifyContent: "center"
  },
  section31: {
    width: "100%",
    height: "14%",
    backgroundColor: "white",
    flexDirection: "column",
    justifyContent: "center"
  },
  actionView: {
    width: "100%",
    height: "25%",
    backgroundColor: "transparent",
    flexDirection: "row",
    justifyContent: "center",
    marginLeft: "2%"
  },
  actionView1: {
    width: "20%",
    height: "100%",
    backgroundColor: "transparent",
    flexDirection: "column",
    justifyContent: "center",
    marginLeft: "2%",
    marginTop: "1%"
  },
  space: {
    width: "4%",
    height: "10%",
    backgroundColor: "black",
    alignSelf: "center",
    marginTop: "-6%"
  },

  smallText: {
    fontSize: 15,
    fontWeight: "bold",
    paddingLeft: "5%",
    color: "grey"
  },
  bigText: {
    fontSize: 15,
    fontWeight: "normal",
    paddingLeft: "5%"
  },
  subText: {
    fontSize: 12,
    fontWeight: "normal",
    paddingLeft: "6%",
    marginTop: "-5%"
  },

  section4: {
    width: "80%",
    height: "100%",
    backgroundColor: "white",
    flexDirection: "column",
    justifyContent: "center",
    marginLeft: "2%"
  },
  subView: {
    width: "45%",
    height: "100%",
    backgroundColor: "transparent",
    borderColor: "grey",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderBottomWidth: 1
  },
  section212Text: {
    fontSize: 15,
    marginLeft: 3,
    fontWeight: "bold",
    alignSelf: "center",
    color: "#767676"
  },
  dropStyle: {
    width: "1%",
    height: "100%",
    backgroundColor: "#DBDBDB"
  },
  section2123Input: {
    width: "55%",
    height: "100%",
    alignSelf: "center",
    justifyContent: "flex-end",
    flexDirection: "row",
    backgroundColor: "transparent",
    marginRight: "0%",
    textAlign: "right"
  },
  section21: {
    width: "100%",
    height: "12%",
    backgroundColor: "white",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: "2%",
    paddingRight: "2%"
  },
  step1View: {
    width: "10%",
    height: "100%",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  verticalLine: {
    width: "35%",
    height: "5%",
    backgroundColor: "#E3E3E3",
    alignSelf: "center"
  },

  step: {
    width: "100%",
    height: "88%",
    backgroundColor: "transparent",
    flexDirection: "column",
    justifyContent: "flex-start"
  }
});
