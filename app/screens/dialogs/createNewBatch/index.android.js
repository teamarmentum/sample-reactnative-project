import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  StatusBar,
  TouchableOpacity,
  Dimensions,
  TextInput,
  View
} from "react-native";

import { Dropdown } from "react-native-material-dropdown";
import { TextInputMask } from "react-native-masked-text";
import ModalDropdown from "react-native-modal-dropdown";

var { height, width } = Dimensions.get("window");
import ActionButton from "react-native-action-button";
import Icon from "react-native-vector-icons/dist/EvilIcons";
import { Switch } from "react-native-switch";
import LinearGradient from "react-native-linear-gradient";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { KeyboardAwareView } from "react-native-keyboard-aware-view";

export default class CreateNewBatch extends Component {
  updateDialogValue(value) {
    console.log("do stuff " + value);
  }

  constructor(props, context) {
    super(props, context);
    this.state = { next: false };
  }

  // componentDidMount(){
  //   console.log('current input');
  //   console.log(this.props);
  // }

  componentWillUnmount() {
    // console.log('will unmount called');
    this.state = { next: false };
  }

  componentWillReceiveProps() {
    console.log("will Recive Props called");
  }

  getStep1View() {
    let data = [
      { value: "Specific" },
      { value: "Option 1" },
      { value: "Option 2" }
    ];
    return (
      <View style={styles.section21}>
        <View style={styles.rowView}>
          <Text style={styles.section212Text}>CHOOSE A RECEIPE</Text>

          <Dropdown
            value="Wicked Wishkey"
            data={data}
            inputContainerStyle={{ borderBottomColor: "transparent" }}
            pickerStyle={{ marginTop: 90 }}
            containerStyle={{
              width: "30%",
              alignSelf: "center",
              marginTop: -20,
              marginLeft: 5
            }}
          />
        </View>

        <View style={styles.rowView}>
          <Text style={styles.section212Text}>SPIRITS DESIGNATION CLASS</Text>

          <Dropdown
            value="Wishkey"
            data={data}
            inputContainerStyle={{ borderBottomColor: "transparent" }}
            pickerStyle={{ marginTop: 90, fontSize: 12 }}
            containerStyle={{
              width: "30%",
              alignSelf: "center",
              marginTop: -20,
              marginLeft: 5
            }}
          />
        </View>
        <View style={styles.rowView}>
          <Text style={styles.section212Text}>SPIRITS DESIGNATION TYPE</Text>

          <Dropdown
            value="Bourbone Wishkey"
            data={data}
            inputContainerStyle={{ borderBottomColor: "transparent" }}
            pickerStyle={{ marginTop: 90 }}
            containerStyle={{
              width: "30%",
              alignSelf: "center",
              marginTop: -20,
              marginLeft: 5
            }}
          />
        </View>

        <View style={styles.rowView}>
          <Text style={styles.section212Text}>VESSEL</Text>

          <Dropdown
            value="Vessel Type 2"
            data={data}
            inputContainerStyle={{ borderBottomColor: "transparent" }}
            pickerStyle={{ marginTop: 90 }}
            containerStyle={{
              width: "30%",
              alignSelf: "center",
              marginTop: -20,
              marginLeft: 5
            }}
          />
        </View>
        <View style={styles.rowView}>
          <Text style={styles.section212Text}>ENTER VOLUME</Text>

          <TextInput
            style={{
              width: "50%",
              marginLeft: width * 0.001,
              fontSize: width * 0.011,
              textAlign: "right"
            }}
            onChangeText={text => this.setState({ search: text })}
            placeholder="Quantity"
            underlineColorAndroid="rgba(0,0,0,0)"
          />

          <Dropdown
            value="GALLONS"
            data={data}
            inputContainerStyle={{ borderBottomColor: "transparent" }}
            pickerStyle={{ marginTop: 90 }}
            containerStyle={{
              width: "20%",
              alignSelf: "center",
              marginTop: -20,
              marginLeft: 15
            }}
          />
        </View>

        <View style={styles.rowView}>
          <Text style={styles.section212Text}>IN HOUSE BATCH NAME</Text>

          <TextInput
            style={{
              width: "50%",
              marginLeft: width * 0.001,
              fontSize: width * 0.011,
              textAlign: "right",
              backgroundColor: "transparent"
            }}
            onChangeText={text => this.setState({ search: text })}
            placeholder="Batch Name"
            underlineColorAndroid="rgba(0,0,0,0)"
          />
        </View>

        <View style={styles.rowView}>
          <Text style={styles.section212Text}>ORIGINAL GRAVITY</Text>

          <TextInput
            style={{
              width: "70%",
              marginLeft: width * 0.001,
              fontSize: width * 0.011,
              textAlign: "right",
              backgroundColor: "transparent"
            }}
            onChangeText={text => this.setState({ search: text })}
            placeholder="Enter Original Gravity"
            underlineColorAndroid="rgba(0,0,0,0)"
          />
        </View>

        <View style={styles.rowView}>
          <Text style={styles.section212Text}>UNIT OF GRAVITY</Text>

          <Dropdown
            value="SPECIFIC"
            data={data}
            inputContainerStyle={{ borderBottomColor: "transparent" }}
            pickerStyle={{ marginTop: 90 }}
            containerStyle={{
              width: "30%",
              alignSelf: "center",
              marginTop: -20,
              marginLeft: 5
            }}
          />
        </View>
      </View>
    );
  }

  getStep2View() {
    let data = [
      { value: "Specific" },
      { value: "Option 1" },
      { value: "Option 2" }
    ];
    return (
      <View style={styles.section21}>
        <View style={[styles.rowView, { borderBottomWidth: 0 }]}>
          <Text
            style={[styles.bigText, { fontSize: 16, fontWeight: "bold" }]}
            numberOfLines={5}
          >
            Add the ingredeints to your batch along with the desired volume
            amount.
          </Text>
        </View>
        <View
          style={[styles.rowView, { borderBottomWidth: 0, marginLeft: "-2%" }]}
        >
          <Text
            style={[
              styles.bigText,
              { fontSize: 14, fontWeight: "bold", color: "#8BC3C3" }
            ]}
            numberOfLines={5}
          >
            RAW MATERIAL
          </Text>
          <Text
            style={[
              styles.bigText,
              { fontSize: 14, fontWeight: "bold", color: "#8BC3C3" }
            ]}
            numberOfLines={5}
          >
            VOLUME
          </Text>
        </View>
        <View style={[styles.rowView, { borderBottomWidth: 0 }]}>
          <View style={styles.subView}>
            <Dropdown
              value="Barley (Qoh:4890 Pounds)"
              data={data}
              inputContainerStyle={{ borderBottomColor: "transparent" }}
              pickerStyle={{ marginTop: 60 }}
              containerStyle={{
                width: "80%",
                alignSelf: "center",
                marginTop: -20,
                marginLeft: 50
              }}
            />
          </View>

          <View style={styles.subView}>
            <TextInput
              style={{
                width: "50%",
                marginLeft: width * 0.001,
                fontSize: width * 0.011,
                textAlign: "right"
              }}
              onChangeText={text => this.setState({ search: text })}
              placeholder="Quantity"
              underlineColorAndroid="rgba(0,0,0,0)"
            />

            <Dropdown
              value="GALLONS"
              data={data}
              inputContainerStyle={{ borderBottomColor: "transparent" }}
              pickerStyle={{ marginTop: 90 }}
              containerStyle={{
                width: "40%",
                alignSelf: "center",
                marginTop: -20,
                marginLeft: 15
              }}
            />
          </View>
        </View>

        <View style={[styles.rowView, { borderBottomWidth: 0 }]}>
          <View style={styles.subView}>
            <Dropdown
              value="Flakes Barley (Qoh:4890 Pounds)"
              data={data}
              inputContainerStyle={{ borderBottomColor: "transparent" }}
              pickerStyle={{ marginTop: 60, fontSize: 12 }}
              containerStyle={{
                width: "100%",
                alignSelf: "center",
                marginTop: -20,
                marginLeft: 5
              }}
            />
          </View>

          <View style={styles.subView}>
            <TextInput
              style={{
                width: "50%",
                marginLeft: width * 0.001,
                fontSize: width * 0.011,
                textAlign: "right"
              }}
              onChangeText={text => this.setState({ search: text })}
              placeholder="Quantity"
              underlineColorAndroid="rgba(0,0,0,0)"
            />

            <Dropdown
              value="GALLONS"
              data={data}
              inputContainerStyle={{ borderBottomColor: "transparent" }}
              pickerStyle={{ marginTop: 90 }}
              containerStyle={{
                width: "40%",
                alignSelf: "center",
                marginTop: -20,
                marginLeft: 15
              }}
            />
          </View>
        </View>

        <View style={[styles.rowView, { borderBottomWidth: 0 }]}>
          <View style={styles.subView}>
            <Text style={styles.section212Text}>DATE</Text>

            <TextInputMask
              ref={"myDateText"}
              type={"datetime"}
              underlineColorAndroid="rgba(0,0,0,0)"
              placeholder="mm/dd/yyyy"
              style={{
                width: "60%",
                height: "100%",
                backgroundColor: "transparent",
                textAlign: "right"
              }}
              options={{ format: "DD/MM/YYYY" }}
            />
          </View>

          <View style={styles.subView}>
            <Text style={styles.section212Text}>TIME</Text>

            <TextInputMask
              ref={"myDateText"}
              type={"datetime"}
              underlineColorAndroid="rgba(0,0,0,0)"
              placeholder="08:55"
              style={styles.section2123Input}
              options={{ format: "hh:mm" }}
            />
            <View style={styles.dropStyle} />
            <Dropdown
              value="AM"
              data={data}
              inputContainerStyle={{ borderBottomColor: "transparent" }}
              pickerStyle={{ marginTop: 90 }}
              containerStyle={{
                width: "20%",
                alignSelf: "center",
                marginTop: -20
              }}
            />
          </View>
        </View>
      </View>
    );
  }

  render() {
    let data = [
      { value: "Specific" },
      { value: "Option 1" },
      { value: "Option 2" }
    ];
    return (
      <KeyboardAwareScrollView
        scrollEnabled={false}
        contentContainerStyle={styles.container}
      >
        <LinearGradient colors={["#004F63", "#008963"]} style={styles.header}>
          <View
            style={{
              width: "100%",
              height: "100%",
              backgroundColor: "transparent",
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center"
            }}
          >
            <Text style={styles.headerText}> Create New Batch </Text>
            <TouchableOpacity
              onPress={() => this.props.sendData(undefined)}
              style={{
                width: "5%",
                backgroundColor: "transparent",
                alignSelf: "flex-start"
              }}
            >
              <Icon
                name="close"
                size={width * 0.025}
                color="#fff"
                style={{ alignSelf: "center", marginTop: 5 }}
              />
            </TouchableOpacity>
          </View>
        </LinearGradient>

        <View style={styles.section2}>
          {!this.state.next ? this.getStep1View() : this.getStep2View()}

          <View style={styles.verticalSpace} />

          <View style={styles.section3}>
            <View style={styles.section31}>
              <Text style={styles.bigText} numberOfLines={5}>
                You can create a new batch with just two steps.
              </Text>
            </View>

            <View style={styles.actionView}>
              <View style={styles.actionView1}>
                <LinearGradient
                  colors={["#008963", "#004F63"]}
                  style={styles.circleViewActive}
                >
                  <Text style={styles.smallText}>1</Text>
                </LinearGradient>

                <View style={styles.space} />
                {!this.state.next && (
                  <View style={styles.circleViewInActive}>
                    <Text style={styles.smallText}>2</Text>
                  </View>
                )}

                {this.state.next && (
                  <LinearGradient
                    colors={["#008963", "#004F63"]}
                    style={styles.circleViewActive}
                  >
                    <Text style={styles.smallText}>2</Text>
                  </LinearGradient>
                )}
              </View>

              <View style={styles.section4}>
                <Text
                  style={[styles.bigText, { fontWeight: "bold" }]}
                >{`Provide Recipe Detail\n`}</Text>
                {!this.state.next && (
                  <Text style={styles.subText}>ongoing</Text>
                )}
                <Text
                  style={[
                    styles.bigText,
                    { fontWeight: "bold", marginTop: "5%" }
                  ]}
                >
                  Add Raw Materials
                </Text>
                {this.state.next && (
                  <Text style={[styles.subText, { marginTop: "0%" }]}>
                    ongoing
                  </Text>
                )}
              </View>
            </View>
          </View>
        </View>

        <View style={styles.section23}>
          {this.state.next && (
            <View style={styles.btnBackground1}>
              <TouchableOpacity
                onPress={() => this.setState({ next: false })}
                style={styles.actionButton}
              >
                <Text style={styles.cancelText}> BACK </Text>
              </TouchableOpacity>
            </View>
          )}
          <LinearGradient
            colors={["#008963", "#004F63"]}
            style={styles.btnBackground}
          >
            <TouchableOpacity
              onPress={() => this.setState({ next: true })}
              style={styles.actionButton}
            >
              <Text style={styles.transferText}>
                {this.state.next ? "START BATCH" : "NEXT"}
              </Text>
            </TouchableOpacity>
          </LinearGradient>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 640,
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "transparent"
  },
  section23: {
    width: "100%",
    height: "12%",
    marginTop: "5%",
    backgroundColor: "transparent",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "flex-end"
  },
  btnBackground: {
    marginRight: 10,
    marginLeft: 10,
    marginTop: 0,
    marginBottom: 5,
    paddingTop: 10,
    paddingBottom: 10,
    borderRadius: width * 0.03,
    borderWidth: 1,
    borderColor: "#fff",
    width: width * 0.155,
    height: width * 0.04,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  circleViewActive: {
    marginRight: 1,
    marginLeft: 1,
    marginTop: 0,
    marginBottom: 5,
    paddingTop: 1,
    paddingBottom: 1,
    backgroundColor: "#008963",
    borderRadius: 12,
    borderWidth: 1,
    borderColor: "transparent",
    width: 25,
    height: 25,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center"
  },
  circleViewInActive: {
    marginRight: 1,
    marginLeft: 1,
    marginTop: 0,
    marginBottom: 0,
    paddingTop: 1,
    paddingBottom: 1,
    backgroundColor: "grey",
    borderRadius: 12,
    borderWidth: 2,
    borderColor: "transparent",
    width: 25,
    height: 25,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center"
  },

  btnBackground1: {
    marginRight: 10,
    marginLeft: 10,
    marginTop: 0,
    marginBottom: 5,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: "#fff",
    borderRadius: width * 0.03,
    borderWidth: 2,
    borderColor: "#008963",
    width: width * 0.155,
    height: width * 0.04,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  actionButton: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "center",
    backgroundColor: "transparent"
  },
  cancelText: {
    fontSize: 15,
    color: "black",
    alignSelf: "center",
    fontWeight: "normal",
    paddingLeft: "0.1%"
  },
  transferText: {
    fontSize: 15,
    color: "white",
    alignSelf: "center",
    fontWeight: "normal",
    paddingLeft: "0.1%"
  },

  mainContainer: {
    width: "92%",
    height: "100%",
    backgroundColor: "transparent",
    flexDirection: "column"
  },
  header: {
    width: "100%",
    height: "8%",
    flexDirection: "row",
    padding: "1%",
    justifyContent: "space-between",
    backgroundColor: "transparent"
  },
  headerText: {
    fontSize: 18,
    width: "50%",
    fontWeight: "normal",
    color: "#ABD5CF",
    alignSelf: "center"
  },
  section2: {
    width: "100%",
    height: "73%",
    backgroundColor: "transparent",
    flexDirection: "row",
    justifyContent: "center"
  },
  section21: {
    width: "70%",
    height: "110%",
    backgroundColor: "transparent",
    flexDirection: "column",
    justifyContent: "flex-start",
    paddingTop: "2%",
    paddingBottom: "2%"
  },
  verticalSpace: {
    width: ".2%",
    height: "70%",
    backgroundColor: "#999999",
    alignSelf: "center"
  },
  section22: {
    width: "29.5%",
    height: "100%",
    backgroundColor: "yellow"
  },
  rowView: {
    width: "94%",
    height: "11%",
    marginLeft: "1%",
    marginRight: "1%",
    backgroundColor: "white",
    borderColor: "grey",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    alignSelf: "center",
    borderBottomWidth: 1
  },
  section3: {
    width: "29.5%",
    height: "100%",
    backgroundColor: "transparent",
    flexDirection: "column",
    justifyContent: "center"
  },
  section31: {
    width: "100%",
    height: "14%",
    backgroundColor: "white",
    flexDirection: "column",
    justifyContent: "center"
  },
  actionView: {
    width: "100%",
    height: "25%",
    backgroundColor: "transparent",
    flexDirection: "row",
    justifyContent: "center",
    marginLeft: "2%"
  },
  actionView1: {
    width: "20%",
    height: "100%",
    backgroundColor: "transparent",
    flexDirection: "column",
    justifyContent: "center",
    marginLeft: "2%",
    marginTop: "1%"
  },
  space: {
    width: "4%",
    height: "10%",
    backgroundColor: "black",
    alignSelf: "center",
    marginTop: "-6%"
  },

  smallText: {
    fontSize: 12,
    fontWeight: "bold",
    paddingLeft: "5%",
    color: "white"
  },
  bigText: {
    fontSize: 15,
    fontWeight: "normal",
    paddingLeft: "5%"
  },
  subText: {
    fontSize: 12,
    fontWeight: "normal",
    paddingLeft: "6%",
    marginTop: "-5%"
  },

  section4: {
    width: "80%",
    height: "100%",
    backgroundColor: "white",
    flexDirection: "column",
    justifyContent: "center",
    marginLeft: "2%"
  },
  subView: {
    width: "45%",
    height: "100%",
    backgroundColor: "transparent",
    borderColor: "grey",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderBottomWidth: 1
  },
  section212Text: {
    fontSize: 15,
    marginLeft: 3,
    fontWeight: "bold",
    alignSelf: "center",
    color: "#767676"
  },
  dropStyle: {
    width: "1%",
    height: "100%",
    backgroundColor: "#DBDBDB"
  },
  section2123Input: {
    width: "55%",
    height: "100%",
    alignSelf: "center",
    justifyContent: "flex-end",
    flexDirection: "row",
    backgroundColor: "transparent",
    marginRight: "0%",
    textAlign: "right"
  }
});
