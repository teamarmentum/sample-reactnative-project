import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  StatusBar,
  TouchableOpacity,
  Dimensions,
  TextInput,
  View
} from "react-native";
var { height, width } = Dimensions.get("window");

import { Switch } from "react-native-switch";
import LinearGradient from "react-native-linear-gradient";
import Icon from "react-native-vector-icons/dist/EvilIcons";
import { Dropdown } from "react-native-material-dropdown";

import { TextInputMask } from "react-native-masked-text";
import ModalDropdown from "react-native-modal-dropdown";
import Icon4 from "react-native-vector-icons/dist/FontAwesome";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

export default class Record extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      entireDumpSwitchIsOn: false,
      vesselEmptySwitchIsOn: false,
      newSpiritSwitchIsOn: false,
      time: true,
      stage: ""
    };
  }

  _toggel = () => {
    this.setState({ time: !this.state.time });
  };

  _stage = currentStage => {
    this.setState({
      ...this.state,
      stage: currentStage
    });
  };

  render() {
    let data = [
      { value: "Specific" },
      { value: "Option 1" },
      { value: "Option 2" }
    ];

    return (
      <KeyboardAwareScrollView
        scrollEnabled={false}
        contentContainerStyle={styles.container}
      >
        <LinearGradient colors={["#004F63", "#008963"]} style={styles.header}>
          <Text style={styles.headerText}> Record Batch</Text>
          <TouchableOpacity onPress={() => this.props.sendData(undefined)}>
            <Icon
              name="close"
              size={width * 0.025}
              color="#fff"
              style={{ alignSelf: "center" }}
            />
          </TouchableOpacity>
        </LinearGradient>
        <View style={styles.section1}>
          <Text style={styles.section11Text}> UPDATE BATCH RECORD </Text>
          <View style={{ alignSelf: "center", marginLeft: 20 }}>
            <Switch
              value={this.state.newSpiritSwitchIsOn}
              onValueChange={val => console.log(val)}
              disabled={false}
              activeText={" "}
              inActiveText={" "}
              styles={{
                alignSelf: "center",
                marginLeft: -40,
                paddingBottom: 3
              }}
              backgroundActive={"green"}
              backgroundInactive={"#E5E5E5"}
              circleActiveColor={"#30a566"}
              circleInActiveColor={"#007265"}
            />
          </View>
        </View>

        <View style={styles.spaceViewMain} />

        <View style={styles.section2}>
          <View style={styles.section21}>
            <View style={styles.section212}>
              <Text style={styles.section212Text}>DATE</Text>
              <TextInputMask
                ref={"myDateText"}
                type={"datetime"}
                underlineColorAndroid="rgba(0,0,0,0)"
                placeholder="mm/dd/yyyy"
                style={{ width: "30%", height: "100%" }}
                options={{ format: "DD/MM/YYYY" }}
              />
            </View>

            <View style={styles.section212}>
              <Text style={styles.section212Text}>DATE</Text>
              <TextInputMask
                ref={"myDateText"}
                type={"datetime"}
                underlineColorAndroid="rgba(0,0,0,0)"
                placeholder="08:55"
                style={styles.section2123Input}
                options={{ format: "hh:mm" }}
              />
              <View style={styles.dropStyle} />
              <Dropdown
                value="AM"
                data={data}
                inputContainerStyle={{ borderBottomColor: "transparent" }}
                pickerStyle={{ marginTop: 90 }}
                containerStyle={{
                  width: "15%",
                  alignSelf: "center",
                  marginTop: -20
                }}
              />
            </View>
          </View>
        </View>

        <View style={styles.section22}>
          <View style={styles.section231}>
            <Text style={styles.section231Text}>BATCH METRIX</Text>
          </View>

          <View style={styles.section232}>
            <View style={styles.section2321}>
              <Text style={styles.section212Text}>ORIGINAL GRAVITY</Text>
              <TextInput
                style={{
                  width: "30%",
                  marginLeft: width * 0.001,
                  fontSize: width * 0.011
                }}
                onChangeText={text => this.setState({ search: text })}
                placeholder="Enter Gravity"
                underlineColorAndroid="rgba(0,0,0,0)"
              />
            </View>

            <View style={styles.section2321}>
              <Text style={styles.section212Text}>UNIT OF GRAVITY</Text>
              <Dropdown
                value="Specific"
                data={data}
                inputContainerStyle={{ borderBottomColor: "transparent" }}
                pickerStyle={{ marginTop: 50 }}
                containerStyle={{
                  width: "30%",
                  alignSelf: "center",
                  marginTop: -20
                }}
              />
            </View>
          </View>

          <View style={styles.section232}>
            <View style={styles.section2321}>
              <Text style={styles.section212Text}>pH</Text>
              <TextInput
                style={{
                  width: "30%",
                  marginLeft: width * 0.001,
                  fontSize: width * 0.011
                }}
                onChangeText={text => this.setState({ search: text })}
                placeholder="Enter pH"
                underlineColorAndroid="rgba(0,0,0,0)"
              />
            </View>

            <View style={styles.section2321}>
              <Text style={styles.section212Text}>TEMPERATURE</Text>
              <View style={styles.section2123}>
                <TextInput
                  style={{
                    width: "45%",
                    marginLeft: width * 0.001,
                    fontSize: width * 0.011
                  }}
                  onChangeText={text => this.setState({ search: text })}
                  placeholder="Temp"
                  underlineColorAndroid="rgba(0,0,0,0)"
                />
                <View
                  style={{
                    width: ".35%",
                    height: "100%",
                    backgroundColor: "#DBDBDB"
                  }}
                />
                <Dropdown
                  value="F"
                  data={data}
                  inputContainerStyle={{ borderBottomColor: "transparent" }}
                  pickerStyle={{ marginTop: 90 }}
                  containerStyle={{
                    width: "30%",
                    alignSelf: "center",
                    marginTop: -20
                  }}
                />
              </View>
            </View>
          </View>
        </View>

        <View style={styles.section23}>
          <View style={styles.btnBackground1}>
            <TouchableOpacity
              onPress={() => this.props.sendData(undefined)}
              style={styles.actionButton}
            >
              <Text style={styles.cancelText}> CANCEL </Text>
            </TouchableOpacity>
          </View>

          <LinearGradient
            colors={["#008963", "#004F63"]}
            style={styles.btnBackground}
          >
            <TouchableOpacity style={styles.actionButton}>
              <Text style={styles.transferText}>SAVE</Text>
            </TouchableOpacity>
          </LinearGradient>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#fff"
  },

  mainContainer: {
    width: "92%",
    height: "100%",
    backgroundColor: "green",
    flexDirection: "column"
  },
  header: {
    backgroundColor: "#455A64"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  header: {
    width: "100%",
    height: "10%",
    backgroundColor: "red",
    flexDirection: "row",
    padding: "1%",
    justifyContent: "space-between"
  },
  headerTest: {
    fontSize: 18,
    fontWeight: "normal",
    color: "#ABD5CF",
    alignSelf: "center"
  },
  section1: {
    width: "100%",
    height: "15%",
    backgroundColor: "white",
    paddingLeft: "2%",
    paddingRight: "2%",
    flexDirection: "row",
    justifyContent: "flex-start",
    paddingLeft: 20
  },
  section11: {
    width: "33.33%",
    height: "100%",
    paddingTop: "1%",
    backgroundColor: "transparent",
    flexDirection: "column",
    justifyContent: "center"
  },
  section11Text: {
    fontSize: 18,
    color: "black",
    alignSelf: "center",
    paddingBottom: 1
  },
  spaceViewMain: {
    width: "97%",
    height: 1,
    backgroundColor: "#DBDBDB",
    justifyContent: "center"
  },
  btnBackground: {
    marginRight: 10,
    marginLeft: 10,
    marginTop: 0,
    marginBottom: 5,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: "#008963",
    borderRadius: width * 0.03,
    borderWidth: 1,
    borderColor: "#fff",
    width: width * 0.155,
    height: width * 0.04,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  btnBackground1: {
    marginRight: 10,
    marginLeft: 10,
    marginTop: 0,
    marginBottom: 5,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: "#fff",
    borderRadius: width * 0.03,
    borderWidth: 2,
    borderColor: "#008963",
    width: width * 0.155,
    height: width * 0.04,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  headerText: {
    fontSize: 18,
    fontWeight: "normal",
    color: "#ABD5CF",
    alignSelf: "center"
  },
  section2: {
    width: "100%",
    height: "13%",
    paddingLeft: "2%",
    paddingRight: "2%",
    backgroundColor: "white",
    flexDirection: "column",
    justifyContent: "center"
  },
  section21: {
    width: "100%",
    height: "100%",
    backgroundColor: "transparent",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  section212: {
    width: "47%",
    height: "100%",
    backgroundColor: "transparent",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: "2%",
    paddingRight: "2%",
    borderColor: "#CBCBCB",
    borderBottomWidth: 1
  },
  section212Text: {
    fontSize: 16,
    fontWeight: "bold",
    alignSelf: "center",
    color: "#767676"
  },
  section2123: {
    width: "40%",
    height: "100%",
    backgroundColor: "transparent",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  section2123Input: {
    width: "55%",
    height: "100%",
    alignSelf: "center",
    justifyContent: "flex-end",
    flexDirection: "row",
    backgroundColor: "transparent",
    marginRight: "-10%"
  },
  dropStyle: {
    width: "1%",
    height: "100%",
    backgroundColor: "#DBDBDB"
  },
  section22: {
    width: "100%",
    height: "40%",
    backgroundColor: "transparent",
    flexDirection: "column",
    justifyContent: "flex-start",
    paddingLeft: "2%",
    paddingRight: "2%",
    borderColor: "#DBDBDB",
    paddingTop: "2%",
    borderBottomWidth: 0
  },
  section223: {
    width: "100%",
    height: "20%",
    marginTop: "1%",
    flexDirection: "row",
    justifyContent: "flex-start",
    backgroundColor: "transparent"
  },
  section23: {
    width: "100%",
    height: "22%",
    backgroundColor: "#DBDBDB",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  section231: {
    width: "100%",
    height: "20%",
    flexDirection: "row",
    justifyContent: "flex-start",
    backgroundColor: "transparent",
    padding: "0%"
  },
  section232: {
    width: "100%",
    height: "25%",
    backgroundColor: "transparent",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: "0%",
    paddingRight: "0%",
    marginTop: "1%"
  },
  section2321: {
    width: "48%",
    height: "100%",
    backgroundColor: "white",
    flexDirection: "row",
    justifyContent: "space-between",
    borderColor: "#CBCBCB",
    borderBottomWidth: 1
  },
  section224D: {
    width: "50%",
    height: "100%",
    backgroundColor: "white"
  },
  section231Text: {
    fontSize: 16,
    alignSelf: "center",
    color: "#005565",
    fontWeight: "bold"
  },
  section2123: {
    width: "35%",
    height: "100%",
    backgroundColor: "transparent",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  section2123Tinput: {
    width: "60%",
    marginLeft: width * 0.001,
    fontSize: width * 0.011
  },
  spaceVertical: {
    width: ".4%",
    height: "100%",
    backgroundColor: "#DBDBDB"
  },
  section233: {
    width: "96%",
    height: "16.66%",
    backgroundColor: "white",
    flexDirection: "row",
    justifyContent: "space-between",
    marginLeft: "2%",
    marginRight: "2%",
    borderColor: "#DBDBDB",
    borderBottomWidth: 2
  },
  section234: {
    width: "100%",
    height: "20%",
    backgroundColor: "white",
    flexDirection: "row",
    justifyContent: "center",
    paddingLeft: "0.1%",
    paddingRight: "0.1%"
  },
  actionButton: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "center",
    backgroundColor: "transparent"
  },
  cancelText: {
    fontSize: 15,
    color: "black",
    alignSelf: "center",
    fontWeight: "normal",
    paddingLeft: "0.1%"
  },
  transferText: {
    fontSize: 15,
    color: "white",
    alignSelf: "center",
    fontWeight: "normal",
    paddingLeft: "0.1%"
  }
});
