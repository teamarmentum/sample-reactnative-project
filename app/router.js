import React, { Component } from "react";
import { View, Text } from "react-native";
import EStyleSheet from "react-native-extended-stylesheet";
import { Scene, Router } from "react-native-router-flux";

import Login from "./screens/login";
import Home from "./screens/home";
import ResetPassword from "./screens/resetpass";
import ForgotPassword from "./screens/forgotPassword";

export default class Navigation extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Router>
        <Scene key="root">
          <Scene
            key="login"
            component={Login}
            initial={true}
            hideNavBar={true}
          />
          <Scene
            key="home"
            component={Home}
            initial={false}
            hideNavBar={true}
          />
          <Scene
            key="resetpass"
            component={ResetPassword}
            initial={false}
            hideNavBar={true}
          />
          <Scene
            key="forgotpass"
            component={ForgotPassword}
            initial={false}
            hideNavBar={true}
          />
        </Scene>
      </Router>
    );
  }
}
