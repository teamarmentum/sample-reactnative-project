/* @flow */

import {
    Dimensions,
} from 'react-native';

let multiplier;
(() => {
    const size = Dimensions.get('screen');
    const ratio =
        Math.max(size.width, size.height) / Math.min(size.width, size.height);
    multiplier = ratio > (5 / 3) ? 0.8 : 1;
})();

export const isHighRatio = multiplier !== 1;

export const getSizeInDp = (size: number): number => size * multiplier;
