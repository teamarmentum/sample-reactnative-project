/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from "react";
import { AppRegistry } from "react-native";

import Index from "./app/index.js";
console.disableYellowBox = true;

AppRegistry.registerComponent("application", () => Index);
